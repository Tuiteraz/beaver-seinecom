#!/usr/bin/env bash
docker-compose -f ./devops/docker/docker-compose/local.yml down
docker-compose -f ./devops/docker/docker-compose/local.yml up --remove-orphans