#!/usr/bin/env bash
docker-compose -f ./devops/docker/docker-compose/production.yml down
docker-compose -f ./devops/docker/docker-compose/production.yml up --remove-orphans