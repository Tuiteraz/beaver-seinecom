#!/usr/bin/env bash

mkdir -p /tmp && cd /tmp
curl -sL "https://github.com/docker/compose/releases/download/1.18.0/docker-compose-$(uname -s)-$(uname -m)" > ./docker-compose
sudo mv ./docker-compose /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo apt-get update && \
sudo apt-get remove -y docker docker-engine docker.io && \
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl unzip wget mc \
    software-properties-common && \
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && \
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash &&\
nvm install 10 && \
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" && \
sudo apt-get update && \
sudo rm -f /etc/default/docker && \
sudo apt-get install -y --asume-yes=true docker-ce

sudo apt-get install unzip