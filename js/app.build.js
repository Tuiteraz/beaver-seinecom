({
    baseUrl:'.',
    include: ['app','require-js'],
    stubModules: ['text'],
    out: "app.js",
    paths: {
        'require-js'       : 'libs/require-js/require',
        'jquery'           : 'libs/jquery/jquery-1.9.1',
        'jquery-plugins'   : 'libs/jquery/jquery.plugins',
        'jquery-history'   : 'libs/jquery/plugins/jquery-history',
        'domReady'         : 'libs/require-js/plugins/domReady',
        'lib-pack'         : 'libs/lib.pack',
        'stylesheets-pack' : '../css/stylesheets.pack',

        'bconsole'         : './libs/beaver/console',
        'extensions'       : 'libs/beaver/extensions',

        'lightbeam-helpers'  : 'libs/lightbeam/helpers',
        'lightbeam-settings' : 'libs/lightbeam/settings',

        'underscore'       : 'libs/underscore',
        'underscore.string': 'libs/underscore.string.min',
        'bootstrap'        : 'libs/bootstrap/js/bootstrap.min',
        'bootstrap-hover-dropdown' : 'libs/bootstrap/js/twitter-bootstrap-hover-dropdown.min',
        'crossroads'       : 'libs/crossroads',
        'signals'          : 'libs/signals',
        'soundmanager2'    : 'libs/sound-manager2/js/soundmanager2',
        'async'            : 'libs/async',
        'object-id'        : 'libs/object-id',
        'video-js'         : 'libs/video-js/video.pack'

    },
    shim: {
        'jquery-plugins'            : ["jquery"],
        'libs/beaver/helpers'        : ['jquery'],
        'libs/bootstrap/js/bootstrap.min' : ['jquery'],

        'libs/beaver/settings'       : ['settings'],

        'crossroads'                : ['signals'],

        'bconsole'                  : ['underscore','extensions'],
        'extensions'                : ['jquery','underscore'],
        'lib-pack'                  : ['bconsole'],
        'stylesheets-pack'          : ['extensions'],
        'underscore.string'         : ['underscore'],
        'bootstrap-hover-dropdown'  : ['bootstrap'],
        'controllers/player-ctrl'   : ['soundmanager2']

    },

    preserveLicenseComments : false,
    logLevel:0
})