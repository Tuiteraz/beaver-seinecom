window.bJvmConsoleEnabled = false
window.sJvmConsoleLogContext = "APP"

if typeof requirejs == "undefined"
  jHead = document.getElementsByTagName('head')[0]
  jScript = document.createElement('script')
  jScript.setAttribute('type','text/javascript')
  jScript.setAttribute('src','/js/libs/require-js/require.js')
  jScript.setAttribute('data-main','/js/app.js')
  jHead.appendChild(jScript)

  return

requirejs.config {
  waitSeconds: 5
  packages: [
    {
      name: "text"
      location : "libs/require-js/plugins"
      main: 'text'
    }
  ]
  paths: {
    'jquery'           : 'libs/jquery/jquery-1.9.1'  # check mongoos-model.coffee version
    'jquery-plugins'   : 'libs/jquery/jquery.plugins'
    'domReady'         : 'libs/require-js/plugins/domReady'
    'lib-pack'         : 'libs/lib.pack'
    'stylesheets-pack' : '../css/stylesheets.pack'
    'require-js'       : 'libs/require-js/require'

    'bconsole'           : 'libs/beaver/console'
    'extensions'         : 'libs/beaver/extensions'

    'lightbeam-helpers'  : 'libs/lightbeam/helpers'
    'lightbeam-settings' : 'libs/lightbeam/settings'

    'underscore'       : 'libs/underscore'
    'underscore.string': 'libs/underscore.string.min'
    'bootstrap'        : 'libs/bootstrap/js/bootstrap.min'
    'bootstrap-hover-dropdown' : 'libs/bootstrap/js/twitter-bootstrap-hover-dropdown.min'
    'crossroads'       : 'libs/crossroads'
    'signals'          : 'libs/signals'
    'soundmanager2'    : 'libs/sound-manager2/js/soundmanager2'
    'async'            : 'libs/async'
    'object-id'        : 'libs/object-id'
    'video-js'         : 'libs/video-js/video.pack'
  }
  shim: {
    'jquery': {
      exports: 'jQuery'
    }

    'jquery-plugins'            : ["jquery"]
    'libs/beaver/helpers'        : ['jquery']
    'bootstrap'                 : ['jquery']

    'libs/beaver/settings'       : ['settings']

    'stubs/pages-stub'          : ['settings','jquery']
#    'configuration/navigation'     : ['settings','jquery']

    'crossroads'                : ['signals']

    'bconsole'                  : ['extensions']
    'extensions'                : ['jquery','underscore']
    'lib-pack'                  : ['bconsole']
    'stylesheets-pack'          : ['extensions']
    'underscore.string'         : ['underscore']
    'bootstrap-hover-dropdown'  : ['bootstrap']
    'controllers/player-ctrl'   : ['soundmanager2']
  }

}

requirejs [
  'lib-pack'
  'bconsole'
  'controllers/cp-ctrl'
  'controllers/site-ctrl'
  'domReady'
  'stylesheets-pack'
], (mdlLibPack
    jconsole
    ctrlCp
    ctrlSite
    domReady
) ->
  jconsole.info "starting app..."

  domReady ->
    jconsole.log "DOM ready now!"
    ctrlCp.init ->
      ctrlSite.init()

    $("meta[name=fragment]").remove() # because search engine would't accept html snapshot with this tag

