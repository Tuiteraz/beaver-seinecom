define [
  'bconsole'
  "models/client-logo-model"
  "models/work-model"
  'async'
],(
  jconsole
  ClientLogoModel
  WorkModel
  async
) ->
  jconsole.info "configuration/navigation"

  #+2013.12.17 tuiteraz
  before_filter:->
    @init_models()

  #+2013.12.17 tuiteraz
  get_content: (fnCallback)->
    @before_filter()
    @get_data ()=>
      hRes =
        aData: [
          {
            sSlug: "intro"
            sTitle: 'Intro'
            sBrowserTitle: 'Intro'
            bIsIntro: true
            bShowInNav : false
          }
          {
            sSlug: "index"
            sTitle: 'Home'
            sBrowserTitle: 'Home'
            bIsSinglePage: false
            bIsIndex: true
            bShowInNav : false
          }
          {
            sSlug: "about-us"
            sTitle: 'ABOUT US'
            sBrowserTitle: 'About us'
            bIsSinglePage: true
            id: "nav-about"
            bShowInNav : true
          }
          {
            sSlug: "works"
            sTitle: 'WORKS'
            sBrowserTitle: 'Works'
            bIsSinglePage: true
            id: "nav-works"
            aChildrens: _.where(@aWorks, {bInArchive:false})
            bShowInNav : true
            bShowChildrensDropdown: true
          }
          {
            sSlug: "clients"
            sTitle: 'CLIENTS'
            sBrowserTitle: 'Clients'
            bIsSinglePage: true
            id: "nav-clients"
            bShowInNav : true
            aChildrens: _.clone @aClientLogos
          }
          {
            sSlug: "creative-archive"
            sTitle: 'CREATIVE ARCHIVE'
            sBrowserTitle: 'Creative archive'
            bIsSinglePage: true
            id: "nav-creative-archive"
            bShowInNav : true
            aChildrens: _.where(@aWorks, {bInArchive:true})
            bShowChildrensDropdown: true
          }
          {
            sSlug: "art-department"
            sTitle: 'ART DEPARTMENT'
            sBrowserTitle: 'Art department'
            bIsSinglePage: true
            id: "nav-art-department"
            bShowInNav : true
            iContainerHeight: 840
          }
          {
            sSlug: "contacts"
            sTitle: 'CONTACTS'
            sBrowserTitle: 'Contacts'
            bIsSinglePage: true
            id: "nav-contacts"
            bShowInNav : true
          }
        ]
      fnCallback hRes

  #+2013.12.17 tuiteraz
  get_data:(fnCallback)->
    async.parallel [
      (fnNext) =>
        @mClientLogo.get {},{sSort: "iOrder"}, (hRes)=>
          @aClientLogos = []

          if hRes.iStatus != 200
            fnNext hRes
          else
            _.each hRes.aData, (hLogo)=>
              @aClientLogos.push hLogo.File
            fnNext()
      (fnNext) =>
        @mWork.get {},{sSort: "iOrder"}, (hRes)=>
          @aWorks = []

          if hRes.iStatus != 200
            fnNext hRes
          else
            _.each hRes.aData, (hWork)=>
              @aWorks.push hWork
            fnNext()

    ],(hErr,aRes)->
      if hErr
        jconsole.error "configuration/navigation.get_data() : #{hRes.sMessage}"
      fnCallback()

  #+2013.12.17 tuiteraz
  init_models:->
    @mClientLogo  = new ClientLogoModel SITE.hMongoDB.hModels.hClientLogo
    @mWork        = new WorkModel SITE.hMongoDB.hModels.hWork

    