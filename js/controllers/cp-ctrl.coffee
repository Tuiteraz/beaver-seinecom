define [
  'bconsole'
  'crossroads'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/nav-item-helpers'
  'templates/common-tmpl'
  'templates/cp-tmpl'
  'lightbeam-helpers'
  'libs/lightbeam/tools/auth'
  "models/user-model"
  "async"
  "./cp/data-ctrl"
  "./cp/login-ctrl"
], (jconsole,
  crossroads,
  tmpl,hlprs,img_hlprs,nav_item_hlprs,
  common_tmpl,cp_tmpl,
  lb_hlprs
  lb_auth
  UserModel
  async
  ctrlData
  ctrlLogin
) ->
  jconsole.info "cp-ctrl"

  #+2013.10.30 tuiteraz
  init: (fnCallback) ->
    @Router = crossroads.create()
    @init_routes(fnCallback)

  #+2013.12.7 tuiteraz
  init_routes:(fnCallback) ->
    jMainCpRoute = @Router.addRoute "/cp/:section:/:object:/:item-id:/:operation:", (sSection,sObject,sItemId,sOperation) =>
      jconsole.debug "CP-ROUTE:/cp/:#{sSection}:/:#{sObject}: - initialized[#{@bInitiated}]"

      document.title = SITE.cp.sTitle

      if !@bInitiated
        hlprs.load_stylesheet "/css/cp.css"
        window.sJvmConsoleLogContext = "CP"

        @init_models()
        if @is_signed_in()
          @bInitiated = true
          @render()
          @bind_events()
          @force_data_route() if _.isEmpty(sSection)
        else
          if !_.isEmpty(sSection)
            @force_root_route()
          else
            lb_auth.sign_out =>
              send_event_to ctrlLogin, {sAction: SITE.actions.cp.signed_out}
      else
        @force_data_route() if _.isEmpty(sSection) && @is_signed_in()

    if jMainCpRoute.match History.getState().hash
#      jconsole.enable_log "CP"
      jconsole.debug "cp-ctrl.init()"

      History.options.disableSuid = true

      @bind_events(true)
      @bInitiated = false

      ctrlLogin.init this
      ctrlData.init this

      lb_auth.get_auth_status (@hAuthStatus)=>
        @Router.parse History.getState().hash

    else
      fnCallback()


  #+2013.10.31 tuiteraz
  init_models: ->
    @mUser   = new UserModel SITE.hMongoDB.hModels.hUser

  #+2013.10.30 tuiteraz
  bind_events:(bMdlOnly=false) ->
    me = this

    History.Adapter.bind window, "statechange", =>
      @Router.parse History.getState().hash

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        hlprs.goto(hDetails)             if hDetails.sAction == SITE.actions.goto_nav_link
        @event_on_signed_in(e,hDetails)  if hDetails.sAction == SITE.actions.cp.signed_in
        @event_on_signed_out(e,hDetails) if hDetails.sAction == SITE.actions.cp.signed_out


    if !bMdlOnly
      @bind_html_events()

  #+2013.11.26 tuiteraz
  bind_html_events: ()->
    me = this

    hBtnSignOut = SITE.cp.nav.buttons.sign_out
    $("##{hBtnSignOut.id}").unbind('click').click (e)->
      lb_auth.sign_out ->
        send_event_to me, {sAction: SITE.actions.cp.signed_out}

    # any nav item click
    $(document).delegate "a","click", (e) ->
      sHref = $(this).attr('href')
      if sHref != "#"
        send_event_to me, {sHref: sHref, sAction: SITE.actions.goto_nav_link, sTitle:SITE.cp.sTitle}
        e.preventDefault()


  #+2013.11.26 tuiteraz
  event_on_signed_in: (e,hDetails) ->
    jconsole.debug "cp/cp-ctrl.event_on_signed_in()"
    @Router.resetState()

    @hAuthStatus = _.clone hDetails.hAuthStatus

    @render()
    @bind_events()
    @force_data_route()

  #+2013.11.126 tuiteraz
  event_on_signed_out:(e,hDetails) ->
    @hAuthStatus.bIsSignedIn = false

    sRoute = "/#{SITE.cp.sSlug}"
    hlprs.goto {sAction: SITE.actions.goto_nav_link, sHref: sRoute}

    send_event_to ctrlLogin, hDetails

  force_data_route: ()->
    # force route for data-ctrl until it's only one content controller
    send_event_to ctrlData, {sAction: SITE.actions.cp.force_default_route}

  #+2013.12.5 tuiteraz
  force_root_route: ()->
    # force /cp route
    hlprs.goto {sAction: SITE.actions.goto_nav_link, sHref: "/cp"}

  # отправить заспро на сервер для получения данных
  # +2013.10.30 tuiteraz
  get: (hQuery, fnCallback) ->
    jconsole.warn "cp-ctrl: get() [stub]"
    # -( stubs
#    @aTests = []
    # -) stubs

    fnCallback()

  #+2013.11.26 tuiteraz
  is_signed_in: ()->
    if @hAuthStatus.bIsSignedIn && @hAuthStatus.hUser.bIsAdmin
      return true
    else
      return false

  #+2013.10.30 tuiteraz
  render: ->
    $('body').addClass "body-subtle-bg body-padding body-cp"
    $('body').empty().append cp_tmpl.render()


