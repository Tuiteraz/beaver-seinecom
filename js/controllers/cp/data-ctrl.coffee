define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/img-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'crossroads'
  "async"
  "./data/client-logos-ctrl"
  "./data/works-ctrl"
  "./data/music-ctrl"
  "./data/art-department-ctrl"
  "./data/text-pieces-ctrl"
], (jconsole,
    tmpl,hlprs,cp_hlprs,img_hlprs,
    common_tmpl,data_tmpl,
    crossroads
    async
    ctrlClientLogos
    ctrlWorks
    ctrlMusic
    ctrlArtDep
    ctrlTextPieces
) ->
  jconsole.info "cp/data-ctrl"

  #+2013.1126 tuiteraz
  init:(@ctrlCp)->
    jconsole.debug "data-ctrl.init()"

    @Router = crossroads.create()
    @Router.ignoreState = true

    # все вызовы @ctrlCp.Router.parse() будут перенаправляться дополнительно на @Router
    @ctrlCp.Router.pipe @Router

    ctrlClientLogos.init()
    ctrlWorks.init(this)
    ctrlMusic.init()
    ctrlArtDep.init(this)
    ctrlTextPieces.init(this)

    @init_routes()
    @bind_events(true)

  #+2013.11.26 tuiteraz
  init_routes: ()->
    me = this

    @Router.addRoute "/#{SITE.cp.sSlug}/#{SITE.cp.data.sSlug}", ()=>
      sRoute = "/#{SITE.cp.sSlug}/#{SITE.cp.data.sSlug}"
      jconsole.debug "DATA-ROUTE: #{sRoute}"
      send_event_to me, {sAction: SITE.actions.cp.force_default_route}

    @Router.addRoute "/#{SITE.cp.sSlug}/#{SITE.cp.data.sSlug}/{sObject}/:item-id:/:operation:", (sObject,sItemId,sOperation)=>
      jconsole.debug "DATA-ROUTE: /cp/data/{#{sObject}}"
      sRoute = "/cp/data/#{sObject}"
      @render(sRoute) if !@is_rendered()
      if !sItemId
        send_event_to me, {sAction:SITE.actions.cp.show_content_tab, sRoute:sRoute}

  #+2013.11.26 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this

    $("##{SITE.cp.data.nav.container_id}").affix {
      offset : SITE.cp.nav.iHeight + 20
    }

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_force_default_route(e,hDetails)     if hDetails.sAction == SITE.actions.cp.force_default_route
        @event_on_content_tab_show(e,hDetails)        if hDetails.sAction == SITE.actions.cp.show_content_tab
        @event_on_content_tab_add_item(e,hDetails)    if hDetails.sAction == SITE.actions.cp.content.add_item
        @event_on_content_tab_edit_item(e,hDetails)   if hDetails.sAction == SITE.actions.cp.content.edit_item
        @event_on_content_tab_go_back(e,hDetails)     if hDetails.sAction == SITE.actions.cp.content.go_back
        @event_on_content_tab_remove_item(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.remove_item
        @event_on_content_tab_update_item(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.update_content
        @event_on_content_tab_refresh(e,hDetails)     if hDetails.sAction == SITE.actions.cp.content.refresh

    SITE.cp.data.nav.items.logos["controller"] =
      _this       : ctrlClientLogos
      add_item    : ctrlClientLogos.add_item
      remove_item : ctrlClientLogos.remove_item
      refresh     : ctrlClientLogos.refresh
      show_tab    : ctrlClientLogos.show_tab
    SITE.cp.data.nav.items.works["controller"] =
      _this       : ctrlWorks
      add_item    : ctrlWorks.add_item
      remove_item : ctrlWorks.remove_item
      refresh     : ctrlWorks.refresh
      show_tab    : ctrlWorks.show_tab
    SITE.cp.data.nav.items.music["controller"] =
      _this       : ctrlMusic
      add_item    : ctrlMusic.add_item
      remove_item : ctrlMusic.remove_item
      refresh     : ctrlMusic.refresh
      show_tab    : ctrlMusic.show_tab
      update_content : ctrlMusic.update_content
    SITE.cp.data.nav.items.art_department["controller"] =
      _this       : ctrlArtDep
      add_item    : ctrlArtDep.add_item
      remove_item : ctrlArtDep.remove_item
      refresh     : ctrlArtDep.refresh
      show_tab    : ctrlArtDep.show_tab
      update_content : ctrlArtDep.update_content
    SITE.cp.data.nav.items.text_pieces["controller"] =
      _this       : ctrlTextPieces
      add_item    : ctrlTextPieces.add_item
      remove_item : ctrlTextPieces.remove_item
      refresh     : ctrlTextPieces.refresh
      show_tab    : ctrlTextPieces.show_tab
      update_content : ctrlTextPieces.update_content


    if !bMdlOnly
      @bind_html_events()

  #+2013.11.28 tuiteraz
  bind_html_events:->
    me = this

    # cp content tab header & items btns
    sSelector = "##{SITE.cp.data.container_id} button[data-content-action]"
    $(document).undelegate(sSelector,'click').delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

  #+2013.11.28 tuiteraz
  event_on_content_tab_add_item: (e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_content_tab_add_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")

    sObjectRoute = jContentCntr.attr "data-route"
    sObjectSlug = _.strRightBack sObjectRoute,"/"

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.add_item
        _this = hNavItem.controller._this
        hNavItem.controller.add_item.call _this,hDetails

  #+2013.12.7 tuiteraz
  event_on_content_tab_edit_item: (e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_content_tab_edit_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"
    sObjectSlug = _.strRightBack sObjectRoute,"/"

    sItemId  = jItemCntr.attr 'id'

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug

    sEditRoute = cp_hlprs.get_edit_item_route(hNavItem,sItemId)
    hlprs.goto {sHref:sEditRoute}

  # this ecent trigger in edit tab
  #+2013.12.12 tuiteraz
  event_on_content_tab_go_back:(e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_content_tab_go_back()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    sObjectRoute = jContentCntr.attr "data-route"

    if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug
      sObjectRoute = cp_hlprs.get_object_route hNavItem
      hlprs.goto {sHref:sObjectRoute}


  #+2013.11.28 tuiteraz
  event_on_content_tab_refresh: (e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_content_tab_refresh()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")

    sObjectRoute = jContentCntr.attr "data-route"
    sObjectSlug = _.strRightBack sObjectRoute,"/"

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.refresh
        _this = hNavItem.controller._this
        hNavItem.controller.refresh.call _this

  #+2013.11.28 tuiteraz
  event_on_content_tab_remove_item: (e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_content_tab_remove_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.remove_item
        _this = hNavItem.controller._this
        hNavItem.controller.remove_item.call _this,sItemId

  #+2013.11.28 tuiteraz
  event_on_content_tab_update_item: (e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_content_tab_update_item()"

    jContentCntr = hDetails.jThis.parents(".cp-content-tab")
    jItemCntr = hDetails.jThis.parents(".cp-content-item")

    sObjectRoute = jContentCntr.attr "data-route"

    # possible route patterns:
    # /cp/data/works
    # /cp/data/works/{id}/edit

    if /^\/cp\/data\/([\w-_]{3,20})$/i.test sObjectRoute
      sObjectSlug = _.strRightBack sObjectRoute,"/"
      sItemId  = jItemCntr.attr 'id'
    else if /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i.test sObjectRoute
      aMatches = sObjectRoute.match /^\/cp\/data\/([\w-_]{3,20})\/([\w\d]{10,40})\/edit$/i
      sObjectSlug = aMatches[1]
      sItemId     = aMatches[2]

    hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug

    if hNavItem.controller
      if _.isFunction hNavItem.controller.update_content
        _this = hNavItem.controller._this
        hNavItem.controller.update_content.call _this,sItemId
      else
        # этот объект не поддерживает редактирование в списке и вероятно событие пришло из открытой формы редактирования элемента
        send_event_to hNavItem.controller._this,hDetails
    else
      jconsole.error "No controller property found for nav item:#{JSON.stringify(hNavItem)}"


  #+2013.11.27 tuiteraz
  event_on_content_tab_show:(e,hDetails)->
    @hide_content_tabs hDetails.sRoute
    @show_content_tab hDetails.sRoute
    @update_nav_items_class hDetails.sRoute


  #+2013.11.26 tuiteraz
  event_on_force_default_route:(e,hDetails) ->
    jconsole.debug "cp/data-ctrl.event_on_force_default_route()"
    @force_works_route()

  #+2013.11.26 tuiteraz
  force_works_route: ()->
    sRoute = "/#{SITE.cp.sSlug}/#{SITE.cp.data.sSlug}/#{SITE.cp.data.nav.items.works.sSlug}"
    hlprs.goto {sHref: sRoute}


  #+2013.11.27 tuiteraz
  hide_content_tabs:(sRoute)->
    $("[data-route='#{sRoute}']").parent().children(":visible").hide()

  #+2013.11.26 tuiteraz
  is_rendered: ()->
    if @is_container_rendered()
      iChildrens1 = $("##{SITE.cp.data.nav.container_id}").children().length
      iChildrens2 = $("##{SITE.cp.data.content.container_id}").children().length
      return if (iChildrens1==0)&&(iChildrens2==0) then false else true
    else
      return false

  #+2013.11.26 tuiteraz
  is_container_rendered: ()->
    jDataCntr = $("##{SITE.cp.data.container_id}")
    return if (jDataCntr.length == 0) then false else true

  #+2013.11.26 tuiteraz
  render:(sRoute)->
    me = this
    if @ctrlCp.is_signed_in()
      if !@is_rendered()
        @render_async ->
          me.bind_events()
          me.update_nav_items_class sRoute, true
      else
        me.update_nav_items_class sRoute, true

  #+2013.11.26 tuiteraz
  render_async: (fnCallback, iTry=0)->
    me = this
    @iRenderIntrvlId ||= 0
    jDataCntr = $("##{SITE.cp.data.container_id}")
    if (jDataCntr.length == 0) && (@iRenderIntrvlId==0)
      @iRenderIntrvlId = setInterval ()->
        iTry += 1
        jconsole.debug "cp/data-ctrl.render() wait for main container render complete(#{iTry})..."
        me.render_async fnCallback, iTry
      , 108
    else if (jDataCntr.length == 1)
      clearInterval @iRenderIntrvlId if @iRenderIntrvlId!=0
      @render_sync()
      fnCallback()

  #+2013.11.27 tuiteraz
  render_sync: ()->
    sLogosRoute           = cp_hlprs.get_object_route SITE.cp.data.nav.items.logos
    sWorksRoute           = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    sMusicRoute           = cp_hlprs.get_object_route SITE.cp.data.nav.items.music
    sCreativeArchiveRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department
    sTextPiecesRoute      = cp_hlprs.get_object_route SITE.cp.data.nav.items.text_pieces

    $("##{SITE.cp.data.nav.container_id}").empty().append data_tmpl.render_nav()
    $("##{SITE.cp.data.content.container_id}").empty().append data_tmpl.render_content_logos(sLogosRoute)
    $("##{SITE.cp.data.content.container_id}").append data_tmpl.render_content_works(sWorksRoute)
    $("##{SITE.cp.data.content.container_id}").append data_tmpl.render_content_music(sMusicRoute)
    $("##{SITE.cp.data.content.container_id}").append data_tmpl.render_content_art_department(sCreativeArchiveRoute)
    $("##{SITE.cp.data.content.container_id}").append data_tmpl.render_content_text_pieces(sTextPiecesRoute)

  #+2013.11.27 tuiteraz
  show_content_tab:(sRoute)->
    jconsole.debug "cp/data-ctrl.show_content_tab()"

    sObjectSlug = _.strRightBack sRoute,"/"
    hNavItem = cp_hlprs.detect_section_nav_item_by_slug SITE.cp.data.nav.items, sObjectSlug
    if hNavItem.controller
      if _.isFunction hNavItem.controller.show_tab
        _this = hNavItem.controller._this
        hNavItem.controller.show_tab.call _this

  # +2013.11.26 tuiteraz
  update_nav_items_class: (sCurrHref, bDelayed = false)->
    me = this
    @iIntrvlId ||= 0
    jLink = $("a[href='#{sCurrHref}']")
    if (jLink.length==0) && bDelayed && (@iIntrvlId==0)
      #  сюда зайдем только один раз чтобы запустить интервальную проверку
      @iIntrvlId = setInterval ()->
        me.update_nav_items_class sCurrHref
      , 300
    else if jLink.length==1
      jNav = jLink.parents '.nav'
      jNav.children('li.active').removeClass 'active'
      jLink.parent().addClass 'active'

      clearInterval @iIntrvlId if @iIntrvlId!=0

