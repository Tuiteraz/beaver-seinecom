define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  'crossroads'
  "models/art-department-photo-model"
], (
  jconsole
  tmpl
  hlprs
  cp_hlprs
  common_tmpl
  data_tmpl
  async
  crossroads
  ArtDepPhotoModel
) ->
  jconsole.info "cp/data/art-department-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlData)->
    jconsole.debug "cp/data/art-department-ctrl.init()"

    @init_models()
    @bind_events(true)

  #+2013.11.28 tuiteraz
  init_models: ()->
    @mArtDepPhoto  = new ArtDepPhotoModel SITE.hMongoDB.hModels.hArtDepartmentPhoto

  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_content_tab_hide(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.hide_tab
        @event_on_content_tab_update_item(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.update_content
        @event_on_content_tab_refresh(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.refresh


  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this

    sArtDepRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department
    $(".cp-content-tab[data-route='#{sArtDepRoute}'] .cp-content-group").sortable {
      items: ".cp-content-item"
      stop: (e,ui) =>
        sPhoneSlug = $(ui.item).parents(".cp-content-group").attr 'data-phone-slug'
        @update_order(sPhoneSlug)
    }

  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events()


#--( EVENTS

  #+2013.12.9 tuiteraz
  event_on_content_tab_hide: (e,hDetails) ->
    @hide_tab()

  #+2013.12.12 tuiteraz
  event_on_content_tab_update_item: (e,hDetails) ->
    send_event_to ctrlItem,hDetails

  #+2013.12.13 tuiteraz
  event_on_content_tab_refresh: (e,hDetails) ->
    @refresh()
#--) EVENTS


#--( INTERFACES

  #+2013.12.17 tuiteraz
  add_item: (hDetails)->
    sPhoneSlug = $(hDetails.jThis).attr "data-phone-slug"
    jconsole.debug "cp/data/art-department-ctrl.add_item(#{sPhoneSlug})"

    @mArtDepPhoto.create sPhoneSlug,(hRes)=>
      hlprs.status.show_results hRes, "Art department photo successfully added"
      @append_content(hRes.aData) if hRes.iStatus == 200

  # redirect to ctrlItem.event_on_edit_remove_item
  #+2013.12.7 tuiteraz
  remove_item:(sItemId) ->
    jconsole.debug "cp/data/art-department-ctrl.remove_item(#{sItemId}) - pending"
    @mArtDepPhoto.remove {_id:sItemId},{}, (hRes)=>
      hlprs.status.show_results hRes, "Art department photo succesfully removed"
      if hRes.iStatus == 200
        $("##{sItemId}").fadeOut 'fast', ->
          $(this).remove()

#--) INTERFACES

  #+2013.12.17 tuiteraz
  append_content: (aData) ->
    jconsole.debug "cp/data/art-department-ctrl.append_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body .cp-content-group[data-phone-slug='#{aData.sPhoneSlug}']")
    sHtml = data_tmpl.render_content_item_art_dep_photo(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mArtDepPhoto.get hQuery,{
      sSort: "iOrder"
    }, fnCallback

  #+2013.12.7 tuiteraz
  refresh: ->
    jconsole.debug "cp/data/art-department-ctrl.refresh()"
    @get_data {}, (hRes)=>
      if hRes.iStatus != 200
        jconsole.error hRes.sMessage
        return

      @render_content(hRes.aData)



  #+2013.12.7 tuiteraz
  render_content: (aData,fnCallback=null) ->
    jconsole.debug "cp/data/art-department-ctrl.render_content()"

    me = this
    sRoute   = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department
    aData    = [aData] if !_.isArray aData
    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")

    sHtml = ""

    hBtnAdd     = SITE.cp.data.content.buttons.add

    _.each SITE.cp.data.content.art_department_photo.aPhones, (hPhone)->
      sHeaderHtml = tmpl.div "btn-group","",[
        tmpl.button hBtnAdd.sClass,"","data-phone-slug='#{hPhone.sSlug}' data-content-action='#{hBtnAdd.sAction}'",[ tmpl.fa_icon(hBtnAdd.sIcon) ]
      ]

      sItemsHtml = ""
      aPhonePhotos = _.where aData, {sPhoneSlug:hPhone.sSlug}
      if _.size(aPhonePhotos) > 0
        _.each aPhonePhotos, (hItem,iIdx) ->
          sItemsHtml += data_tmpl.render_content_item_art_dep_photo(hItem)

      sHtml += tmpl.div 'cp-content-group row', "","data-phone-slug='#{hPhone.sSlug}'", [
        tmpl.div "cp-content-group-header","","",[
          tmpl.h 3,hPhone.sTitle
          sHeaderHtml
        ]
        sItemsHtml
      ]

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.debug "cp/data/art-department-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0


  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.debug "cp/data/art-department-ctrl.hide_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department
    $("[data-route='#{sRoute}']").hide()

  #+2013.12.13 tuiteraz
  update_order: (sPhoneSlug) ->
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.art_department
    aItemOrders = []
    $(".cp-content-tab[data-route='#{sRoute}'] .cp-content-group[data-phone-slug='#{sPhoneSlug}'] .cp-content-item").each (iIdx) ->
      sId = $(this).attr 'id'
      aItemOrders.push {_id: sId, iOrder : iIdx}

    async.each aItemOrders, (hItemOrder, fnCallback)=>
      @mArtDepPhoto.update {_id:hItemOrder._id},{iOrder:hItemOrder.iOrder},{}, (hRes)=>
        if hRes.iStatus != 200
          fnCallback hRes.sMessage
        else
          fnCallback()

    , (sErr)->
      jconsole.error "art-department-ctrl.update_order() : #{sErr}" if sErr

