define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/client-logo-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    common_tmpl,data_tmpl,
    async
    ClientLogoModel
) ->
  jconsole.info "cp/data/client-logos-ctrl"

  #+2013.11.29 tuiteraz
  init: ->
    @init_models()

  #+2013.11.28 tuiteraz
  init_models: ()->
    @mClientLogo  = new ClientLogoModel SITE.hMongoDB.hModels.hClientLogo


  #+2013.11.28 tuiteraz
  add_item: ->
    jconsole.debug "cp/data/client-logos-ctrl.add_item()"
    @mClientLogo.create (hRes)=>
      hlprs.status.show_results hRes, "Client logo successfully added"
      @append_content(hRes.aData) if hRes.iStatus == 200

  #+2013.12.3 tuiteraz
  after_render_filter:->
    me = this

    sLogosRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.logos
    $(".cp-content-tab[data-route='#{sLogosRoute}']").sortable {
      items: ".cp-content-item"
      stop: (e,ui) ->
        # jconsole.debug "client-logos-ctrl.after_render_filter()"
        me.update_order()
    }

  #+2013.12.2 tuiteraz
  append_content: (aData) ->
    jconsole.debug "cp/data/client-logos-ctrl.append_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.logos

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")
    sHtml = data_tmpl.render_content_item_logos(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2013.12.2 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mClientLogo.get hQuery,{
      sPopulateProperties:"File"
      sSort: "iOrder"
    }, fnCallback

  #+2013.11.28 tuiteraz
  refresh: ->
    jconsole.debug "cp/data/client-logos-ctrl.refresh()"
    @get_data {}, (hRes)=>
      hlprs.status.show_results hRes
      @render_content(hRes.aData) if hRes.iStatus == 200


  #+2013.12.2 tuiteraz
  render_content: (aData,fnCallback=null) ->
    me = this
    jconsole.debug "cp/data/client-logos-ctrl.render_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.logos

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.render_content_item_logos(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.render_content_item_logos(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.3 tuiteraz
  remove_item:(sItemId) ->
    jconsole.debug "cp/data/client-logos-ctrl.remove_item(#{sItemId})"
    @mClientLogo.remove {_id:sItemId},{}, (hRes)=>
      hlprs.status.show_results hRes, "Client logo succesfully removed"

      if hRes.iStatus == 200
        $("##{sItemId}").fadeOut 'fast', ->
          $(this).remove()

  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.debug "cp/data/client-logos-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.logos

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        hlprs.status.show_results hRes

        if hRes.iStatus ==200
          @render_content hRes.aData, ()->
            $("[data-route='#{sRoute}']:hidden").show()
            $.scrollTo 0, 0

  #+2013.12.4 tuiteraz
  update_order: ->
    sLogosRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.logos
    aClLogosOrder = []
    $(".cp-content-tab[data-route='#{sLogosRoute}'] .cp-content-item").each (iIdx) ->
      sId = $(this).attr 'id'
      aClLogosOrder.push {_id: sId, iOrder : iIdx}

    async.each aClLogosOrder, (hClLogoOrder, fnCallback)=>
      @mClientLogo.update {_id:hClLogoOrder._id},{iOrder:hClLogoOrder.iOrder},{}, (hRes)=>
        if hRes.iStatus != 200
          fnCallback hRes
        else
          fnCallback()

    , (hErr)->
      if hErr
        hErr.sMessage = "client-logos-ctrl.update_order() : #{hErr.sMessage}"
        hRes = _.clone hErr
      else
        hRes=
          iStatus : 200

      hlprs.status.show_results hRes, "Client logos order succesfully updated"

