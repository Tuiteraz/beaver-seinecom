define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/music-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    common_tmpl,data_tmpl,
    async
    MusicModel
) ->
  jconsole.info "cp/data/music-ctrl"

  #+2013.11.29 tuiteraz
  init: ->
    @init_models()

  #+2013.11.28 tuiteraz
  init_models: ()->
    @mMusic  = new MusicModel SITE.hMongoDB.hModels.hMusic

  #+2013.12.4 tuiteraz
  bind_html_events: ()->
    sMusicRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.music
    $(".cp-content-tab[data-route='#{sMusicRoute}']").sortable {
      items: ".cp-content-item"
      stop: (e,ui) =>
        @update_order()
    }

    $(".cp-content-tab[data-route='#{sMusicRoute}'] .form-group input").unbind('keypress').keypress ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = SITE.cp.data.content.buttons.update
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')


  #+2013.11.28 tuiteraz
  add_item: ->
    jconsole.debug "cp/data/music-ctrl.add_item()"

    @mMusic.create (hRes)=>
      hlprs.status.show_results hRes, "New music file succesfully added"
      @append_content(hRes.aData) if hRes.iStatus == 200


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events()

  #+2013.12.4 tuiteraz
  append_content: (aData) ->
    jconsole.debug "cp/data/music-ctrl.append_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.music

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")
    sHtml = data_tmpl.render_content_item_music(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2013.12.4 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mMusic.get hQuery,{
      sPopulateProperties:"File"
      sSort: "iOrder"
    }, fnCallback

  #+2013.11.28 tuiteraz
  refresh: ->
    jconsole.debug "cp/data/music-ctrl.refresh()"
    @get_data {}, (hRes)=>
      hlprs.status.show_results hRes
      @render_content(hRes.aData) if hRes.iStatus == 200

  #+2013.12.4 tuiteraz
  render_content: (aData,fnCallback=null) ->
    me = this

    jconsole.debug "cp/data/music-ctrl.render_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.music

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.render_content_item_music(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.render_content_item_music(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.4 tuiteraz
  remove_item:(sItemId) ->
    jconsole.debug "cp/data/music-ctrl.remove_item(#{sItemId})"
    @mMusic.remove {_id:sItemId},{}, (hRes)=>
      hlprs.status.show_results hRes,"Music file succesfully removed"

      if hRes.iStatus == 200
        $("##{sItemId}").fadeOut 'fast', ->
          $(this).remove()


  #+2013.12.4 tuiteraz
  show_tab:()->
    jconsole.debug "cp/data/music-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.music

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0

  #+2013.12.4 tuiteraz
  update_content: (sItemId) ->
    hMusicTitle = SITE.cp.data.content.music.elements.hMusicTitle
    hBtnUpdate     = SITE.cp.data.content.buttons.update

    sTitle = $("##{sItemId} input.#{hMusicTitle.sClass}").attr 'value'

    @mMusic.update {_id:sItemId},{sTitle:sTitle},{}, (hRes)=>
      hlprs.status.show_results hRes,"Music title succesfully updated"
      if hRes.iStatus == 200
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()


  #+2013.12.4 tuiteraz
  update_order: ->
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.music
    aItemOrders = []
    $(".cp-content-tab[data-route='#{sRoute}'] .cp-content-item").each (iIdx) ->
      sId = $(this).attr 'id'
      aItemOrders.push {_id: sId, iOrder : iIdx}

    async.each aItemOrders, (hItemOrder, fnCallback)=>
      @mMusic.update {_id:hItemOrder._id},{iOrder:hItemOrder.iOrder},{}, (hRes)=>
        if hRes.iStatus != 200
          fnCallback hRes
        else
          fnCallback()

    , (hErr)->
      if hErr
        hErr.sMessage = "music-ctrl.update_order() : #{sErr}"
        hRes = _.clone hErr
      else
        hRes=
          iStatus : 200

      hlprs.status.show_results hRes, "Music order succesfully updated"

