define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  "models/text-piece-model"
], (jconsole,
    tmpl,hlprs,cp_hlprs
    common_tmpl,data_tmpl,
    async
    TextPieceModel
) ->
  jconsole.info "cp/data/text-pieces-ctrl"

  #+2013.11.29 tuiteraz
  init: ->
    @init_models()

  #+2013.11.28 tuiteraz
  init_models: ()->
    @mTextPiece  = new TextPieceModel SITE.hMongoDB.hModels.hTextPiece

  #+2013.12.4 tuiteraz
  bind_html_events: ()->
    sTextPiecesRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.text_pieces
#    $(".cp-content-tab[data-route='#{sTextPiecesRoute}']").sortable {
#      items: ".cp-content-item"
#      stop: (e,ui) =>
#        @update_order()
#    }

    $(".cp-content-tab[data-route='#{sTextPiecesRoute}'] .form-group input").unbind('keypress').keypress ()->
      jItem = $(this).parents(".cp-content-item")
      sItemId = jItem.attr 'id'
      hBtnUpdate     = SITE.cp.data.content.buttons.update
      $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeIn('slow')


  #+2013.11.28 tuiteraz
  add_item: ->
    jconsole.debug "cp/data/text-pieces-ctrl.add_item()"

    @mTextPiece.create {},(hRes)=>
      hlprs.status.show_results hRes, "New text piece succesfully added"
      @append_content(hRes.aData) if hRes.iStatus == 200


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events()

  #+2013.12.4 tuiteraz
  append_content: (aData) ->
    jconsole.debug "cp/data/text-pieces-ctrl.append_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.text_pieces

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")
    sHtml = data_tmpl.render_content_item_text_piece(aData)
    jCntBody.append sHtml

    @after_render_filter()

  #+2013.12.4 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mTextPiece.get hQuery,{
      sSort: "sSlug"
    }, fnCallback

  #+2013.12.17 tuiteraz
  get_html_data: (sItemId)->
    hPropSlug    = SITE.cp.data.content.text_pieces.elements.hSlug
    hPropContent = SITE.cp.data.content.text_pieces.elements.hContent

    hData =
      sSlug    : $("##{sItemId} .#{hPropSlug.sClass}").prop 'value'
      sContent : $("##{sItemId} .#{hPropContent.sClass}").prop 'value'

  #+2013.11.28 tuiteraz
  refresh: ->
    jconsole.debug "cp/data/text-pieces-ctrl.refresh()"
    @get_data {}, (hRes)=>
      hlprs.status.show_results hRes
      @render_content(hRes.aData) if hRes.iStatus == 200

  #+2013.12.4 tuiteraz
  render_content: (aData,fnCallback=null) ->
    me = this

    jconsole.debug "cp/data/text-pieces-ctrl.render_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.text_pieces

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")

    sHtml = ""
    if !_.isArray aData
      sHtml = data_tmpl.render_content_item_text_piece(aData)
    else
      _.each aData, (hItem,iIdx) ->
        sHtml += data_tmpl.render_content_item_text_piece(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.4 tuiteraz
  remove_item:(sItemId) ->
    jconsole.debug "cp/data/text-pieces-ctrl.remove_item(#{sItemId})"
    @mTextPiece.remove {_id:sItemId},{}, (hRes)=>
      hlprs.status.show_results hRes,"Text piece succesfully removed"

      if hRes.iStatus == 200
        $("##{sItemId}").fadeOut 'fast', ->
          $(this).remove()


  #+2013.12.4 tuiteraz
  show_tab:()->
    jconsole.debug "cp/data/text-pieces-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.text_pieces

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0

  #+2013.12.4 tuiteraz
  update_content: (sItemId) ->
    hBtnUpdate  = SITE.cp.data.content.buttons.update

    hData = @get_html_data(sItemId)

    @mTextPiece.update {_id:sItemId},hData,{}, (hRes)=>
      hlprs.status.show_results hRes,"Text piece succesfully updated"
      if hRes.iStatus == 200
        $("##{sItemId} [data-content-action='#{hBtnUpdate.sAction}']").fadeOut()


  #+2013.12.4 tuiteraz
#  update_order: ->
#    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.text_pieces
#    aItemOrders = []
#    $(".cp-content-tab[data-route='#{sRoute}'] .cp-content-item").each (iIdx) ->
#      sId = $(this).attr 'id'
#      aItemOrders.push {_id: sId, iOrder : iIdx}
#
#    async.each aItemOrders, (hItemOrder, fnCallback)=>
#      @mTextPiece.update {_id:hItemOrder._id},{iOrder:hItemOrder.iOrder},{}, (hRes)=>
#        if hRes.iStatus != 200
#          fnCallback hRes
#        else
#          fnCallback()
#
#    , (hErr)->
#      if hErr
#        hErr.sMessage = "music-ctrl.update_order() : #{sErr}"
#        hRes = _.clone hErr
#      else
#        hRes=
#          iStatus : 200
#
#      hlprs.status.show_results hRes, "Music order succesfully updated"

