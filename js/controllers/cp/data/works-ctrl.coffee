define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  "async"
  'crossroads'
  "models/work-model"
  "./works/item-ctrl"
], (
  jconsole
  tmpl
  hlprs
  cp_hlprs
  common_tmpl
  data_tmpl
  async
  crossroads
  WorkModel
  ctrlItem
) ->
  jconsole.info "cp/data/works-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlData)->
    jconsole.debug "cp/data/works-ctrl.init()"


    @Router = crossroads.create()
    @Router.ignoreState = true

    # все вызовы @ctrlData.Router.parse() будут перенаправляться дополнительно на @Router
    @ctrlData.ctrlCp.Router.pipe @Router

    ctrlItem.init this

    @init_models()
    @init_routes()
    @bind_events(true)

  #+2013.11.28 tuiteraz
  init_models: ()->
    @mWork  = new WorkModel SITE.hMongoDB.hModels.hWork

  #+2013.12.7 tuiteraz
  init_routes: ()->
    me = this

  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_content_tab_hide(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.hide_tab
        @event_on_content_tab_update_item(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.update_content
        @event_on_content_tab_refresh(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.refresh


    if !bMdlOnly
      @bind_html_events()

  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this

    sWorksRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    $(".cp-content-tab[data-route='#{sWorksRoute}']").sortable {
      items: ".cp-content-item"
      stop: (e,ui) =>
        @update_order()
    }

  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events()


#--( EVENTS

  #+2013.12.9 tuiteraz
  event_on_content_tab_hide: (e,hDetails) ->
    @hide_tab()

  #+2013.12.12 tuiteraz
  event_on_content_tab_update_item: (e,hDetails) ->
    send_event_to ctrlItem,hDetails

  #+2013.12.13 tuiteraz
  event_on_content_tab_refresh: (e,hDetails) ->
    @refresh()
#--) EVENTS


#--( INTERFACES

  # redirect to ctrlItem.event_on_edit_add_item
  #+2013.11.28 tuiteraz
  add_item: ->
    jconsole.debug "cp/data/works-ctrl.add_item()"
    send_event_to ctrlItem, {sAction: SITE.actions.cp.content.add_item}

  # redirect to ctrlItem.event_on_edit_remove_item
  #+2013.12.7 tuiteraz
  remove_item:(sItemId) ->
    jconsole.debug "cp/data/works-ctrl.remove_item(#{sItemId})"
    send_event_to ctrlItem, {sAction: SITE.actions.cp.content.remove_item, sItemId: sItemId}

#--) INTERFACES


  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mWork.get hQuery,{
      sSort: "iOrder"
    }, fnCallback

  #+2013.12.7 tuiteraz
  refresh: ->
    jconsole.debug "cp/data/works-ctrl.refresh()"
    @get_data {}, (hRes)=>
      if hRes.iStatus != 200
        jconsole.error hRes.sMessage
        return

      @render_content(hRes.aData)



  #+2013.12.7 tuiteraz
  render_content: (aData,fnCallback=null) ->
    me = this

    jconsole.debug "cp/data/works-ctrl.render_content()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works

    jCntBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")

    sHtml = ""

    aData = [aData] if !_.isArray aData

    aPortfolioData = _.where aData, {bInArchive:false}
    if _.size(aPortfolioData) > 0
      sHtml += tmpl.div 'row', "", [
        tmpl.h 3,"Portfolio works"
      ]
      _.each aPortfolioData, (hItem,iIdx) ->
        sHtml += data_tmpl.render_content_item_work(hItem)

    aArchiveData = _.where aData, {bInArchive:true}
    if _.size(aArchiveData) > 0
      sHtml += tmpl.div 'row', "", [
        tmpl.h 3,"Archive works"
      ]
      _.each aArchiveData, (hItem,iIdx) ->
        sHtml += data_tmpl.render_content_item_work(hItem)

    jCntBody.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback

  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.debug "cp/data/works-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        if hRes.iStatus !=200
          jconsole.error hRes.sMessage
          return

        @render_content hRes.aData, ()->
          $("[data-route='#{sRoute}']:hidden").show()
          $.scrollTo 0, 0


  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.debug "cp/data/works-ctrl.hide_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    $("[data-route='#{sRoute}']").hide()

  #+2013.12.13 tuiteraz
  update_order: ->
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    aItemOrders = []
    $(".cp-content-tab[data-route='#{sRoute}'] .cp-content-item").each (iIdx) ->
      sId = $(this).attr 'id'
      aItemOrders.push {_id: sId, iOrder : iIdx}

    async.each aItemOrders, (hItemOrder, fnCallback)=>
      @mWork.update {_id:hItemOrder._id},{iOrder:hItemOrder.iOrder},{}, (hRes)=>
        if hRes.iStatus != 200
          fnCallback hRes.sMessage
        else
          fnCallback()

    , (sErr)->
      jconsole.error "works-ctrl.update_order() : #{sErr}" if sErr

