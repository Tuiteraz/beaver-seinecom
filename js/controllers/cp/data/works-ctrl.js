// Generated by CoffeeScript 1.12.7
(function() {
  define(['bconsole', 'libs/beaver/templates', 'helpers/helpers', 'helpers/cp-helpers', 'templates/common-tmpl', 'templates/cp/data-tmpl', "async", 'crossroads', "models/work-model", "./works/item-ctrl"], function(jconsole, tmpl, hlprs, cp_hlprs, common_tmpl, data_tmpl, async, crossroads, WorkModel, ctrlItem) {
    jconsole.info("cp/data/works-ctrl");
    return {
      init: function(ctrlData) {
        this.ctrlData = ctrlData;
        jconsole.debug("cp/data/works-ctrl.init()");
        this.Router = crossroads.create();
        this.Router.ignoreState = true;
        this.ctrlData.ctrlCp.Router.pipe(this.Router);
        ctrlItem.init(this);
        this.init_models();
        this.init_routes();
        return this.bind_events(true);
      },
      init_models: function() {
        return this.mWork = new WorkModel(SITE.hMongoDB.hModels.hWork);
      },
      init_routes: function() {
        var me;
        return me = this;
      },
      bind_events: function(bMdlOnly) {
        var me;
        if (bMdlOnly == null) {
          bMdlOnly = false;
        }
        me = this;
        $(me).unbind('click').click(function(e, hDetails) {
          if (defined(hDetails.sAction)) {
            if (hDetails.sAction === SITE.actions.cp.content.hide_tab) {
              this.event_on_content_tab_hide(e, hDetails);
            }
            if (hDetails.sAction === SITE.actions.cp.content.update_content) {
              this.event_on_content_tab_update_item(e, hDetails);
            }
            if (hDetails.sAction === SITE.actions.cp.content.refresh) {
              return this.event_on_content_tab_refresh(e, hDetails);
            }
          }
        });
        if (!bMdlOnly) {
          return this.bind_html_events();
        }
      },
      bind_html_events: function() {
        var me, sWorksRoute;
        me = this;
        sWorksRoute = cp_hlprs.get_object_route(SITE.cp.data.nav.items.works);
        return $(".cp-content-tab[data-route='" + sWorksRoute + "']").sortable({
          items: ".cp-content-item",
          stop: (function(_this) {
            return function(e, ui) {
              return _this.update_order();
            };
          })(this)
        });
      },
      after_render_filter: function() {
        return this.bind_html_events();
      },
      event_on_content_tab_hide: function(e, hDetails) {
        return this.hide_tab();
      },
      event_on_content_tab_update_item: function(e, hDetails) {
        return send_event_to(ctrlItem, hDetails);
      },
      event_on_content_tab_refresh: function(e, hDetails) {
        return this.refresh();
      },
      add_item: function() {
        jconsole.debug("cp/data/works-ctrl.add_item()");
        return send_event_to(ctrlItem, {
          sAction: SITE.actions.cp.content.add_item
        });
      },
      remove_item: function(sItemId) {
        jconsole.debug("cp/data/works-ctrl.remove_item(" + sItemId + ")");
        return send_event_to(ctrlItem, {
          sAction: SITE.actions.cp.content.remove_item,
          sItemId: sItemId
        });
      },
      get_data: function(hQuery, fnCallback) {
        if (hQuery == null) {
          hQuery = {};
        }
        return this.mWork.get(hQuery, {
          sSort: "iOrder"
        }, fnCallback);
      },
      refresh: function() {
        jconsole.debug("cp/data/works-ctrl.refresh()");
        return this.get_data({}, (function(_this) {
          return function(hRes) {
            if (hRes.iStatus !== 200) {
              jconsole.error(hRes.sMessage);
              return;
            }
            return _this.render_content(hRes.aData);
          };
        })(this));
      },
      render_content: function(aData, fnCallback) {
        var aArchiveData, aPortfolioData, jCntBody, me, sHtml, sRoute;
        if (fnCallback == null) {
          fnCallback = null;
        }
        me = this;
        jconsole.debug("cp/data/works-ctrl.render_content()");
        sRoute = cp_hlprs.get_object_route(SITE.cp.data.nav.items.works);
        jCntBody = $("[data-route='" + sRoute + "'] .cp-content-tab-body");
        sHtml = "";
        if (!_.isArray(aData)) {
          aData = [aData];
        }
        aPortfolioData = _.where(aData, {
          bInArchive: false
        });
        if (_.size(aPortfolioData) > 0) {
          sHtml += tmpl.div('row', "", [tmpl.h(3, "Portfolio works")]);
          _.each(aPortfolioData, function(hItem, iIdx) {
            return sHtml += data_tmpl.render_content_item_work(hItem);
          });
        }
        aArchiveData = _.where(aData, {
          bInArchive: true
        });
        if (_.size(aArchiveData) > 0) {
          sHtml += tmpl.div('row', "", [tmpl.h(3, "Archive works")]);
          _.each(aArchiveData, function(hItem, iIdx) {
            return sHtml += data_tmpl.render_content_item_work(hItem);
          });
        }
        return jCntBody.fadeOut('fast', function() {
          $(this).empty().append(sHtml).fadeIn('fast', (function(_this) {
            return function() {
              return me.after_render_filter();
            };
          })(this));
          return hlprs.call_if_function(fnCallback);
        });
      },
      show_tab: function() {
        var sRoute;
        jconsole.debug("cp/data/works-ctrl.show_tab()");
        sRoute = cp_hlprs.get_object_route(SITE.cp.data.nav.items.works);
        if (!cp_hlprs.is_content_tab_body_empty(sRoute)) {
          $("[data-route='" + sRoute + "']:hidden").show();
          return $.scrollTo(0, 0);
        } else {
          return this.get_data({}, (function(_this) {
            return function(hRes) {
              if (hRes.iStatus !== 200) {
                jconsole.error(hRes.sMessage);
                return;
              }
              return _this.render_content(hRes.aData, function() {
                $("[data-route='" + sRoute + "']:hidden").show();
                return $.scrollTo(0, 0);
              });
            };
          })(this));
        }
      },
      hide_tab: function() {
        var sRoute;
        jconsole.debug("cp/data/works-ctrl.hide_tab()");
        sRoute = cp_hlprs.get_object_route(SITE.cp.data.nav.items.works);
        return $("[data-route='" + sRoute + "']").hide();
      },
      update_order: function() {
        var aItemOrders, sRoute;
        sRoute = cp_hlprs.get_object_route(SITE.cp.data.nav.items.works);
        aItemOrders = [];
        $(".cp-content-tab[data-route='" + sRoute + "'] .cp-content-item").each(function(iIdx) {
          var sId;
          sId = $(this).attr('id');
          return aItemOrders.push({
            _id: sId,
            iOrder: iIdx
          });
        });
        return async.each(aItemOrders, (function(_this) {
          return function(hItemOrder, fnCallback) {
            return _this.mWork.update({
              _id: hItemOrder._id
            }, {
              iOrder: hItemOrder.iOrder
            }, {}, function(hRes) {
              if (hRes.iStatus !== 200) {
                return fnCallback(hRes.sMessage);
              } else {
                return fnCallback();
              }
            });
          };
        })(this), function(sErr) {
          if (sErr) {
            return jconsole.error("works-ctrl.update_order() : " + sErr);
          }
        });
      }
    };
  });

}).call(this);

//# sourceMappingURL=works-ctrl.js.map
