define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/cp-helpers'
  'helpers/work-item-helpers'
  'templates/common-tmpl'
  'templates/cp/data-tmpl'
  'templates/cp/data/item-tmpl'
  "async"
  'crossroads'
  "models/work-model"
  "models/file-model"
], (
  jconsole
  tmpl
  hlprs
  cp_hlprs
  wi_hlprs
  common_tmpl
  data_tmpl
  item_tmpl
  async
  crossroads
  WorkModel
  FileModel
) ->
  jconsole.info "cp/data/works/item-ctrl"

  #+2013.11.7 tuiteraz
  init:(@ctrlWorks)->
    jconsole.debug "cp/data/works/item-ctrl.init()"

    @Router = crossroads.create()
    @Router.ignoreState = true

    # все вызовы @ctrlData.Router.parse() будут перенаправляться дополнительно на @Router
    @ctrlWorks.ctrlData.ctrlCp.Router.pipe @Router

    @init_models()
    @init_routes()
    @bind_events(true)

  #+2013.11.28 tuiteraz
  init_models: ()->
    @mWork  = new WorkModel SITE.hMongoDB.hModels.hWork

  #+2013.12.7 tuiteraz
  init_routes: ()->
    me = this

    sWorksRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    @Router.addRoute "#{sWorksRoute}/{sItemId}/edit", (sItemId)=>
      sRoute = "#{sWorksRoute}/#{sItemId}/edit"
      jconsole.debug "DATA-WORKS-ROUTE: #{sRoute}"
      send_event_to me, {sAction:SITE.actions.cp.content.edit_item, sItemId: sItemId}


  #+2013.12.7 tuiteraz
  bind_events:(bMdlOnly=false)->
    me = this

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        @event_on_add_item(e,hDetails)    if hDetails.sAction == SITE.actions.cp.content.add_item
        @event_on_edit_item(e,hDetails)   if hDetails.sAction == SITE.actions.cp.content.edit_item
        @event_on_remove_item(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.remove_item
        @event_on_go_back(e,hDetails)     if hDetails.sAction == SITE.actions.cp.content.go_back
        @event_on_update_item(e,hDetails)      if hDetails.sAction == SITE.actions.cp.content.update_content

        @event_on_item_add_row(e,hDetails)     if hDetails.sAction == SITE.actions.cp.content.add_row
        @event_on_item_remove_row(e,hDetails)  if hDetails.sAction == SITE.actions.cp.content.remove_row
        @event_on_item_add_picture(e,hDetails) if hDetails.sAction == SITE.actions.cp.content.add_picture
        @event_on_item_add_video(e,hDetails)   if hDetails.sAction == SITE.actions.cp.content.add_video

    if !bMdlOnly
      @bind_html_events()

  # using @hItem filled at event_on_edit_item()
  #+2013.12.7 tuiteraz
  bind_html_events:->
    me = this

    sSelector = ".cp-content-tab-body.edit-item button[data-item-action]"
    $(document).undelegate(sSelector,'click').delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-item-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

  #+2013.12.11 tuiteraz
  add_row: (sTmplName,fnCallback) ->
    hRow = @create_row sTmplName

    @hItem.aContent.push hRow

    hOptions =
      aArrayProperties          : "aContent"
      sArrayPropertiesOperation : "push"

    @mWork.update {_id : @hItem._id},{aContent : hRow},hOptions, (hRes)->
      hlprs.status.show_results hRes,"","item-ctrl.add_row(): #{hRes.sMessage}"
      if hRes.iStatus == 200
        fnCallback(hRow)

  #+2013.12.12 tuiteraz
  add_header_picture:(sPropertyName,fnCallback) ->
    jconsole.debug "add_header_picture(#{sPropertyName})"

    mFile  = new FileModel SITE.hMongoDB.hModels.hFile
    mFile.create (hRes) =>
      if hRes.iStatus != 200
        hlprs.status.show_results hRes,"","add_header_picture() -> mFile.create() - #{hRes.sMessage}"
        fnCallback hRes
      else
        sFileIdToRemove =  @hItem[sPropertyName]
        if sFileIdToRemove
          mFile  = new FileModel SITE.hMongoDB.hModels.hFile
          mFile.remove {_id: sFileIdToRemove}, (hRes)->
            hlprs.status.show_results hRes,"","add_header_picture() -> mFile.remove(#{sFileIdToRemove}) - #{hRes.sMessage}"

        sFileId = hRes.aData._id

        @hItem[sPropertyName] = sFileId
        hData = {}
        hData[sPropertyName] = sFileId

        @mWork.update {_id: @hItem._id},hData,{}, (hRes) ->
          hlprs.status.show_results hRes,"","add_header_picture() -> mWork.update() - #{hRes.sMessage}"

          if hRes.iStatus != 200
            fnCallback hRes
          else
            hRes =
              iStatus : 200
              sPropertyName: sPropertyName
              sFileId: sFileId
            fnCallback hRes

  #+2013.12.12 tuiteraz
  add_row_cell_picture:(sRowId, sCellId,fnCallback) ->
    jconsole.debug "add_row_cell_picture(#{sRowId}, #{sCellId})"

    hCellScope = wi_hlprs.get_cell_scope @hItem,sRowId,sCellId
    if hCellScope
      mFile  = new FileModel SITE.hMongoDB.hModels.hFile
      mFile.create (hRes) =>
        hlprs.status.show_results hRes, "", "add_row_cell_picture() -> mFile.create() - #{hRes.sMessage}"

        if hRes.iStatus != 200
          fnCallback hRes

        else
          if wi_hlprs.is_cell_content_type_picture hCellScope.hCell
            sFileToRemoveId = hCellScope.hCell.sContent
            mFile  = new FileModel SITE.hMongoDB.hModels.hFile
            mFile.remove {_id: sFileToRemoveId}, (hRes)->
              hlprs.status.show_results hRes, "", "add_row_cell_picture() -> mFile.remove(#{sFileToRemoveId}) - #{hRes.sMessage}"

          sFileId = hRes.aData._id
          hCellScope.hCell.sContentType = SITE.cp.data.content.works.cell.hContentTypes.picture
          hCellScope.hCell.sContent = sFileId

          @hItem.aContent[hCellScope.iRowIdx].aCells[hCellScope.iCellIdx] = _.clone hCellScope.hCell
          @mWork.update_row {_id:@hItem._id},{id: hCellScope.hRow.id},@hItem.aContent[hCellScope.iRowIdx], (hRes)->
            hlprs.status.show_results hRes,"", "add_row_cell_picture() -> mWork.update_row() - #{hRes.sMessage}"
            fnCallback hRes

    else
      hRes =
        iStatus : 409
        sMessage : "add_row_cell_picture() : can't find row & cell"

      hlprs.status.show_results hRes
      fnCallback hRes

  #+2013.12.16 tuiteraz
  add_row_cell_video:(sRowId, sCellId,fnCallback) ->
    jconsole.debug "add_row_cell_video(#{sRowId}, #{sCellId}) - in progress"

    hCellScope = wi_hlprs.get_cell_scope @hItem,sRowId,sCellId
    if hCellScope
      cp_hlprs.modal_input.show "textarea","Cell video html code", hCellScope.hCell,(hRes) =>
        # callback fired only on modal submit btn click
        hlprs.status.show_results hRes
        if hRes.iStatus == 200
          if wi_hlprs.is_cell_content_type_picture hCellScope.hCell
            #remove prevoius sContent -> file id
            sFileToRemoveId = hCellScope.hCell.sContent
            mFile  = new FileModel SITE.hMongoDB.hModels.hFile
            mFile.remove {_id: sFileToRemoveId}, (hRes)->
              hlprs.status.show_results hRes, "", "add_row_cell_video() -> mFile.remove(#{sFileToRemoveId}) - #{hRes.sMessage}"

          hCellScope.hCell.sContent = hRes.sContent
          hCellScope.hCell.sContentType = SITE.cp.data.content.works.cell.hContentTypes.video

          @hItem.aContent[hCellScope.iRowIdx].aCells[hCellScope.iCellIdx] = _.clone hCellScope.hCell
          @mWork.update_row {_id:@hItem._id},{id: hCellScope.hRow.id},@hItem.aContent[hCellScope.iRowIdx], (hRes)->
            hlprs.status.show_results hRes,"", "add_row_cell_video() -> mWork.update_row() - #{hRes.sMessage}"
            fnCallback hRes

    else
      hRes =
        iStatus : 409
        sMessage : "add_row_cell_video() : can't find row & cell"

      hlprs.status.show_results hRes
      fnCallback hRes


  #+2013.12.4 tuiteraz
  after_render_filter:->
    @bind_html_events()

#--( EVENTS

  #+2013.11.28 tuiteraz
  event_on_add_item: ->
    jconsole.debug "cp/data/works/item-ctrl.event_on_add_item()"
    @mWork.create {},(hRes)=>
      if hRes.iStatus != 200
        hlprs.status.show_results hRes,"","cp/data/works/item-ctrl.add_item() : #{hRes.sMessage}"
      else
        sEditRoute = cp_hlprs.get_edit_item_route SITE.cp.data.nav.items.works, hRes.aData._id
        async.parallel [
          (fnNext)->
            hlprs.goto {sHref:sEditRoute}
            fnNext()
          (fnNext)=>
            send_event_to @ctrlWorks, {sAction:SITE.actions.cp.content.refresh}
            fnNext()
        ],(sErr,aRes) ->
          if sErr
            hlprs.status.show sErr,'danger'
          else
            hlprs.status.show "New portfolio work succesfully created"


  #+2013.12.7 tuiteraz
  event_on_edit_item: (e,hDetails)->
    jconsole.debug "cp/data/works/item-ctrl.event_on_edit_item(#{hDetails.sItemId})"
    sRoute = cp_hlprs.get_edit_item_route SITE.cp.data.nav.items.works, hDetails.sItemId

    async.parallel [
      (fnCallback)=>
        send_event_to @ctrlWorks, {sAction: SITE.actions.cp.content.hide_tab}
        fnCallback()
      (fnCallback)=>
        @get_data {_id:hDetails.sItemId}, (hRes)=>
          if hRes.iStatus !=200
            jconsole.error hRes.sMessage
            fnCallback()
          else if _.size(hRes.aData)==1
            @hItem = hRes.aData[0]
            @render_content hRes.aData, ()->
              $("[data-route='#{sRoute}']:hidden").show()
              $.scrollTo 0, 0
              fnCallback()
          else
            sWorksRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
            hlprs.goto {sHref:sWorksRoute}

    ]

  #+2013.12.9 tuiteraz
  event_on_go_back:(e,hDetails) ->
    sWorksRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    hlprs.goto {sHref:sWorksRoute}

  #+2013.12.7 tuiteraz
  event_on_remove_item:(e,hDetails) ->
    jconsole.debug "cp/data/works/item-ctrl.event_on_remove_item(#{hDetails.sItemId})"
    @mWork.remove {_id:hDetails.sItemId},{}, (hRes)=>
      if hRes.iStatus != 200
        hlprs.status.show hRes.sMessage,'danger'
      else
        @remove_html hDetails.sItemId, ->
          sWorksRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
          hlprs.goto {sHref:sWorksRoute}
          hlprs.status.show "Item succesfully removed"

  #+2013.12.12 tuiteraz
  event_on_update_item:(e,hDetails) ->
    jconsole.debug "cp/data/works/item-ctrl.event_on_update_item() - in progress"

    hData = @get_html_data()

    @mWork.update {_id : @hItem._id},hData,{}, (hRes) =>
      hlprs.status.show_results hRes,"Item data updated","event_on_update_item(): #{hRes.sMessage}"
      send_event_to(@ctrlWorks,{sAction:SITE.actions.cp.content.refresh}) if hRes.iStatus == 200


#--) EVENTS

#--( ITEM EVENTS

  #+2013.12.11 tuiteraz
  event_on_item_add_row: (e,hDetails) ->
    sTmplName = hDetails.jThis.attr "data-row-tmpl"
    #jconsole.debug "event_on_item_add_row('#{sTmplName}') - pending"

    @add_row sTmplName, (hRow)->
      jContentCntr = $(".cp-content-tab-body.edit-item .panel-body:visible")
      jContentCntr.append item_tmpl.render_content_row(hRow)
      $.scrollTo 0,$("##{hRow.id}:visible")

  #+2013.12.12 tuiteraz
  event_on_item_add_picture: (e,hDetails) ->
    jconsole.debug "/cp/data/works/item-ctrl.event_on_item_add_picture()"
    jBtn = hDetails.jThis

    if jBtn.parents(".row-cell-panel-container").length == 1
      sCellId = jBtn.parents(".row-cell").prop 'id'
      sRowId = jBtn.parents(".row").prop 'id'
      @add_row_cell_picture sRowId, sCellId, (hRes)=>
        # @hItem.aContent updated with new content
        if hRes.iStatus == 200
          hCellScope = wi_hlprs.get_cell_scope(@hItem,sRowId,sCellId)
          wi_hlprs.update_cell_html_content hCellScope

    else if jBtn.parents(".thumbnail").length == 1
      sPropertyName = jBtn.parents(".thumbnail").attr 'data-property-name'
      @add_header_picture sPropertyName, (hRes) =>
        if hRes.iStatus == 200
          sEditRoute = cp_hlprs.get_edit_item_route SITE.cp.data.nav.items.works, @hItem._id
          sSelector = "[data-route='#{sEditRoute}'] [data-property-name='#{hRes.sPropertyName}']"
          $(sSelector).contents().children("img").prop 'src', "/db/file/#{hRes.sFileId}"
    else
      hlprs.status.show "unknown btn position",'danger'
      jconsole.error "unknown btn position"

  #+2013.12.16 tuiteraz
  event_on_item_add_video:(e,hDetails) ->
    jconsole.debug "/cp/data/works/item-ctrl.event_on_item_add_video()"
    jBtn = hDetails.jThis

    if jBtn.parents(".row-cell-panel-container").length == 1
      sCellId = jBtn.parents(".row-cell").prop 'id'
      sRowId = jBtn.parents(".row").prop 'id'
      @add_row_cell_video sRowId, sCellId, (hRes)=>
        # @hItem.aContent updated with new content
        if hRes.iStatus == 200
          hCellScope = wi_hlprs.get_cell_scope(@hItem,sRowId,sCellId)
          wi_hlprs.update_cell_html_content hCellScope

    else
      hlprs.status.show "unknown btn position",'danger'
      jconsole.error "unknown btn position"


  #+2013.12.11 tuiteraz
  event_on_item_remove_row: (e,hDetails) ->
    sRowId = hDetails.jThis.parents('.row').attr 'id'
    jconsole.debug "event_on_item_remove_row(#{sRowId}) - pending"

    @remove_row sRowId, ->
      $("##{sRowId}").slideUp 'slow',->
        $(this).remove()


#--) ITEM EVENTS

  # creating only json data structure
  #+2013.12.11 tuiteraz
  create_row:(sTmplName) ->
    hRes =
      id: ObjectId()
      aCells: []

    if      sTmplName == "single-cell"
      hRes.aCells.push {
        id: ObjectId()
        sContentType: ""
        sContent:""
      }
    else if sTmplName == "double-cell"
      hRes.aCells.push {
        id: ObjectId()
        sContentType: ""
        sContent:""
      }
      hRes.aCells.push {
        id: ObjectId()
        sContentType: ""
        sContent:""
      }

    return hRes

  #+2013.12.7 tuiteraz
  get_data: (hQuery={},fnCallback) ->
    @mWork.get hQuery,{
      sSort: "iOrder"
    }, fnCallback

  #+2013.12.12 tuiteraz
  get_html_data: ->
    hPropTitle     = SITE.cp.data.content.works.item.elements.hTitle
    hPropDesc      = SITE.cp.data.content.works.item.elements.hDescription
    bPropInArchive = SITE.cp.data.content.works.item.elements.hInArchive

    hData =
      sTitle       : $("##{@hItem._id}-#{hPropTitle.sId}").prop 'value'
      sDescription : $("##{@hItem._id}-#{hPropDesc.sId}").prop 'value'
      bInArchive   : $("##{@hItem._id}-#{bPropInArchive.sId}").prop 'checked'

  #+2013.12.12 tuiteraz
  remove_html: (sItemId,fnCallback) ->
    me = this

    async.parallel [
      (fnNext)->
        jCntr = $("##{sItemId}")
        if jCntr.length ==1
          jCntr.fadeOut 'fast', ->
            $(this).remove()
            fnNext()
        else
          fnNext()

      (fnNext)->
        sEditRoute = cp_hlprs.get_edit_item_route SITE.cp.data.nav.items.works, sItemId
        jCntr = $("[data-route='#{sEditRoute}']")
        if jCntr.length ==1
          jCntr.fadeOut 'fast', ->
            $(this).remove()
            fnNext()
        else
          fnNext()

    ], ->
      fnCallback()


  #+2013.12.11 tuiteraz
  remove_row:(sRowId, fnCallback) ->
    @remove_row_cells_files sRowId,(hRes)=>
      if hRes.iStatus ==200
        hRow = _.findWhere @hItem.aContent, {id:sRowId}
        iRowCntIdx = _.indexOf @hItem.aContent,hRow

        @hItem.aContent.splice iRowCntIdx,1

        hOptions =
          aArrayProperties          : ["aContent"]
          sArrayPropertiesOperation : "remove"

        @mWork.update {_id : @hItem._id},{aContent : hRow},hOptions, (hRes)->
          hlprs.status.show_results hRes,"","remove_row(): #{hRes.sMessage}"
          fnCallback() if hRes.iStatus == 200

  #+2013.12.13 tuiteraz
  remove_row_cells_files: (sRowId,fnCallback) ->
    hRow = _.findWhere @hItem.aContent, {id:sRowId}
    aFnParallel = []
    _.each hRow.aCells,(hCell) ->
      if wi_hlprs.is_cell_content_type_picture(hCell)
        aFnParallel.push (fnNext)->
          mFile =  new FileModel SITE.hMongoDB.hModels.hFile
          mFile.remove {_id: hCell.sContent},{}, (hRes)->
            if hRes.iStatus != 200
              hRes.sMessage = "remove_row_cells_files() mFile.remove(#{hCell.sContent}) : #{hRes.sMessage}"
              fnNext hRes
            else
              fnNext()
    async.parallel aFnParallel,(hErr,aRes) ->
      if hErr
        hlprs.status.show_results hErr
        fnCallback hErr
      else
        fnCallback {iStatus:200,aData:aRes}


  #+2013.12.7 tuiteraz
  render_content: (aData,fnCallback=null) ->
    me = this

    jconsole.debug "cp/data/works/item-ctrl.render_content()"
    sRoute = cp_hlprs.get_edit_item_route SITE.cp.data.nav.items.works, aData[0]._id

    if $("[data-route='#{sRoute}']").length == 0
      $("##{SITE.cp.data.content.container_id}").append item_tmpl.render_tab(sRoute)

    jTabCntr = $("[data-route='#{sRoute}']")

    sHtml = item_tmpl.render(aData[0])

    jTabCntr.fadeOut 'fast', ->
      $(this).empty().append(sHtml).fadeIn 'fast', =>
        me.after_render_filter()
      hlprs.call_if_function fnCallback



  #+2013.12.2 tuiteraz
  show_tab:()->
    jconsole.debug "cp/data/works/item-ctrl.show_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works

    if !cp_hlprs.is_content_tab_body_empty(sRoute)
      $("[data-route='#{sRoute}']:hidden").show()
      $.scrollTo 0, 0
    else
      @get_data {}, (hRes)=>
        hlprs.status.show_results hRes
        if hRes.iStatus ==200
          @render_content hRes.aData, ()->
            $("[data-route='#{sRoute}']:hidden").show()
            $.scrollTo 0, 0


  #+2013.12.2 tuiteraz
  hide_tab:()->
    jconsole.debug "cp/data/works/item-ctrl.hide_tab()"
    sRoute = cp_hlprs.get_object_route SITE.cp.data.nav.items.works
    $("[data-route='#{sRoute}']:hidden").show()


