define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'templates/common-tmpl'
  "async"
  'templates/cp/login-tmpl'
  'lightbeam-helpers'
  'libs/lightbeam/tools/auth'

], (jconsole,
    tmpl,hlprs,img_hlprs,
    common_tmpl
    async
    login_tmpl
    lb_hlprs
    lb_auth
) ->
  jconsole.info "cp/login-ctrl"

  #+2013.1126 tuiteraz
  init:(@ctrlCp)->
    jconsole.debug "login-ctrl.init()"

    @bind_events(true) # привяжем только события модуля, html оставим на потом

  #+2013.11.9 tuiteraz
  bind_events:(bMdlOnly=false) ->
    me = this

    $(me).unbind('click').click (e,hDetails)->
      if defined hDetails.sAction
        @event_on_signed_out(e,hDetails) if hDetails.sAction == SITE.actions.cp.signed_out


    if !bMdlOnly
      @bind_html_events()


  #+2013.11.26 tuiteraz
  bind_html_events: () ->
    me = this

    hBtnLogin = LIGHTBEAM.auth.form.buttons.login
    $("##{hBtnLogin.id}").unbind('click').click (e)->
      hEmail  = LIGHTBEAM.auth.form.elements.email
      hPasswd = LIGHTBEAM.auth.form.elements.password

      sEmail = $("##{hEmail.id}").attr "value"
      sPasswd = $("##{hPasswd.id}").attr "value"

      if _.isEmpty(sEmail) || _.isEmpty(sPasswd)
        $(".form-errors").slideUp().hide().empty().append(tmpl.div "alert alert-danger", [
          "login and password can't be empty"
        ]).slideDown()
      else
        hReq =
          sEmail: sEmail
          sPassword: sPasswd

        lb_auth.sign_in hReq, (hRes)->
          if hRes.iStatus == 200
            $("#login-form-container").fadeOut ->
              send_event_to me.ctrlCp, {sAction: SITE.actions.cp.signed_in, hAuthStatus:hRes}
          else
            $(".form-errors").slideUp().empty().hide().append(tmpl.div "alert alert-danger", [
              hRes.sMessage
            ]).slideDown()


  #+2013.11.9 tuiteraz
  render: ->
    $('body').removeClass().addClass("body-subtle-bg").empty().append login_tmpl.render()
    $("#login-container").css {height:$(window).height()}

  #+2013.11.26 tuiteraz
  event_on_signed_out: (e,hDetails) ->
    jconsole.debug "cp/loging-ctrl.event_on_signed_out()"
    @render()
    @bind_events()



