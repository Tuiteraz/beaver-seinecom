define [
  'bconsole'
  'controllers/site/navigation-ctrl'
  'controllers/site/content-ctrl'
  'controllers/site/player-ctrl'
  'helpers/helpers'
  'video-js'
], (
  jconsole,
  navigation,content,player,
  site_hlprs
) ->
  jconsole.info "site-ctrl"

  #+2013.10.30 tuiteraz
  init: () ->
    window.sJvmConsoleLogContext = "SITE"
    jconsole.debug "site-ctrl.init()"

    site_hlprs.load_stylesheet "/css/site.css", ->
      navigation.init ->
        content.init navigation, ->
          content.render()
          content.bind_events()
          navigation.render()
          navigation.bind_events()

          if !is_ie(8) && !is_ie(9)
            player.init navigation, ->
              player.render()
              player.bind_events()

