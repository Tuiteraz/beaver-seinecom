define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/nav-item-helpers'
  'templates/common-tmpl'
  './content/index-ctrl'
  './content/intro-ctrl'
  './content/work-ctrl'
  'async'
], (jconsole,tmpl,hlprs,img_hlprs,nav_item_hlprs,common_tmpl,index,intro,work,async) ->

  jconsole.info "content-ctrl"

  init: (@ctxNavigation,fnCallback=null)->
    jconsole.debug "content-ctrl.init()"
    me = this
    async.series [
      (fnNext)->
        intro.init me
        fnNext()

      (fnNext)->
        index.init me,->
          fnNext()
    ], (sErr,aRes)->
      fnCallback() if _.isFunction fnCallback


  #+2013.10.12 tuiteraz
  bind_events: ->
    me = this
    # interval height check and resize main containers: intro,page
    @check_content_height()
    setInterval =>
      @check_content_height()
    , 500

    index.bind_events()

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        send_event_to(index, hDetails) if hDetails.sAction == SITE.actions.close_modal_photo_container
        send_event_to(me.ctxNavigation, hDetails) if hDetails.sAction == SITE.actions.goto_nav_link



  #+2013.10.12 tuiteraz
  check_content_height:->
    iWndHeight = $(window).height()
    intro.set_height iWndHeight
    iArtHeight = Math.max iWndHeight,700
    me = this

    iIndexContainerHeight = 0
    $("##{SITE.index.container_id} article").each ->
      sId = $(this).attr 'id'
      hNavItem = nav_item_hlprs.detect_by_attr me.ctxNavigation.aNavItems, {id:sId}
      if hNavItem.iContainerHeight
        iCurrArtHeight = hNavItem.iContainerHeight
      else
        iCurrArtHeight = iArtHeight

      if $(this).height != iCurrArtHeight
        $(this).css {height: iCurrArtHeight}

      iIndexContainerHeight += iCurrArtHeight

    $("##{SITE.index.container_id}").css {height: iIndexContainerHeight}

  #+2013.10.12 tuiteraz
  hide: (fnCallback=null) ->
#    index.stop_video() if !is_ie()
    $("##{SITE.footer.container_id}").hide()
    $("##{SITE.content_container_id}").fadeOut 'fast', ->
      $("body").removeClass "padding-top"
      $("body > .container").removeClass 'glow'
      fnCallback() if _.isFunction fnCallback

  #+2013.10.22 tuiteraz
  hide_index:(fnCallback=null) ->
    index.hide fnCallback

  hide_intro:(fnCallback=null) ->
    intro.hide fnCallback

  #+2013.10.22 tuiteraz
  hide_works: (fnCallback=null) ->
    work.hide fnCallback

  render: ->
    jconsole.debug "content-ctrl.render() : start"
    jconsole.debug "content-ctrl.render() - scaffolding ..."
    common_tmpl.render_scaffolding()
    jconsole.debug "content-ctrl.render() - intro ..."
    intro.render()
    jconsole.debug "content-ctrl.render() - index ..."
    index.render()
    jconsole.debug "content-ctrl.render() : finish"


  #+2013.10.12 tuiteraz
  show: (fnCallback=null) ->
    #jconsole.debug "content-ctrl: show() fnCallback is func=#{bIsFn}"
    me = this

    $("body").addClass "padding-top"
    $("body > .container").addClass 'glow'
    $("##{SITE.content_container_id}").fadeIn ->
      $("##{SITE.index.container_id}").fadeIn ->
        img_hlprs.jail_for "##{ me.ctxNavigation.hCurrNavItem.id }"
        fnCallback() if _.isFunction(fnCallback)

    $("##{SITE.footer.container_id}").fadeIn()
#    index.play_video() if !is_ie()


  #+2013.10.12 tuiteraz
  show_index: (fnCallback=null) ->
    @show(fnCallback)

  #+2013.10.12 tuiteraz
  show_intro: ->
    intro.show()

  #+2013.10.22 tuiteraz
  show_work:(sWorkId) ->
    $("body").addClass 'padding-top'
    $("body > .container").addClass 'glow'
    $("##{SITE.content_container_id}").fadeIn ->
      work.hide_all()
      $("##{SITE.works_container_id}").fadeIn ->
        work.show sWorkId

    $("##{SITE.footer.container_id}").fadeIn()



