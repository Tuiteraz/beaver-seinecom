define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'templates/common-tmpl'
  'templates/index/about-us-tmpl'
  'templates/index/art-department-tmpl'
  'templates/index/clients-tmpl'
  'templates/index/contacts-tmpl'
  'templates/index/creative-archive-tmpl'
  'templates/index/works-tmpl'
  'async'
  "models/art-department-photo-model"
  "models/text-piece-model"
], (jconsole,tmpl,hlprs,img_hlprs,common_tmpl,
  about_us_tmpl
  art_department_tmpl
  clients_tmpl
  contacts_tmpl
  creative_archive_tmpl
  works_tmpl
  async
  ArtDepPhotoModel
  TextPieceModel
) ->

  me = this
  jconsole.info "index-ctrl"

  #+2013.10.16 tuiteraz
  init: (@ctxContent,fnCallback) ->
    @init_models()

    async.series [
      (fnNext)=>
        @mArtDepPhoto.get {},{sSort:"iOrder"}, (hRes)=>
          if hRes.iStatus == 200
            @aArtDepPhotos = hRes.aData

          fnNext()

      (fnNext)=>
        @mTextPiece.get {},{sSort:"sSlug"}, (hRes)=>
          if hRes.iStatus == 200
            @aTextPieces = hRes.aData

          fnNext()

    ], (hErr,aRes) ->
      fnCallback()

  #+2013.12.18 tuiteraz
  init_models: ()->
    @mArtDepPhoto  = new ArtDepPhotoModel SITE.hMongoDB.hModels.hArtDepartmentPhoto
    @mTextPiece    = new TextPieceModel SITE.hMongoDB.hModels.hTextPiece


  #+2013.10.18
  bind_events: ->
    #$("section.text div").draggable()
    me = this

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        if hDetails.sAction == SITE.actions.close_modal_photo_container
          sPhoneId = $(".modal-photo-container:visible").attr 'data-phone-id'
          me.event_on_modal_photo_close_click(sPhoneId)


    $.stellar.positionProperty.test1 = {
      setPosition: ($element, iNewLeft, iOriginalLeft, iNewTop, iOriginalTop)->
        if $element.hasClass 'title-text'

          # -left
          # <0
          iNewLeft = (iNewTop-iOriginalTop)*(-1)
          iNewLeft = Math.min iNewLeft, 0

          # -opacity
          if Math.abs(iNewLeft) < 108
            fNewOpacity = 1 - Math.abs(iNewLeft/108)
          else
            fNewOpacity = 0

          fNewOpacity = 1 if _.isNaN iNewTop

          $element.css {
            'transform': "translate3d(#{iNewLeft}px,0px,0)"
            'opacity': fNewOpacity
          }

        else
          $element.css 'transform', "translate3d(#{iNewLeft - iOriginalLeft}px,#{iNewTop-iOriginalTop}px,0)"
    }

    $.stellar {
      positionProperty : 'test1'
      horizontalScrolling : false
      verticalScrolling : true
      hideDistantElements : false
    }

    $("##{SITE.index.works.carousel_id}").jcarousel {
      scroll: 1
      wrap: 'circular'
    }
    $("##{SITE.index.creative_archive.carousel_id}").jcarousel {
      scroll: 1
      wrap: 'circular'
    }

    $(".map-marker").click (e) =>
      @event_on_map_marker_click()
      e.preventDefault()

    $(document).delegate ".map .title .close","click", (e) =>
      @event_on_map_close_click()
      e.preventDefault()
    $(document).delegate ".map","click", (e) =>
      @event_on_map_close_click()
      e.preventDefault()
    $(document).delegate ".map .title .print","click", (e) =>
      @event_on_map_print_click()
      e.preventDefault()


    $(".art-department-phone").hover ->
      $(this).children('img').stop().transition {scale: SITE.index.art_department.phone.fImgHoverScale}
      $(".art-department-phone[id!='#{ $(this).attr('id') }']").stop().transition {opacity:0.3}
    , ->
      $(this).children('img').stop().transition {scale: SITE.index.art_department.phone.fImgNormalScale}
      $(".art-department-phone[id!='#{ $(this).attr('id') }']").stop().transition {opacity:1}

    $(".art-department-phone").click (e)->
      me.event_on_phone_click $(this).attr 'id'
      e.preventDefault()

    $(document).delegate ".modal-photo-container .close", "click", (e)->
      me.event_on_modal_photo_close_click $(this).parents(".modal-photo-container").attr('data-phone-id')
      e.preventDefault()

    $(".rs-slider").refineSlide {
      maxWidth: SITE.iInnerContentWidth
      useArrows: false
      transition: "fade"
      thumbMargin : 1
      onInit: ->
        jSlider = this
        sId = jSlider.slider.$slider.attr 'id'
        $(document).delegate "##{sId} a:visible", "click", (e) ->
          jSlider.slider.next()
          e.preventDefault()
      afterChange: ->
        me.check_slider_img_height()
    }

    setInterval ->
      sSelector = "img.lazy[data-src]:visible"
      if $(sSelector).length > 0
        img_hlprs.jail_hidden_for "img.lazy[data-src]:visible",false
    , 1000


    $(".jcarousel-prev").click (e)->
      jconsole.debug "prev click"
      sId = $(this).parents("article").attr 'id'
      img_hlprs.jail_hidden_for "##{sId}"
    $(".jcarousel-next").click (e)->
      sId = $(this).parents("article").attr 'id'
      img_hlprs.jail_hidden_for "##{sId}"

#    $("#index-about-video").unbind('click').click (e)->
#      if !is_ie()
#        jVideo = videojs("index-about-video")
#        if jVideo.paused()
#          jVideo.play()
#        else
#          jVideo.pause()


  #+2013.11.25 tuiteraz
  check_slider_img_height: ->
    # check img size to fit window
    iHeight = $(window).height()
    iThmbWrapHeight =  $(".rs-thumb-wrap:first").height()
    iMdlPhTitle = $(".modal-photo-container > .title:first").height()
    iImgSize = iHeight - iThmbWrapHeight - SITE.iNavbarHeight - iMdlPhTitle - 10

    $(".rs-slider .rs-slide-image").css {maxHeight:iImgSize}
    $(".rs-slider").css {height:iImgSize}


  #+2013.10.18 tuiteraz
  event_on_map_close_click: ->
    jMapCntr = $("#map-container")
    jMap = $("#map-container .map")

#    jMap.transition {opacity: 0, perspective: 300, rotate3d: '-1,1,0,-10deg', scale:0.2}, 1500, ->
#      jMapCntr.fadeOut 'fast', ->
#        hlprs.enable_scroll()
    jMap.fadeOut 'fast', ->
      jMapCntr.fadeOut 'fast', ->
        hlprs.enable_scroll()


  #+2013.10.18 tuiteraz
  event_on_map_print_click: ->
    jImg = $("#map-container img")
    jWnd = window.open jImg.attr 'src'
    $(jWnd.document).ready ->
      setTimeout ->
        jWnd.window.print()
      ,2000

  #+2013.10.18 tuiteraz
  event_on_map_marker_click: ->
#    if $("#map-container").length == 0
#      $("body").append common_tmpl.render_map()

    iTop    = $(window).scrollTop()
    iHeight = $(window).height()
    iWidth  = $(window).width()
#    hCss = {
#      top: iTop
#      height: iHeight
#      width: iWidth
#    }
    hlprs.disable_scroll()
    style = "style='top:#{iTop}px; height:#{iHeight}px; width:#{iWidth}px;'"
    $("#map-container").remove()
    $("body").append common_tmpl.render_map(style)

#    jMapCntr = $("#map-container")
#    jMap = $("#map-container .map")
#    jMap.css { opacity:0, perspective: 300, rotate3d: '-1,1,0,10deg', scale: 0.2 }
#    jMapCntr.css(hCss).fadeIn 'fast', ->
#      jMap.transition {opacity: 1, perspective: 300, rotate3d: '0,1,0,0deg', scale:1}, 1500

  #+2013.10.21 tuiteraz
  event_on_modal_photo_close_click: (sPhoneId) ->
    jModalPhotoCntr = $(".modal-photo-container[data-phone-id='#{sPhoneId}']")
    jModalPhotoCntr.fadeOut ->
      hlprs.enable_scroll()
      me.show_player()


  #+2013.10.20 tuiteraz
  event_on_phone_click: (sPhoneId) ->
    async.parallel [
      ()=>
        # send event to nav-ctrl via content-ctrl to activate art-dep nav link
        jArtDepLink = $("[href='/art-department#']")
        sHref = jArtDepLink.attr('href')
        sDataNavLink = jArtDepLink.attr('data-nav-link')
        send_event_to @ctxContent, {sDataNavLink:sDataNavLink,sHref: sHref, sAction: SITE.actions.goto_nav_link}
      ()=>
        @hide_player()
      ()=>
        jSliderCtnr = $("[data-phone-id='#{sPhoneId}']")
        hlprs.disable_scroll()

        iTop    = $(window).scrollTop()
        iHeight = $(window).height()
        iWidth  = $(window).width()
        hCss = {
          top: 0
          height: iHeight
          width: iWidth
        }

        jSliderCtnr.css hCss

        @check_slider_img_height()

        jSliderCtnr.fadeIn ->
          $(".modal-photo-container[data-phone-id='#{sPhoneId}'] img.lazy").jail()

    ], (sErr,aResults)->
      jconsole.error sErr if sErr
      jconsole.dir aResults if aResults



  #+2013.10.22 tuiteraz
  hide: (fnCallback=null) ->
#    @stop_video() if !is_ie()
    $("##{SITE.index.container_id}").fadeOut 'fast', ->
      fnCallback() if _.isFunction fnCallback

  #+2013.11.12 tuiteraz
  hide_player: ->
    $("##{SITE.player.container_id}").hide()


  # Page(string) = id
  # Page(hash) = hPage{}
  # +2013.7.17 tuiteraz
  render: (Page) ->
    $("##{SITE.index.container_id}").append about_us_tmpl.render @ctxContent
    @ctxContent.check_content_height()
    $("##{SITE.index.container_id}").append works_tmpl.render @ctxContent
    @ctxContent.check_content_height()
    $("##{SITE.index.container_id}").append clients_tmpl.render @ctxContent
    @ctxContent.check_content_height()
    $("##{SITE.index.container_id}").append creative_archive_tmpl.render @ctxContent
    @ctxContent.check_content_height()
    $("##{SITE.index.container_id}").append art_department_tmpl.render @ctxContent, @aArtDepPhotos
    @ctxContent.check_content_height()
    $("##{SITE.index.container_id}").append contacts_tmpl.render @ctxContent, @aTextPieces

  #+2013.11.12 tuiteraz
  show_player: ->
    $("##{SITE.player.container_id}").show()

  #+2013.12.23 tuiteraz
  stop_video: ->
    jVideo = videojs("index-about-video")
    try
      jVideo.pause() if _.isFunction(jVideo.pause)
    catch e



  #+2013.12.23 tuiteraz
  play_video: ->
    jVideo = videojs("index-about-video")
    try
      jVideo.play() if _.isFunction(jVideo.play)
    catch e


