define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/nav-item-helpers'
  'templates/common-tmpl'
], (jconsole,tmpl,hlprs,nav_item_hlprs,common_tmpl) ->

  jconsole.info "intro-ctrl"

  #+2013.10.16
  init: (@ctxContent) ->


  #+2013.10.12 tuiteraz
  hide:(fnCallback=null)->
    $("##{SITE.intro.container_id}").stop().fadeOut 'fast', ->
      $("body > .container").removeClass "intro"
      fnCallback() if _.isFunction fnCallback


  #+2013.10.12 tuiteraz
  render: ->
    hNavItemIndex = nav_item_hlprs.detect_by_attr @ctxContent.ctxNavigation.aNavItems, {bIsIndex:true}
    sHrefIndex = nav_item_hlprs.get_href_for hNavItemIndex
    $("##{SITE.intro.container_id}").empty().css({display:'none'}).append tmpl.div "","intro-content","align='center'", [
      """
        <div style="display: block; left: -185px; width: 287px; position: relative; cursor: pointer;">
            <a href="/index#">
              <img src="/images/logo-intro-2.png"  />
            </a>
        </div>
        <div style="display: block; left: 185px; top: -12px; width: 291px; position: relative">
            <a href="http://www.trampoline.su/">
                <img src="/images/tramplin-logo-en.png" style="display: block;margin-bottom:20px;" />
            </a>
        </div>
      """
    ]


  set_height:(iHeight) ->
    $("##{SITE.intro.container_id}").css {height:iHeight}

  #+2013.10.12 tuiteraz
  show:->
    $("body > .container").addClass("intro")
    $("##{SITE.intro.container_id}").stop().fadeIn 1000, ->
      $(this).css {display:'table'}

  #+2013.10.12 tuiteraz
  is_visible:->
    $("##{SITE.intro.container_id}:visible").length == 1


