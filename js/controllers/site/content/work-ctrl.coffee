define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'templates/common-tmpl'
  'templates/work-item-tmpl'
  "models/work-model"
], (
  jconsole
  tmpl
  hlprs
  common_tmpl
  wi_tmpl
  WorkModel
) ->

  jconsole.info "work-ctrl"

  # +2013.7.16 tuiteraz
  before_filter: ->
    @aWorks ||= []
    @init_models()

  # отправить заспро на сервер для получения страницы
  # +2013.7.16 tuiteraz
  get: (hQuery,fnCallback) ->
    jconsole.warn "work-ctrl: get()"

    @mWork.get hQuery,{sSort: "iOrder"}, fnCallback

  # +2013.7.19 tuiteraz
  get_and_push: (hQuery,fnCallback) ->
    @get hQuery, (hRes)=>
      if hRes.iStatus == 200
        if _.isArray hRes.aData
          if _.size(hRes.aData) == 1
            @aWorks.push _.clone(hRes.aData[0])
          else
            @aWorks.push _.clone(hWork) for hWork in hRes.aData
        else
          @aWorks.push hRes.aData

        fnCallback hRes.aData
      else
        fnCallback(null)

  # Query(string) = id
  # Query(Object) = {id,sSlug ...}
  # +2013.7.16 tuiteraz
  find: (Query,fnCallback) ->
    @before_filter()

    if typeof Query == 'string'
      hQuery = {_id:Query}
    else
      hQuery = Query

    hWork = []
    hWork = _.where(@aWorks, hQuery) if _.size(@aWorks) > 0   # check local storage
    if _.isEmpty hWork      # get from server and push into local
      @get_and_push hQuery,(aRes)->
        fnCallback aRes[0]
    else
      fnCallback hWork[0]

  #+2013.10.22 tuiteraz
  hide: (fnCallback=null) ->
    $("##{SITE.works_container_id}").fadeOut 'fast', ->
      $(".work").hide()
      fnCallback() if _.isFunction fnCallback

  #+2013.10.22 tuiteraz
  hide_all: () ->
    $(".work").hide()

  #+2013.12.18 tuiteraz
  init_models:->
    @mWork        = new WorkModel SITE.hMongoDB.hModels.hWork

  # +2013.7.17 tuiteraz
  render: (hWork) ->
    jconsole.debug "work-ctrl: render('#{hWork.sTitle}')"

    jContainer = $("##{SITE.works_container_id}")
    jContainer.append wi_tmpl.render hWork

  #+2013.10.22 tuiteraz
  show: (sWorkId) ->
    jconsole.debug "work-ctrl: show('#{sWorkId}')"
    me = this

    @find sWorkId, (hWork)=>
      jContainer = $("##{SITE.works_container_id}")
      jContainer.fadeOut 'fast', ->
        me.render hWork if ($("##{hWork._id}").length == 0)
        me.hide_all()

        jWork = $("##{hWork._id}")
        jWork.addClass 'work-in-out-glow'

        $(this).fadeIn 'fast', ->
          jWork.fadeIn ->
            $(this).removeClass "work-in-out-glow"
