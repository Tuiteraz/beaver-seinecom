define [
  'bconsole'
  'crossroads'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/nav-item-helpers'
  'templates/common-tmpl'
  './content-ctrl'
  './player-ctrl'
  'configuration/navigation'
  'async'
], (
  jconsole
  crossroads
  tmpl,hlprs,img_hlprs,nav_item_hlprs,
  common_tmpl
  content,player
  cfgNavigation
  async
) ->

  jconsole.info "navigation-ctrl"


  # security data check when needed
  # +2013.7.16 tuiteraz
  before_filter: ->
#    @get if !defined @aNavItems


  # +2013.7.16 tuiteraz
  init: (fnCallback=null)->
    jconsole.debug "navigation-ctrl.init()"
    History.options.disableSuid = true

    async.series [
      (fnNext) =>
        @get ->
          fnNext()

      (fnNext) =>
        @init_routes()
        fnNext()

    ], (hErr,aRes) ->
      fnCallback()

  #+2013.12.17 tuiteraz
  init_routes: ->
    @Router = crossroads.create()

    @Router.addRoute /^(?:\.)?\/([а-яa-z-\d]+)?(?:#.*)?$/i, (sRoute) =>
      jconsole.log "ROUTE:#{sRoute}"
      if sRoute
        @hCurrNavItem = nav_item_hlprs.detect_by_slug @aNavItems,sRoute

        if @hCurrNavItem.bIsIndex
          @event_on_visit_index()
        else if @hCurrNavItem.bIsIntro
          @event_on_visit_intro()
        else
          @event_on_visit_index =>
            $.scrollTo 0, ( $("##{ @hCurrNavItem.id }").offset().top - SITE.iNavbarHeight )

      else
        @hCurrNavItem = nav_item_hlprs.detect_by_attr @aNavItems,{bIsIntro:true}

        @event_on_visit_intro()

      document.title = "#{SITE.sBrowserMainTitle}-#{@hCurrNavItem.sTitle}"
      jconsole.dir @hCurrNavItem

    @Router.addRoute /^(?:\.)?\/(works|creative-archive)\/([а-яa-z-\d]+)(?:#)?/i, (sObject,sRoute) =>
      jconsole.log "ROUTE: /#{sObject}/#{sRoute}"
      if sRoute
        @hCurrNavItem = nav_item_hlprs.detect_by_attr @aNavItems,{_id:sRoute}
        @update_nav_items_class $("#nav-works-drop").attr('href')
        @event_on_visit_work()
      else
        @hCurrNavItem = nav_item_hlprs.detect_by_attr @aNavItems,{bIsIndex:true}
        @event_on_visit_index()

      document.title = "#{SITE.sBrowserMainTitle}-#{@hCurrNavItem.sTitle}"
      jconsole.dir @hCurrNavItem

    @Router.addRoute /^(?:\.)?\/creative-archive\/([а-яa-z-\d]+)(?:#)?/i, (sRoute) =>
      jconsole.log "ROUTE:#{sRoute}"
      if sRoute
        @hCurrNavItem = nav_item_hlprs.detect_by_slug @aNavItems,sRoute
        @update_nav_items_class $("#nav-creative-archive-drop").attr('href')
        @event_on_visit_work()
      else
        @hCurrNavItem = nav_item_hlprs.detect_by_attr @aNavItems,{bIsIndex:true}
        @event_on_visit_index()

      jconsole.dir @hCurrNavItem


  # +2013.7.19 tuieraz
  bind_events: ->
    jconsole.debug "navigation-ctrl.bind_events()"
    me = this
    jconsole.debug "initial location parse : #{History.getState().hash}"
    @Router.parse History.getState().hash

    History.Adapter.bind window, "statechange", ->
      jconsole.debug "History.Adapter.statechange()"
      me.Router.parse History.getState().hash

    # nav item click
    $(document).delegate "a","click", (e) ->
      sHref = $(this).attr('href')
      jconsole.info "nav item click recognized: #{sHref}"

      if sHref != "#"
        sDataNavLink = $(this).attr('data-nav-link')
        me.update_nav_items_class sHref
        send_event_to me, {sDataNavLink:sDataNavLink,sHref: sHref, sAction: SITE.actions.goto_nav_link}
        e.preventDefault()

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        if hDetails.sAction == SITE.actions.goto_nav_link
          if $(".modal-photo-container:visible").length==1
            send_event_to content, {sAction : SITE.actions.close_modal_photo_container}
          hlprs.goto hDetails

    @bind_scrollspy()

    $(".dropdown-toggle").dropdownHover()

  #+2013.10.17 tuiteraz
  bind_scrollspy: ->
    me = this
    setInterval ->
      iWndScrollTop = $(window).scrollTop() + SITE.iNavbarHeight
      $("article:visible").each ->
        iTop = $(this).offset().top
        iDelta = Math.abs(iTop - iWndScrollTop)
        if iDelta <= SITE.article_scrollspy.iDelta
          sId = $(this).attr 'id'
          hNavItem = nav_item_hlprs.detect_by_attr me.aNavItems, { id: sId }
          sHref = nav_item_hlprs.get_href_for hNavItem
          me.update_nav_items_class sHref
    , SITE.article_scrollspy.iInterval


  # +2013.10.11 tuiteraz
  event_on_visit_index: (fnCallback=null) ->
    jconsole.debug "event_on_visit_index()"
    @before_filter()

    content.hide_intro =>
      content.hide_works =>
        player.show()
        @show()
        content.show_index fnCallback


  # +2013.10.11 tuiteraz
  event_on_visit_intro:  ->
    jconsole.debug "event_on_visit_intro()"
    @before_filter()

    content.hide =>
      player.hide()
      @hide =>
        content.show_intro()

  # +2013.10.11 tuiteraz
  event_on_visit_work:  ->
    jconsole.debug "event_on_visit_work()"
    @before_filter()

    content.hide_index =>
      content.hide_intro()
      player.show()
      @show()
      content.show_work @hCurrNavItem._id

  # +2013.7.30 tuiteraz
  get_current_title: ->
    return @hCurrNavItem.sTitle

  # если в параметрах указан элемент меню к которму прикреплена страница,
  # то вернем ее id
  # +2013.7.17 tuiteraz
  get_current_page_id: ->
    @before_filter()

    return @hCurrNavItem.id

  # если в параметрах указан элемент меню к которму прикреплена страница,
  # то вернем ее slug
  # +2013.7.16 tuiteraz
  get_current_page_slug: ->
    @before_filter()
    hPage = index.find @hCurrNavItem.id
    return hPage.sSlug

  # +2013.7.16 tuiteraz
  get: (fnCallback)->
    jconsole.debug "navigation-ctrl.get()"
    me = this
    cfgNavigation.get_content (hRes)=>
      @aNavItems = hRes.aData
      fnCallback()


  # выполнить имитацию перехода на главную страницу - когда первый раз загружаем сайт
  # после этого все функции работы с текущим пунктом меню будут выдавать индексную страницу
  # +2013.7.16 tuiteraz
  goto_index: ->
    @before_filter()
    hNavItem = nav_item_hlprs.detect_by_attr @aNavItems, {bIsIndex:true}
    hlprs.goto {sHref:hNavItem.sSlug}

  # +2013.10.9 tuiteraz
  goto_intro: ->
    @before_filter()
    hNavItem = nav_item_hlprs.detect_by_attr @aNavItems, {bIsIntro:true}
    hlprs.goto {sHref:hNavItem.sSlug}

  #+2013.101.2 tuiterazw
  hide: (fnCallback=null) ->
    $("##{SITE.nav_spacer_id}").hide()
    $("##{SITE.nav_container_id}").fadeOut ->
      fnCallback() if _.isFunction fnCallback

  # когда первый раз сайт открывает и в параметрах ничего нет
  # или не настроено меню
  # +2013.7.16 tuiteraz
  is_empty: ->
    @before_filter()

    if _.size(@aNavItems) > 0
      return false
    else
      return true

  # для пункта меню с прикрепленной статической страницей = true
  # +2013.7.16 tuiteraz
  is_single_page: ->
    @before_filter()
    bRes = @hCurrNavItem.bIsSinglePage
    #jconsole.info "is_single_page = #{bRes}"

    return bRes

  # +2013.7.17 tuiteraz
  render: ->
    @before_filter()

    sLiHtml = ""
    for hNavItem in @aNavItems
      if hNavItem.bShowInNav
        sId = "##{hNavItem.id}"
        sHref = nav_item_hlprs.get_href_for hNavItem
#        sParams ="data-nav-link='#{sHref}'"
        sParams =""
        sIcon = ""
        sIcon = tmpl.twbp_icon(hNavItem.sIcon) if defined hNavItem.sIcon

        if !hNavItem.bShowChildrensDropdown
          sLiHtml += tmpl.li "", [
            tmpl.a sHref, sParams, sIcon + hNavItem.sTitle
          ]
        else if hNavItem.bShowChildrensDropdown
          sDropToggleId = "#{hNavItem.id}-drop"
          sParams += " data-toggle='dropdown' data-hover='dropdown' data-delay='1000' data-close-others='true' role='button' id='#{sDropToggleId}'"

          sChildrensLiHtml = ""
          for hChildren in hNavItem.aChildrens
            sChildrenHref = nav_item_hlprs.get_href_for hChildren
            sChildrensLiHtml += tmpl.li "", "", [
              tmpl.a "/#{hNavItem.sSlug}#{sChildrenHref}","",[ hChildren.sTitle ]
            ]


          sChildrensUlHtml = tmpl.ul "dropdown-menu","","role='menu' aria-labelledby='#{sDropToggleId}'", [
            sChildrensLiHtml
          ]

          sLiHtml += tmpl.li "dropdown", [
            tmpl.a sHref, sParams, sIcon + hNavItem.sTitle,"dropdown-toggle"
            sChildrensUlHtml
          ]



    hNavItemIndex = nav_item_hlprs.detect_by_attr @aNavItems, {bIsIndex:true}
    sHrefLogo = nav_item_hlprs.get_href_for hNavItemIndex

    sHtml = tmpl.nav "navbar navbar-default", [
      tmpl.div "navbar-inner", [
        tmpl.a sHrefLogo, "", " ","brand"
        tmpl.ul "nav", [sLiHtml]
      ]
    ]
    $("##{SITE.nav_container_id}").empty().append sHtml

  #+2013.101.2 tuiterazw
  show: ->
    $("##{SITE.nav_container_id}").fadeIn()
    $("##{SITE.nav_spacer_id}").fadeIn()

  # sCurrHref - может быть главного пункта так и саб
  # +2013.7.19 tuiteraz
  # *2013.10.16 tuiteraz
  update_nav_items_class: (sCurrHref)->
    jLink = $("a[href='#{sCurrHref}']")
    $("a[href!='#{sCurrHref}']").parent().removeClass 'active'
    if jLink.parents('.dropdown-menu').length >= 1
      jLink.parents('.dropdown').addClass 'active'
    else
      jLink.parent().addClass 'active'


  # выбираем пункт меню по параметрам в урле
  # +2013.7.16 tuiteraz
  # *2013.8.29 tuiteraz
  # *2013.9.9 tuiteraz
  # *2013.9.25 tuiteraz: using History.getState().hash
  select_by_history_hash: ->
    sNavSlug = nav_item_hlprs.extract_slug_from_hash History.getState().hash
    if !defined sNavSlug
      @goto_intro()
    else
      @hCurrNavItem = nav_item_hlprs.detect_by_slug @aNavItems, sNavSlug


