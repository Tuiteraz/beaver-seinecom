define [
  'bconsole'
  'libs/beaver/templates'
  'helpers/helpers'
  'templates/common-tmpl'
  "models/music-model"
  "async"
], (
  jconsole
  tmpl
  hlprs
  common_tmpl
  MusicModel
  async
) ->

  jconsole.info "player-ctrl"

  #+2013.10.25
  init: (@ctxNavigation,fnCallback=null)->
    jconsole.debug "player-ctrl.init()"
    @init_models()
    if !is_ie(8) && !is_ie(9)
      async.series [
        (fnNext)=>
          @get_playlist (hRes)=>
            @aPlayList = hRes.aData
            fnNext()
        (fnNext)=>
          soundManager.setup {
            flashVersion: 9
            useFlashBlock: true
            url: '/js/libs/sound-manager2/swf/'
            debugMode: false
            consoleOnly: false
          }

          soundManager.onready (oStatus) =>
            return fnNext(false) if !oStatus.success

            jconsole.debug "CREATING SOUNDS"
            _.each @aPlayList, (hSong,iIdx) ->
              jconsole.debug "+ #{hSong.sTitle}"
              soundManager.createSound {
                id: "#{hSong.sTitle}",
                url: "/db/file/#{hSong.File}"
              }
            fnNext()

      ],(hErr,aRes)->
        if hErr
          fnCallback hErr
        else
          fnCallback()
    else
      fnCallback()


  #+2013.12.18 tuiteraz
  init_models: ()->
    jconsole.debug "player-ctrl.init_models()"
    @mMusic  = new MusicModel SITE.hMongoDB.hModels.hMusic

  #+2013.10.12 tuiteraz
  bind_events: ->
    me = this
    $("##{SITE.player.container_id} .list-title").click (e) ->
      $("##{SITE.player.container_id} .list").slideToggle()
      if $(this).hasClass "list-open"
        $(this).removeClass "list-open"
        $(this).addClass "list-close"
      else if $(this).hasClass "list-close"
        $(this).removeClass "list-close"
        $(this).addClass "list-open"

    $("##{SITE.player.container_id} .list li").click (e) ->
      me.play $(this).attr 'data-song-id'


  #*2013.12.18 tuiteraz
  get_playlist:(fnCallback) ->
    jconsole.debug "player-ctrl: get_playlist()"
    @mMusic.get {},{sSort: "iOrder"},(hRes)->
      fnCallback(hRes)

  #+2013.10.26 tuieraz
  get_playing_song_id: ->
    jPlaying = $("##{SITE.player.container_id} .list li[data-playing-state='playing']")
    if jPlaying.length == 1
      return jPlaying.attr "data-song-id"
    else
      jPaused = $("##{SITE.player.container_id} .list li[data-playing-state='paused']")
      if jPaused.length == 1
        return jPaused.attr "data-song-id"
      else
        return null

  #+2013.10.25
  hide: ->
    $("##{SITE.player.container_id}").hide()

  play: (Id) ->
    sPlayingId = @get_playing_song_id()
    if Id == sPlayingId
      jSound = soundManager.getSoundById(sPlayingId)
      if jSound.paused
        jSound.resume()
        @update_play_state_for sPlayingId, 'playing'
      else if jSound.playState == 1
        jSound.pause()
        @update_play_state_for sPlayingId, 'paused'
      else
        jSound.play sPlayingId
        @update_play_state_for sPlayingId, 'playing'
    else
      soundManager.stopAll()
      soundManager.play Id
      @update_play_state_for sPlayingId, 'stoped'
      @update_play_state_for Id, 'playing'

  #+2013.10.25 tuiteraz
  render: ->
    sLiHtml = ""
    _.each @aPlayList, (hSong) ->
      sLiHtml += tmpl.li "","","data-song-id='#{hSong.sTitle}'", [hSong.sTitle]

    sHtml  = tmpl.div "title","",["PLAY LIST"]
    sHtml += tmpl.div "list-title list-open","",["MUSIC THAT WE LIKE"]
    sHtml += tmpl.ul "list","",[ sLiHtml ]
    $("##{SITE.player.container_id}").append sHtml

  #2013.10.25
  show: ->
    $("##{SITE.player.container_id}").fadeIn()

  update_play_state_for: (sId,sState) ->
    jLi = $("##{SITE.player.container_id} li[data-song-id='#{sId}']")
    jLi.attr 'data-playing-state',sState

