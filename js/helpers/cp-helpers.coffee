define [
  'bconsole'
  'libs/beaver/templates'
  "async"
  'helpers/work-item-helpers'
], (
  jconsole
  tmpl
  async
  wi_hlprs
) ->
  jconsole.info "cp-helpers"

  # +2013.11.28 tuiteraz
  detect_section_nav_item_by_slug: (hNavItems,sNavSlug) ->
    hNavItem = _.findWhere hNavItems,{sSlug:sNavSlug}
    return hNavItem

  #+2013.11.27 tuiteraz
  get_object_route: (hNavItem)->
    "/#{SITE.cp.sSlug}/#{SITE.cp.data.sSlug}/#{hNavItem.sSlug}"

  #+2013.12.7 tuiteraz
  get_edit_item_route: (hNavItem,sItemId)->
    "/#{SITE.cp.sSlug}/#{SITE.cp.data.sSlug}/#{hNavItem.sSlug}/#{sItemId}/edit"

  #+2013.12.2 tuiteraz
  is_content_tab_body_empty: (sRoute) ->
    jBody = $("[data-route='#{sRoute}'] .cp-content-tab-body")
    if jBody.children().length == 0
      return true
    else
      return false

  #+2013.12.16 tuiteraz
  modal_input:
    before_filter:()->
      @sCntrId      ||= "beaver-modal-input-container"
      @sInputId     ||= "beaver-modal-input"
      @sBtnSubmitId ||= "beaver-modal-btn-submit"

    show: (sInputType,sTitle="Modal input",hCell,fnCallback)->
      @before_filter()

      @render(sInputType,sTitle,hCell)
      $("##{@sCntrId}").modal {
        show: true
      }
      $("##{@sCntrId}").modal 'show'
      $("##{@sCntrId}").on 'shown.bs.modal', (e)=>
        $("##{@sInputId}").focus()


      $("##{@sBtnSubmitId}").click (e) =>
        @hide()
        hRes =
          iStatus : 200
          sContent: $("##{@sInputId}").prop 'value'
        fnCallback hRes

    hide: ()->
      @before_filter()
      $("##{@sCntrId}").modal 'hide'

    render:(sInputType,sTitle,hCell)->
      @before_filter()

      $("##{@sCntrId}").remove()

      sParams = """
        tabindex="-1"
        role="dialog"
        aria-hidden="true"
      """

      sInputHtml = ""

      if sInputType = "textarea"
        if wi_hlprs.is_cell_content_type_video hCell
          sContent = hCell.sContent
        else
          sContent = ""
        sInputHtml = tmpl.textarea "form-control",@sInputId,"rows='4'",[sContent]

      $('body').append tmpl.div "modal fade",@sCntrId,sParams,[
        tmpl.div "modal-dialog","","",[
          tmpl.div "modal-content","","",[
            tmpl.div "modal-header","","",[
              tmpl.button "close","","data-dismiss='modal' aria-hidden='true'",["&times;"]
              tmpl.h 4,sTitle,"modal-title"
            ]
            tmpl.div "modal-body","","",[sInputHtml]
            tmpl.div "modal-footer","","",[
              tmpl.button "btn btn-default","","data-dismiss='modal'",[ "Close" ]
              tmpl.button "btn btn-primary",@sBtnSubmitId,"",[ "Ok" ]
            ]
          ]
        ]
      ]


