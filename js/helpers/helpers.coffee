define [
  "libs/beaver/templates"
  'bconsole'
], (tmpl,jconsole) ->
  jconsole.info "helpers"

#------------КОНТРОЛЬ СКРОЛА
  prevent_default: (e) ->
    e = e || window.event
    if (e.preventDefault)
      e.preventDefault()
    e.returnValue = false

  keydown:(e) ->
    aKeys = [37, 38, 39, 40]
    _.each aKeys, (iCode) =>
      if e.keyCode == iCode
        prevent_default(e)
        return

  wheel:(e) ->
    prevent_default(e)

  disable_scroll: ->
    window.prevent_default = @prevent_default
    window.addEventListener('DOMMouseScroll', @wheel, false) if window.addEventListener
    window.onmousewheel = @wheel
    document.onkeydown = @keydown

  enable_scroll: ->
    window.removeEventListener('DOMMouseScroll', @wheel, false) if window.removeEventListener
    window.onmousewheel = document.onmousewheel = document.onkeydown = null
    try
      delete window.prevent_default
    catch e
      window["prevent_default"]=undefined

  # +2013.4.24 tuiteraz
  title_for: (hPage) ->
    if defined hPage.sBrowserTitle
      sBrowserTitle = hPage.sBrowserTitle + " - " + SITE.sBrowserMainTitle
    else
      sBrowserTitle = SITE.sBrowserMainTitle

    if is_ie 9
      document.title = sBrowserTitle
    else
      $("title").text sBrowserTitle



  # +2013.5.12 tuiteraz
  # *2013.6.3  tuiteraz: +bAutoHide
  # *2013.6.28 tuiteraz: +fnCallback
  # *2013.9.9  tuiteraz: +IE8 document.body.scrollTop
  show_modal_alert: (sText, bAutoHide=true, bNoAlertWell = false, fnCallback = null) ->
    @sAlertId = "modal-alert"

    render = =>
      $('body').append tmpl.div "", @sAlertId, [
        tmpl.div "back", "","style='display:none;'", []
        tmpl.div "alert","","style='display:none;'", []
      ]

    show = =>
      jBack = $("##{@sAlertId} .back")
      jAlert = $("##{@sAlertId} .alert")
      hAlertCoord = j.get_coordinates_of jAlert

      iWndHeight = $(window).height()
      iWndWidth  = $(window).width()

      iPageYOffset = get_y_offset()

      iTop = iPageYOffset + (iWndHeight - hAlertCoord.iHeight ) / 2 -100
      iLeft = (iWndWidth - hAlertCoord.iWidth ) / 2

      jBack.css { height: iPageYOffset+ $(window).height()}
      jAlert.css { left: iLeft, top: iTop}

      if bNoAlertWell
        jAlert.addClass 'no-back'
      else
        jAlert.removeClass 'no-back'

      @disable_scroll()

      #      $("##{sAlertId} .back").fadeIn 'slow'
      jBack.stop().fadeIn 200, ->
        jAlert.stop().fadeIn 200, ->
          fnCallback() if _.isFunction(fnCallback)


    render() if $("##{@sAlertId}").length == 0

    $("##{@sAlertId} .alert")[0].innerHTML = sText

    show()
    if bAutoHide
      setTimeout =>
        @hide_modal_alert()
      , 4000

  # +2013.6.3 tuiteraz
  # *2013.6.3 tuiteraz: +fnCallback
  hide_modal_alert: (fnCallback=null)->
    if defined @sAlertId
      jBack = $("##{@sAlertId} .back")
      jAlert = $("##{@sAlertId} .alert")
      jAlert.stop().fadeOut 400, =>
        jBack.stop().fadeOut 400
        @enable_scroll()
        fnCallback() if defined fnCallback

  # полс еотрисовки страницы проверим, чтобы высота контейнера была не меньше высоты окна.
  # чтобы под футером не было пустого места
  # +2013.7.17 uiteraz
  check_container_height: ->
    sSelector = "body > .container"
    jContainer = $(sSelector)

    # удалим навязанную высоту чтобы получить реальную
    jContContainer = $("##{SITE.content_container_id}")
    sStyle = jContContainer.attr 'style'
    jContContainer.attr 'style',''

    iContainerHeight = jContainer[0].scrollHeight
    iChildrenHeight = @get_childrens_height_of jContainer

    iContContainerHeight = jContContainer.height()

    # восставноим стиль, вдруг там была не только высота
    jContContainer.attr 'style',sStyle

    iNewContContainerHeight = (iContainerHeight - iChildrenHeight) + iContContainerHeight - 5
    iOldContContainerHeight = jContContainer.height()

    #jconsole.log "iChildrenHeight=#{iChildrenHeight}"
    #jconsole.log "#{iOldContContainerHeight}(old) -> #{iNewContContainerHeight}(new)"
    if iOldContContainerHeight != iNewContContainerHeight
      $("##{SITE.content_container_id}").css {height: iNewContContainerHeight}


  # +2013.7.18 tuiteraz
  get_childrens_height_of: (Selector) ->
    iSumHeight = 0
    $(Selector).children().attr('style',"").each ->
      iHeight = $(this).height()
      iSumHeight += iHeight

      #jconsole.info "#{$(this).attr('id')} -> #{iHeight}"

    return parseInt(iSumHeight)

  # fnCallback(jScope,bSuccess,jLink)
  # +2013.8.23 tuiteraz
  load_stylesheet: ( sPath, fnCallback=null, jScope=null ) ->
    head = document.getElementsByTagName( 'head' )[0] # reference to document.head for appending/ removing link nodes
    jLink = document.createElement 'link'           # create the link node
    jLink.setAttribute 'href', sPath
    jLink.setAttribute 'rel', 'stylesheet'
    jLink.setAttribute 'type', 'text/css'

    # get the correct properties to check for depending on the browser
    if ( !_.isUndefined jLink.sheet )
      sSheet = 'sheet'
      sCssRules = 'cssRules'
    else
      sSheet = 'styleSheet'
      sCssRules = 'rules'

    iIntervalId = setInterval =>  #start checking whether the style sheet has successfully loaded
      try
        if ( jLink[sSheet] && jLink[sSheet][sCssRules].length ) # SUCCESS! our style sheet has loaded
          clearInterval( iIntervalId )                     # clear the counters
          clearTimeout( iTimeoutId )
          fnCallback.call( jScope || window, true, jLink ) if _.isFunction fnCallback   # fire the callback with success == true
      catch e
    , 10                                                   # how often to check if the stylesheet is loaded

    iTimeoutId = setTimeout =>      # start counting down till fail
      clearInterval( iIntervalId )  # clear the counters
      clearTimeout( iTimeoutId )
      head.removeChild( jLink )      # since the style sheet didn't load, remove the link node from the DOM
      fnCallback.call( jScope || window, false, jLink ) if _.isFunction fnCallback # fire the callback with success == false
    , 15000                                 # how long to wait before failing

    head.appendChild jLink           # insert the link node into the DOM and start loading the style sheet

    return jLink                     # return the link node;


  # +2013.7.19 tuiteaz
  # *2013.7.19 tuiteaz: hUrlParams
  goto: (hDetails, hUrlParams=null) ->
    sTitle = if hDetails.sTitle then hDetails.sTitle else null
    sNavLink = hDetails.sHref
    History.pushState(null, sTitle , sNavLink)
    window.location.href = sNavLink if is_ie 9

  #+2013.12.2 tuiteraz
  call_if_function: (fnFnToCall) ->
    fnFnToCall() if _.isFunction(fnFnToCall)

  #+2013.12.4 tuiteraz
  progress:
    before_filter:()->
      @sProgressCntrId ||= "beaver-progressbar"

    show: (sClass="progress-striped",sBarClass="progress-bar-success")->
      @before_filter()

      @render(sClass,sBarClass) if $("##{@sProgressCntrId}").length == 0
      @reset()

      $("##{@sProgressCntrId}").show()

    hide: ()->
      @before_filter()
      setTimeout =>
        $("##{@sProgressCntrId}").fadeOut 'slow'
      , 800

    render:(sClass="",sBarClass="")->
      @before_filter()

      $("##{@sProgressCntrId}").empty() if $("##{@sProgressCntrId}").length == 1

      sParams = "style='display:none;'"

      $('body').append tmpl.div "progress active #{sClass}", @sProgressCntrId, sParams, [
        tmpl.div "progress-bar #{sBarClass}","","role='progressbar' aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'", []
      ]

    reset: ()->
      jPrgBar = $("##{@sProgressCntrId} > .progress-bar")
      jPrgBar.attr 'aria-valuenow', 0

    set_current_value:(iCurrValue)->
      jPrgBar = $("##{@sProgressCntrId} > .progress-bar")
      jPrgBar.attr 'aria-valuenow', iCurrValue
      jPrgBar.css { width: "#{iCurrValue}%"}

  #+2013.12.13 tuiteraz
  status:
    before_filter:()->
      @iHideTimerId ||= 0
      @sStatusCntrId ||= "beaver-modal-text-status"

      clearTimeout(@iHideTimerId) if @iHideTimerId>0

    show: (sText,sType="success")->
      @before_filter()

      sClass = "alert alert-#{sType}"
      sClass += " alert-dismissable" if sType == "danger"

      @render(sText,sClass)

      setTimeout =>
        $("##{@sStatusCntrId}").removeClass("beaver-status-out").addClass "beaver-status-in"
      , 200

      if sType != "danger"
        @iHideTimerId = setTimeout =>
          @hide()
        , 5000
    # sErrCustomMessage - if iStatus!=200 end sErrCustomMessage!=null will show sErrCustomMessage
    #+2013.12.16 tuiteraz
    show_results:(hRes,sSuccMessage="",sErrCustomMessage=null) ->
      if hRes.iStatus != 200
        if sErrCustomMessage
          sMessage = sErrCustomMessage
        else
          sMessage = hRes.sMessage
        @show sMessage,'danger'
        jconsole.error sMessage
      else
        @show sSuccMessage if !_.isEmpty(sSuccMessage)

    hide: ()->
      @before_filter()

      $("##{@sStatusCntrId}").removeClass("beaver-status-in").addClass "beaver-status-out"

    render:(sText,sClass="")->
      @before_filter()

      $("##{@sStatusCntrId}").remove()

      if _.str.include sClass, "alert-dismissable"
        sBtnHtml = tmpl.button "close","","data-dismiss='alert' aria-hidden='true'", ["&times;"]
      else
        sBtnHtml = ""

      $('body').append tmpl.div "beaver-status-out",@sStatusCntrId,"align='center'",[
        tmpl.div sClass,"","", [
          sBtnHtml
          sText
        ]
      ]

