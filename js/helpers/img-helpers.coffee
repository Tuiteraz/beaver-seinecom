define [
  "libs/beaver/templates"
  'bconsole'
], (tmpl,jconsole) ->
  jconsole.info "img-helpers"

  jail_for : (sSelector) ->
    jconsole.debug "jail_for(#{sSelector})"
    $("#{sSelector} img.lazy").jail()

  jail_hidden_for : (sSelector,bExtendSelector=true) ->
    #jconsole.debug "jail_hidden_for(#{sSelector})"
    if bExtendSelector
      $("#{sSelector} img.lazy").jail { loadHiddenImages:true }
    else
      $("#{sSelector}").jail { loadHiddenImages:true }

  jail_hidden_with_placeholder_for : (sSelector) ->
    #jconsole.debug "jail_hidden_with_placeholder_for(#{sSelector})"
    $("#{sSelector} img.lazy").jail {
      loadHiddenImages:true
      placeholder : SITE.sImgAJAXLoaderPath
    }

