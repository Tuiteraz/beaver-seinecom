define [
  'bconsole'
], (
  jconsole
) ->
  jconsole.info "text-pieces-helpers"

  #+2013.12.18 tuiteraz
  content_of: (sSlug,aTextPieces=null)->
    hTextPiece = @find(sSlug,aTextPieces)
    return hTextPiece.sContent

  #+2013.12.18 tuiteraz
  find: (sSlug,aTextPieces=null)->
    if !@aTextPieces
      @aTextPieces = aTextPieces

    aRes = _.where @aTextPieces, {sSlug:sSlug}
    if _.isArray(aRes)
      return aRes[0]
    else
      return null

