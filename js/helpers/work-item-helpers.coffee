define [
  'bconsole'
  "models/file-model"
  "async"
  'libs/beaver/templates'
], (
  jconsole
  FileModel
  async
  tmpl
) ->
  jconsole.info "work-item-helpers"

  #+2013.12.13 tuiteraz
  clear_cell_html_content:(hCellScope)->
    jCell = $("##{hCellScope.hRow.id} ##{hCellScope.hCell.id}:visible")
    if jCell.length == 1
      $.each jCell.children(":not(.row-cell-panel-container)"), (iIdx) ->
        $(this).remove()

  #+2013.12.13 tuiteraz
  get_cell_scope:(hItem,sRowId,sCellId) ->
    hRow    = _.findWhere hItem.aContent, {id: sRowId}
    iRowIdx = _.indexOf hItem.aContent, hRow
    if hRow
      hCell    = _.findWhere hRow.aCells, {id: sCellId}
      iCellIdx = _.indexOf hRow.aCells, hCell
      if hCell
        hRes =
          hRow     : hRow
          iRowIdx  : iRowIdx
          hCell    : hCell
          iCellIdx : iCellIdx
        return hRes
      else
        return null
    else
      return null


  #+2013.12.13 tuiteraz
  is_cell_content_type_picture:(hCell)->
    if hCell
      if hCell.sContentType == SITE.cp.data.content.works.cell.hContentTypes.picture
        return true
      else
        return false
    else
      return null

  #+2013.12.16 tuiteraz
  is_cell_content_type_video:(hCell)->
    if hCell
      if hCell.sContentType == SITE.cp.data.content.works.cell.hContentTypes.video
        return true
      else
        return false
    else
      return null



  #+2013.12.13 tuiteraz
  update_cell_html_content:(hCellScope) ->
    jconsole.debug "work-item-helpers.update_cell_html_content()"

    @clear_cell_html_content(hCellScope)

    jCell = $("##{hCellScope.hRow.id} ##{hCellScope.hCell.id}:visible")

    if @is_cell_content_type_picture(hCellScope.hCell)
      jCell.append tmpl.div "row-cell-content" ,"",[
        tmpl.img "/db/file/#{hCellScope.hCell.sContent}"
      ]

    else if @is_cell_content_type_video(hCellScope.hCell)
      aMatch = hCellScope.hCell.sContent.match /embed\/([\w\d]{4,30})?/
      if !_.isUndefined(aMatch) & _.isArray(aMatch)
        sHtml = tmpl.div "row-cell-content","",[
          tmpl.img "http://img.youtube.com/vi/#{aMatch[1]}/0.jpg"
        ]

      else
        sHtml = tmpl.div "row-cell-content","",[
          hCellScope.hCell.sContent
        ]

      jCell.append sHtml


