define [
  'bconsole'
  'jquery'
  'jquery-plugins'
  'libs/beaver/settings'
  'underscore'
  'underscore.string'
  'settings'  # должны идти первыми
  'bootstrap'
  'bootstrap-hover-dropdown'
  'soundmanager2'
  'async'
  'object-id'
], (jconsole) ->
  jconsole.info "lib-pack"
  _.mixin _.str.exports()
  return
