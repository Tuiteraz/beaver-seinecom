define [
  'bconsole'
  'lightbeam-settings'
], (jconsole) ->
  jconsole.info "lightbeam/tools/auth"

  #+2013.11.9 tuiteraz
  get_auth_status:(fnCallback=null) ->
    if _.isFunction(fnCallback)
      bAsync = true
    else
      bAsync = false
      hRes = {}

    $.ajax {
      type: 'GET'
      url: "/#{LIGHTBEAM.auth.sToolPath}"
      dataType: 'json'
      async: bAsync
      error:  (data,textStatus,jqXHR) ->
        hRes =
          iStatus : data.iStatus
          sMessage : data.sMessage

        fnCallback(hRes) if _.isFunction fnCallback

      success:  (data,textStatus,jqXHR) ->
        hRes =
          iStatus     : jqXHR.status
          bIsSignedIn : data.bIsSignedIn
          hUser       : data.hUser
          sMessage    : data.sMessage

        fnCallback(hRes) if _.isFunction fnCallback
    }

    return hRes if !bAsync


  # hData={sLogin,sPassword}
  #+2013.11.9 tuiteraz
  sign_in: (hData,fnCallback=null) ->
    if _.isFunction(fnCallback)
      bAsync = true
    else
      bAsync = false
      hRes = {}

    $.ajax {
      type: 'POST'
      url: "/#{LIGHTBEAM.auth.sToolPath}"
      dataType: 'json'
      data: hData
      async: bAsync
      error:  (data,textStatus,jqXHR) ->
        hResponseJSON = j.parse_JSON(data.responseText)
        hRes =
          iStatus : data.status
          bIsSignedIn : hResponseJSON.bIsSignedIn
          hUser       : hResponseJSON.hUser
          sMessage    : hResponseJSON.sMessage

        fnCallback(hRes) if _.isFunction fnCallback

      success:  (data,textStatus,jqXHR) ->
        hRes =
          iStatus     : jqXHR.status
          bIsSignedIn : data.bIsSignedIn
          hUser       : data.hUser
          sMessage    : data.sMessage

        fnCallback(hRes) if _.isFunction fnCallback
    }

    return hRes if !bAsync

  #+2013.11.9 tuiteraz
  sign_out: (fnCallback=null) ->
    if _.isFunction(fnCallback)
      bAsync = true
    else
      bAsync = false
      hRes = {}

    $.ajax {
      type: 'DELETE'
      url: "/#{LIGHTBEAM.auth.sToolPath}"
      dataType: 'json'
      async: bAsync
      error:  (data,textStatus,jqXHR) ->
        hResponseJSON = j.parse_JSON(data.responseText)
        hRes =
          iStatus : data.status
          bIsSignedIn : hResponseJSON.bIsSignedIn
          sMessage    : hResponseJSON.sMessage

        fnCallback(hRes) if _.isFunction fnCallback

      success:  (data,textStatus,jqXHR) ->
        hRes =
          iStatus     : jqXHR.status
          bIsSignedIn : data.bIsSignedIn
          sMessage    : data.sMessage

        fnCallback(hRes) if _.isFunction fnCallback
    }

    return hRes if !bAsync

  #+2013.11.26 tuiteraz
  if_signed_in: (fnCallback) ->
    @get_auth_status (hAuthStatus)=>
      if hAuthStatus.bIsSignedIn && hAuthStatus.hUser.bIsAdmin
        fnCallback() is _.isFunction(fnCallback)
