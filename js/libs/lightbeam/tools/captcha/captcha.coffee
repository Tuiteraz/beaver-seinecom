define [
  'libs/beaver/templates'
  'bconsole'
], (tmpl,jconsole) ->
  jconsole.info "lightbeam/tools/captcha"

  # +2013.6.1 tuiteraz
  bind_events: ->
    $("#captcha img").unbind('click').click ->
      $(this).toggleClass 'active'
      if $(this).hasClass 'active'
        sThisSrc= $(this).attr 'src'
        $("#captcha img[src!='#{sThisSrc}'].active").removeClass 'active'

  # +2013.6.2 tuiteraz
  selected_number: ->
    if @is_selected()
      sImgSrc = $("#captcha .active").attr 'src'
      hImg = _.detect @aImages, (hImg)->
        sImgSrc.search(hImg.sSrc) >= 0
      iIdx = _.indexOf @aImages,hImg
      return iIdx + 1
    else
      return 0

  set_control_number: (iNewNumber) ->
    @iControlNumber = iNewNumber

  # +2013.6.1 tuiteraz
  # *2013.9.4 tuiteraz: +sJuicyUrl
  get_control_number: ()->
    me = this

    $.ajax {
      type: 'GET'
      url: "/captcha"
      dataType: 'text'
      async: false
      error:  (data,textStatus,jqXHR) ->
        jconsole.error data

      success:  (data,textStatus,jqXHR) ->
        me.iControlNumber = parseInt data
        me.update_title()
    }

    return @iControlNumber

  # +2013.6.2 tuiteraz
  is_selected: ->
    if $("#captcha .active").length == 1 then true else false

  # +2013.4.26 tuiteraz
  render: () ->

    @get_control_number()

    hInput =
      id: "captcha"
      sFullName: 'Captcha'
      sInputStyle: FRM.input.style.input
      sInputType: FRM.input.type.hidden


    sDir = "jvpic/lib/captcha/images/"
    @aImages = [
      {sTitle: "house", sSrc:"#{sDir}01.png"}
      {sTitle: "key", sSrc:"#{sDir}02.png"}
      {sTitle: "flag", sSrc:"#{sDir}03.png"}
      {sTitle: "watch", sSrc:"#{sDir}04.png"}
      {sTitle: "bug", sSrc:"#{sDir}05.png"}
      {sTitle: "pen", sSrc:"#{sDir}06.png"}
      {sTitle: "light", sSrc:"#{sDir}07.png"}
      {sTitle: "note", sSrc:"#{sDir}08.png"}
      {sTitle: "heart", sSrc:"#{sDir}09.png"}
      {sTitle: "globe", sSrc:"#{sDir}10.png"}
    ]

    @aImages = _.shuffle @aImages



    sImgHtml = ''
    for hImg in @aImages
      sImgHtml += tmpl.img hImg.sSrc

    tmpl.div "","captcha", "align='center' ", [
      tmpl.twbp_input hInput
      tmpl.div "title", "", [ ]
      tmpl.div "", "captcha-image-container","align='center'", [ sImgHtml ]
    ]

  # после асинхронного получения контрольного номера обновим заголвоок
  # +2013.6.3 tuiteraz
  # *2013.9.4 tuiteraz
  update_title: ->
    if defined @aImages
      sImgTitle = @aImages[@iControlNumber-1].sTitle
      sTitleHtml = "In order to confirm that you are human, select <span style='font-weight: bold;' >#{sImgTitle}</span>"
      $("#captcha .title").slideUp().empty().append(sTitleHtml).slideDown()



