// Generated by CoffeeScript 1.12.7
(function() {
  define(['libs/beaver/templates', 'bconsole'], function(tmpl, jconsole) {
    jconsole.info("lightbeam/tools/captcha");
    return {
      bind_events: function() {
        return $("#captcha img").unbind('click').click(function() {
          var sThisSrc;
          $(this).toggleClass('active');
          if ($(this).hasClass('active')) {
            sThisSrc = $(this).attr('src');
            return $("#captcha img[src!='" + sThisSrc + "'].active").removeClass('active');
          }
        });
      },
      selected_number: function() {
        var hImg, iIdx, sImgSrc;
        if (this.is_selected()) {
          sImgSrc = $("#captcha .active").attr('src');
          hImg = _.detect(this.aImages, function(hImg) {
            return sImgSrc.search(hImg.sSrc) >= 0;
          });
          iIdx = _.indexOf(this.aImages, hImg);
          return iIdx + 1;
        } else {
          return 0;
        }
      },
      set_control_number: function(iNewNumber) {
        return this.iControlNumber = iNewNumber;
      },
      get_control_number: function() {
        var me;
        me = this;
        $.ajax({
          type: 'GET',
          url: "/captcha",
          dataType: 'text',
          async: false,
          error: function(data, textStatus, jqXHR) {
            return jconsole.error(data);
          },
          success: function(data, textStatus, jqXHR) {
            me.iControlNumber = parseInt(data);
            return me.update_title();
          }
        });
        return this.iControlNumber;
      },
      is_selected: function() {
        if ($("#captcha .active").length === 1) {
          return true;
        } else {
          return false;
        }
      },
      render: function() {
        var hImg, hInput, i, len, ref, sDir, sImgHtml;
        this.get_control_number();
        hInput = {
          id: "captcha",
          sFullName: 'Captcha',
          sInputStyle: FRM.input.style.input,
          sInputType: FRM.input.type.hidden
        };
        sDir = "jvpic/lib/captcha/images/";
        this.aImages = [
          {
            sTitle: "house",
            sSrc: sDir + "01.png"
          }, {
            sTitle: "key",
            sSrc: sDir + "02.png"
          }, {
            sTitle: "flag",
            sSrc: sDir + "03.png"
          }, {
            sTitle: "watch",
            sSrc: sDir + "04.png"
          }, {
            sTitle: "bug",
            sSrc: sDir + "05.png"
          }, {
            sTitle: "pen",
            sSrc: sDir + "06.png"
          }, {
            sTitle: "light",
            sSrc: sDir + "07.png"
          }, {
            sTitle: "note",
            sSrc: sDir + "08.png"
          }, {
            sTitle: "heart",
            sSrc: sDir + "09.png"
          }, {
            sTitle: "globe",
            sSrc: sDir + "10.png"
          }
        ];
        this.aImages = _.shuffle(this.aImages);
        sImgHtml = '';
        ref = this.aImages;
        for (i = 0, len = ref.length; i < len; i++) {
          hImg = ref[i];
          sImgHtml += tmpl.img(hImg.sSrc);
        }
        return tmpl.div("", "captcha", "align='center' ", [tmpl.twbp_input(hInput), tmpl.div("title", "", []), tmpl.div("", "captcha-image-container", "align='center'", [sImgHtml])]);
      },
      update_title: function() {
        var sImgTitle, sTitleHtml;
        if (defined(this.aImages)) {
          sImgTitle = this.aImages[this.iControlNumber - 1].sTitle;
          sTitleHtml = "In order to confirm that you are human, select <span style='font-weight: bold;' >" + sImgTitle + "</span>";
          return $("#captcha .title").slideUp().empty().append(sTitleHtml).slideDown();
        }
      }
    };
  });

}).call(this);

//# sourceMappingURL=captcha.js.map
