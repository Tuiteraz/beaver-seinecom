define [
  'helpers/helpers'
  'domReady'
  'libs/video-js/video'
], (hlprs,domReady) ->

  hlprs.load_stylesheet("/js/libs/video-js/video-js.css")
  domReady ->
    document.createElement 'video'
    document.createElement 'audio'
    videojs.options.flash.swf = "/js/libs/video-js/video-js.swf"