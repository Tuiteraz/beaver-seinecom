define [
  'bconsole'
  'libs/lightbeam/mongoose-model'
  "models/file-model"
], (
  jconsole
  MongooseModel
  FileModel
) ->
  jconsole.info "art-department-photo-model"

  ArtDepPhoto = (hParams) ->
    MongooseModel.call this, hParams
    return this

  ArtDepPhoto.prototype = Object.create MongooseModel.prototype

  #+2013.12.2 tuiteraz
  ArtDepPhoto.prototype.create = (sPhoneSlug,fnCallback=null) ->
    me = this
    #first make file record
    mFile  = new FileModel SITE.hMongoDB.hModels.hFile
    mFile.create (hRes) ->
      if hRes.iStatus != 200
        return fnCallback(hRes) if _.isFunction(fnCallback)

      hData =
        File       : hRes.aData._id
        iOrder     : 999999
        sPhoneSlug : sPhoneSlug
        dtCreated  : Date()

      MongooseModel.prototype.create.call me, hData, (hRes)->
        fnCallback(hRes)

  #+2013.12.3 tuiteraz
  ArtDepPhoto.prototype.remove = (Query,hOptions={},fnCallback=null)->
    me = this
    mFile  = new FileModel SITE.hMongoDB.hModels.hFile
    mArtDepPhoto =  new ArtDepPhoto @hParams
    mArtDepPhoto.get Query, {}, (hRes)->
      if hRes.iStatus != 200
        hRes.sMessage = "mArtDepPhoto.remove() > mLogo.get() : #{hRes.sMessage}"
        fnCallback(hRes)
      else
        mFile.remove {_id: hRes.aData[0].File},{}, (hRes)->
          if hRes.iStatus != 200
            hRes.sMessage = "mArtDepPhoto.remove() > mFile.remove() : #{hRes.sMessage}"
            fnCallback(hRes)
          else
            MongooseModel.prototype.remove.call me, Query,hOptions, (hRes)->
              fnCallback(hRes)


  return ArtDepPhoto
