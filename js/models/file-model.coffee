define [
  'bconsole'
  'libs/lightbeam/mongoose-model'
  'helpers/helpers'
  'async'
], (
  jconsole
  MongooseModel
  hlprs
  async
) ->
  jconsole.info "file-model"

  FileModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  FileModel.prototype = Object.create MongooseModel.prototype

  FileModel.prototype.create = (hOptions) ->
    if _.isFunction(hOptions)
      fnCallback = hOptions
      hOptions =
        fnCallback : fnCallback

    jFrm = document.createElement "form"
    jInp = document.createElement "input"

    $(jFrm).addClass 'hidden'
    $('body').append jFrm

    $(jInp).attr "type","file"
    $(jInp).attr "name","FileToUpload"
    $(jFrm).append jInp

    $(jInp).trigger 'click'
    $(jInp).unbind('change').change (e)=>
      aFiles = []
      async.series [
        (fnNext)->
          $.each e.target.files, (iIdx, jFile)->
            jReader = new FileReader()
            jReader.onload = (e)->
              aFiles.push {
                sFilename: jFile.name
                sData: e.target.result
              }
              fnNext null,aFiles

            jReader.readAsDataURL(jFile)
        (fnNext)=>
          $.ajax {
            type: 'POST'
            url: "/db/#{@hParams.sCollectionName}/upload"
            async: true
            data: {
              sFileName: aFiles[0].sFilename
              sFileData: aFiles[0].sData
            }
            beforeSend: (jXMLHttpRequest)->
              hlprs.progress.show()
            progressUpload: (e,iPercentComplete) ->
              hlprs.progress.set_current_value(iPercentComplete)

            error:  (data,textStatus,jqXHR) ->
              hRes =
                iStatus : data.iStatus
                sMessage : data.sMessage

              fnNext(null,hRes)

            success:  (data,textStatus,jqXHR) ->
              hlprs.progress.hide()
              jqXHR.responseJSON = j.parse_JSON(jqXHR.responseText) if !jqXHR.responseJSON

              hRes =
                iStatus : jqXHR.status
                aData   : data

              fnNext null,hRes
          }

      ],(sErr,aRes) ->
        jconsole.error sErr if sErr
        fnCallback aRes[1]


  return FileModel
  