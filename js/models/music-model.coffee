define [
  'bconsole'
  'libs/lightbeam/mongoose-model'
  "models/file-model"
], (
  jconsole
  MongooseModel
  FileModel
) ->
  jconsole.info "music-model"

  MusicModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  MusicModel.prototype = Object.create MongooseModel.prototype

  #+2013.12.2 tuiteraz
  MusicModel.prototype.create = (fnCallback=null) ->
    me = this
    #first make file record
    mFile  = new FileModel SITE.hMongoDB.hModels.hFile
    mFile.create (hRes) ->
      if hRes.iStatus != 200
        return fnCallback(hRes) if _.isFunction(fnCallback)

      hData =
        File: hRes.aData._id
        iOrder:999999
        sTitle: hRes.aData.sTitle
        dtCreated: Date()

      MongooseModel.prototype.create.call me, hData, (hRes)->
        fnCallback(hRes)

  #+2013.12.3 tuiteraz
  MusicModel.prototype.remove = (Query,hOptions={},fnCallback=null)->
    me = this
    mFile  = new FileModel SITE.hMongoDB.hModels.hFile
    mLogo =  new MusicModel @hParams
    mLogo.get Query, {}, (hRes)->
      if hRes.iStatus != 200
        hRes.sMessage = "mMusicModel.remove() > mLogo.get() : #{hRes.sMessage}"
        fnCallback(hRes)
      else
        mFile.remove {_id: hRes.aData[0].File},{}, (hRes)->
          if hRes.iStatus != 200
            hRes.sMessage = "mMusicModel.remove() > mFile.remove() : #{hRes.sMessage}"
            fnCallback(hRes)
          else
            MongooseModel.prototype.remove.call me, Query,hOptions, (hRes)->
              fnCallback(hRes)

  return MusicModel
