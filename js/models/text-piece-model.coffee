define [
  'bconsole'
  'libs/lightbeam/mongoose-model'
], (
  jconsole
  MongooseModel
) ->
  jconsole.info "text-piece-model"

  TextPieceModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  TextPieceModel.prototype = Object.create MongooseModel.prototype


  return TextPieceModel
  