define [
  'bconsole'
  'libs/lightbeam/mongoose-model'
], (jconsole,MongooseModel) ->
  jconsole.info "user-model"

  UserModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  UserModel.prototype = Object.create MongooseModel.prototype

#  mUserModel.prototype = {
#
#  }

  return UserModel
  