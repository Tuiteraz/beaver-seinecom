define [
  'bconsole'
  'libs/lightbeam/mongoose-model'
  "models/file-model"
  'async'
  '../helpers/work-item-helpers'
], (
  jconsole
  MongooseModel
  FileModel
  async
  wi_hlprs
) ->
  jconsole.info "work-model"

  me_mdl = this

  WorkModel = (hParams) ->
    MongooseModel.call this, hParams
    return this

  WorkModel.prototype = Object.create MongooseModel.prototype

  #+2013.12.2 tuiteraz
  WorkModel.prototype.create = (hData, fnCallback=null) ->
    hData.sTitle       ||= "New portfolio work"
    hData.sDescription ||= ""
    hData.bInArchive   ||= false
    hData.iOrder       ||= 999999

    MongooseModel.prototype.create.call this, hData, (hRes)->
      fnCallback(hRes)

  #+2013.12.3 tuiteraz
  WorkModel.prototype.remove = (Query,hOptions={},fnCallback=null)->
    me = this

    @remove_files Query,(hRes)->
      if hRes.iStatus != 200
        fnCallback(hRes)
      else
        MongooseModel.prototype.remove.call me, Query,hOptions, (hRes)->
          fnCallback(hRes)

  #+2013.12.11 tuiteraz
  WorkModel.prototype.update = (hQuery, hData, hOptions={},fnCallback=null)->
    if _.isFunction(hOptions) && !fnCallback
      fnCallback = hOptions
      hOptions = {}

    MongooseModel.prototype.update.call this, hQuery,hData,hOptions, (hRes)->
      fnCallback(hRes)

  #+2013.12.13 tuiteraz
  WorkModel.prototype.update_row = (hQuery, hRowQuery, hRow, fnCallback)->
    hOptions =
      aArrayProperties: ["aContent"]
      sArrayPropertiesOperation: "update"
      hArrayPropertyQuery: hRowQuery

    MongooseModel.prototype.update.call this, hQuery,{aContent:hRow},hOptions, (hRes)->
      fnCallback(hRes)


  #+2013.12.13 tuiteraz
  WorkModel.prototype.remove_files=(Query,fnCallback) ->
    #jconsole.debug "WorkModel.remove_files()"
    mWork =  new WorkModel @hParams
    mWork.get Query, {}, (hRes)->
      if hRes.iStatus != 200
        hRes.sMessage = "WorkModel.remove() > mWork.remove_files() : #{hRes.sMessage}"
        fnCallback(hRes)
      else
        hItem = hRes.aData[0]
        mFile =  new FileModel SITE.hMongoDB.hModels.hFile
        aFnParallel = []

        # ThmbImg
        aFnParallel.push (fnNext) ->
          if hItem.ThmbImg
            mFile.remove {_id: hItem.ThmbImg},{}, (hRes)->
              if hRes.iStatus != 200
                hRes.sMessage = "WorkModel.remove() > mFile.remove_files() -> hItem.ThmbImg : #{hRes.sMessage}"
                fnNext hRes
              else
                fnNext()
          else
            fnNext()
        # LogoImg
        aFnParallel.push (fnNext) ->
          if hItem.LogoImg
            mFile.remove {_id: hItem.LogoImg},{}, (hRes)->
              if hRes.iStatus != 200
                hRes.sMessage = "WorkModel.remove() > mFile.remove_files() -> hItem.LogoImg : #{hRes.sMessage}"
                fnNext hhRes
              else
                fnNext()
          else
            fnNext()

        _.each hItem.aContent, (hRow)->
          _.each hRow.aCells, (hCell)->
            if wi_hlprs.is_cell_content_type_picture(hCell) & !_.isUndefined(hCell.sContent)
              aFnParallel.push (fnNext) ->
                mFile.remove {_id: hCell.sContent},{}, (hRes)->
                  if hRes.iStatus != 200
                    hRes.sMessage = "WorkModel.remove() > mFile.remove_files() -> cells file[#{hCell.sContent}] : #{hRes.sMessage}"
                    fnNext hRes
                  else
                    fnNext()

        async.parallel aFnParallel, (hErr,aRes)->
          if hErr
            hRes = _.clone hErr
          else
            hRes =
              iStatus : 200
              aData: aRes

          fnCallback(hRes)


  return WorkModel
  