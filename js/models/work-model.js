// Generated by CoffeeScript 1.12.7
(function() {
  define(['bconsole', 'libs/lightbeam/mongoose-model', "models/file-model", 'async', '../helpers/work-item-helpers'], function(jconsole, MongooseModel, FileModel, async, wi_hlprs) {
    var WorkModel, me_mdl;
    jconsole.info("work-model");
    me_mdl = this;
    WorkModel = function(hParams) {
      MongooseModel.call(this, hParams);
      return this;
    };
    WorkModel.prototype = Object.create(MongooseModel.prototype);
    WorkModel.prototype.create = function(hData, fnCallback) {
      if (fnCallback == null) {
        fnCallback = null;
      }
      hData.sTitle || (hData.sTitle = "New portfolio work");
      hData.sDescription || (hData.sDescription = "");
      hData.bInArchive || (hData.bInArchive = false);
      hData.iOrder || (hData.iOrder = 999999);
      return MongooseModel.prototype.create.call(this, hData, function(hRes) {
        return fnCallback(hRes);
      });
    };
    WorkModel.prototype.remove = function(Query, hOptions, fnCallback) {
      var me;
      if (hOptions == null) {
        hOptions = {};
      }
      if (fnCallback == null) {
        fnCallback = null;
      }
      me = this;
      return this.remove_files(Query, function(hRes) {
        if (hRes.iStatus !== 200) {
          return fnCallback(hRes);
        } else {
          return MongooseModel.prototype.remove.call(me, Query, hOptions, function(hRes) {
            return fnCallback(hRes);
          });
        }
      });
    };
    WorkModel.prototype.update = function(hQuery, hData, hOptions, fnCallback) {
      if (hOptions == null) {
        hOptions = {};
      }
      if (fnCallback == null) {
        fnCallback = null;
      }
      if (_.isFunction(hOptions) && !fnCallback) {
        fnCallback = hOptions;
        hOptions = {};
      }
      return MongooseModel.prototype.update.call(this, hQuery, hData, hOptions, function(hRes) {
        return fnCallback(hRes);
      });
    };
    WorkModel.prototype.update_row = function(hQuery, hRowQuery, hRow, fnCallback) {
      var hOptions;
      hOptions = {
        aArrayProperties: ["aContent"],
        sArrayPropertiesOperation: "update",
        hArrayPropertyQuery: hRowQuery
      };
      return MongooseModel.prototype.update.call(this, hQuery, {
        aContent: hRow
      }, hOptions, function(hRes) {
        return fnCallback(hRes);
      });
    };
    WorkModel.prototype.remove_files = function(Query, fnCallback) {
      var mWork;
      mWork = new WorkModel(this.hParams);
      return mWork.get(Query, {}, function(hRes) {
        var aFnParallel, hItem, mFile;
        if (hRes.iStatus !== 200) {
          hRes.sMessage = "WorkModel.remove() > mWork.remove_files() : " + hRes.sMessage;
          return fnCallback(hRes);
        } else {
          hItem = hRes.aData[0];
          mFile = new FileModel(SITE.hMongoDB.hModels.hFile);
          aFnParallel = [];
          aFnParallel.push(function(fnNext) {
            if (hItem.ThmbImg) {
              return mFile.remove({
                _id: hItem.ThmbImg
              }, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  hRes.sMessage = "WorkModel.remove() > mFile.remove_files() -> hItem.ThmbImg : " + hRes.sMessage;
                  return fnNext(hRes);
                } else {
                  return fnNext();
                }
              });
            } else {
              return fnNext();
            }
          });
          aFnParallel.push(function(fnNext) {
            if (hItem.LogoImg) {
              return mFile.remove({
                _id: hItem.LogoImg
              }, {}, function(hRes) {
                if (hRes.iStatus !== 200) {
                  hRes.sMessage = "WorkModel.remove() > mFile.remove_files() -> hItem.LogoImg : " + hRes.sMessage;
                  return fnNext(hhRes);
                } else {
                  return fnNext();
                }
              });
            } else {
              return fnNext();
            }
          });
          _.each(hItem.aContent, function(hRow) {
            return _.each(hRow.aCells, function(hCell) {
              if (wi_hlprs.is_cell_content_type_picture(hCell) & !_.isUndefined(hCell.sContent)) {
                return aFnParallel.push(function(fnNext) {
                  return mFile.remove({
                    _id: hCell.sContent
                  }, {}, function(hRes) {
                    if (hRes.iStatus !== 200) {
                      hRes.sMessage = "WorkModel.remove() > mFile.remove_files() -> cells file[" + hCell.sContent + "] : " + hRes.sMessage;
                      return fnNext(hRes);
                    } else {
                      return fnNext();
                    }
                  });
                });
              }
            });
          });
          return async.parallel(aFnParallel, function(hErr, aRes) {
            if (hErr) {
              hRes = _.clone(hErr);
            } else {
              hRes = {
                iStatus: 200,
                aData: aRes
              };
            }
            return fnCallback(hRes);
          });
        }
      });
    };
    return WorkModel;
  });

}).call(this);

//# sourceMappingURL=work-model.js.map
