define [], ()->
  #+2013.10.18 tuiteraz
  get: ->
    [
      {
        sSlug: "bic-tv"
        sTitle: 'BicTV'
        sBrowserTitle: 'BicTV'
        bIsSinglePage: true
        id: "works-bic-tv"
        sImgThumbPath: "/images/works-thumbnails/bic-tv-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center" style="margin:60px 0 30px 0;">
            <div style="width:90%;margin:0 0 130px 0;" align="left">
              <img src="/images/client-logos/logo-bic-big.jpg"/>
              <div style="font-family:'MyriadPro-Regular';float:right;width: 440px;">
                <span style="font-weight:bold;">BIC TV spots series "Seasons"</span><br>
                <span>
                There is TV spots series before weather forecast. Four animation video show how BIC pen helps during four seasons: winter, spring, summer and fall. This campaign was also supported by outdoor and print.
                </span>
              </div>
              <div class="clearfix"/>
            </div>
            <iframe width="640" height="480" src="//www.youtube.com/embed/Hbhug3ltHLw?rel=0" frameborder="0" allowfullscreen style="margin-bottom:70px;"></iframe>
            <br>
            <iframe width="640" height="480" src="//www.youtube.com/embed/TlWBsmLSpKM?rel=0" frameborder="0" allowfullscreen style="margin-bottom:70px;"></iframe>
            <br>
            <iframe width="640" height="480" src="//www.youtube.com/embed/xEkSgSjvtAk?rel=0" frameborder="0" allowfullscreen></iframe>
          </div>
        """
      }
      {
        sSlug: "bic"
        sTitle: 'Bic'
        sBrowserTitle: 'Bic'
        bIsSinglePage: true
        id: "works-bic"
        sImgThumbPath: "/images/works-thumbnails/bic-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-bic.jpg"/>
          </div>
        """
      }
      {
        sSlug: "magimix"
        sTitle: 'Magimix'
        sBrowserTitle: 'Magimix'
        bIsSinglePage: true
        id: "works-magimix"
        sImgThumbPath: "/images/works-thumbnails/magimix-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-magimix.jpg"/>
          </div>
        """

      }
      {
        sSlug: "kaas"
        sTitle: 'Kaas'
        sBrowserTitle: 'Kass'
        bIsSinglePage: true
        id: "works-kaas"
        sImgThumbPath:"/images/works-thumbnails/patricia-kaas-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-kaas.jpg"/>
          </div>
        """
      }
      {
        sSlug: "fan-of-brands"
        sTitle: 'Fan Of Brands'
        sBrowserTitle: 'Fan Of Brands'
        bIsSinglePage: true
        id: "works-fan-of-brands"
        sImgThumbPath:"/images/works-thumbnails/fan-of-brands-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-fan-of-brands.jpg"/>
          </div>
        """

      }
      {
        sSlug: "lumene"
        sTitle: 'Lumene'
        sBrowserTitle: 'Lumene'
        bIsSinglePage: true
        id: "works-lumene"
        sImgThumbPath:"/images/works-thumbnails/lumene-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-lumene.jpg"/>
          </div>
        """

      }
      {
        sSlug: "kangaroo"
        sTitle: 'Kangaroo'
        sBrowserTitle: 'Kangaroo'
        bIsSinglePage: true
        id: "works-kangaroo"
        sImgThumbPath: "/images/works-thumbnails/kangaroo-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-kangaroo.jpg"/>
          </div>
        """

      }
      {
        sSlug: "flormar"
        sTitle: 'Flormar'
        sBrowserTitle: 'Flormar'
        bIsSinglePage: true
        id: "works-flormar"
        sImgThumbPath:"/images/works-thumbnails/flormar-thmb.jpg"
        aCategories: ['portfolio']
        sContent: """
          <div align="center">
            <img src="/images/works/work-flormar.jpg"/>
          </div>
        """

      }
      {
        sSlug: "yves-rocher"
        sTitle: 'Yves Rocher'
        sBrowserTitle: 'Yves Rocher'
        bIsSinglePage: true
        id: "works-yves-rosher"
        sImgThumbPath:"/images/works-thumbnails/yves-rosher-thmb.jpg"
        aCategories: ['archive']
        sContent: """
          <div align="center">
            <img src="/images/works/work-yves-rosher.jpg"/>
          </div>
        """

      }
      {
        sSlug: "lefutur"
        sTitle: 'LEFUTUR'
        sBrowserTitle: 'LEFUTUR'
        bIsSinglePage: true
        id: "works-lefutur"
        sImgThumbPath:"/images/works-thumbnails/lefutur-thmb.jpg"
        aCategories: ['archive']
        sContent: """
          <div align="center">
            <img src="/images/works/work-lefutur.jpg"/>
          </div>
        """

      }
      {
        sSlug: "swatch"
        sTitle: 'Swatch'
        sBrowserTitle: 'Swatch'
        bIsSinglePage: true
        id: "works-swatch"
        sImgThumbPath:"/images/works-thumbnails/swatch-thmb.jpg"
        aCategories: ['archive']
        sContent: """
          <div align="center">
            <img src="/images/works/work-swatch.jpg"/>
          </div>
        """

      }
    ]

  #*2013.10.18 tuiteraz
  get_content:(sCategory=null) ->

    if !sCategory
      aRes = @get()
    else
      aRes = _.filter @get(), (hWork) ->
        _.contains hWork.aCategories, sCategory

    return aRes