define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "common-tmpl"

  # +2013.7.17 tuiteraz
  render_scaffolding: ->
#    $('head').append @render_google_counter()
#    $('body').empty().append @render_yandex_counter()
    $('body').append @render_container()
    $("##{SITE.footer.container_id}").empty().append @render_footer()

  # +2013.8.22 tuiteraz
  render_google_counter: ->
    """
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-43416135-1', 'wtrmln.org');
    ga('send', 'pageview');

    </script>
    """

  # +2013.8.22 tuiteraz
  render_yandex_counter: ->
    """
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter22134065 = new Ya.Metrika({id:22134065, clickmap:true, trackLinks:true, accurateTrackBounce:true, trackHash:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/22134065" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    """

  # +2013.7.16 tuiteraz
  render_container: ->
    tmpl.div "container", [
      tmpl.div "row", SITE.intro.container_id ,"align='center'", []
      tmpl.div "row", SITE.player.container_id,"style='display:none;'",[]
      tmpl.div "row", SITE.nav_container_id,"style='display:none;'",[]
      tmpl.div "row", SITE.content_container_id,"style='display:none;'",[
        tmpl.div "row",SITE.index.container_id, [
          tmpl.div "top-nav-underlight"
          tmpl.div "top-scroll-fade"
        ]
        tmpl.div "row", SITE.works_container_id, [
          tmpl.div "top-scroll-fade"
        ]
      ]

      tmpl.div "row", SITE.footer.container_id ,[]
    ]

  render_footer: ->
    tmpl.div "col-md-12", [
#      tmpl.span "credits", [SITE.footer.sCredits]
    ]

  #+2013.10.18 tuiteraz
  render_map: (style)->
    tmpl.div "","map-container", style,[
      tmpl.div "map","","",[
        tmpl.div "title", "", [
          tmpl.span "pull-right", "", [
            tmpl.div "close","","", [
              tmpl.fa_icon('times')
            ]
          ]
          tmpl.span "print pull-right", "", "style='margin-right:20px;'",[ "PRINT" ]
          tmpl.div "clearfix"
        ]
        tmpl.div "image-container","", [
          tmpl.img "/images/map-1.jpg"
        ]
      ]
    ]

