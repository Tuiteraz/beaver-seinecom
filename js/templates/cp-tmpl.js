// Generated by CoffeeScript 1.12.7
(function() {
  define(['libs/beaver/templates', 'helpers/helpers', 'bconsole'], function(tmpl, hlprs, jconsole) {
    jconsole.info("cp-tmpl");
    return {
      render: function() {
        var hBtnSignOut;
        hBtnSignOut = SITE.cp.nav.buttons.sign_out;
        return tmpl.div("container", [tmpl.nav("navbar navbar-default navbar-fixed-top", SITE.cp.nav.container_id, "role='navigation'", [tmpl.div("navbar-inner", [tmpl.div("navbar-header", "", "", [tmpl.a("/cp", "", [SITE.cp.nav.sTitle], "navbar-brand")]), tmpl.div("col-md-6", "", []), tmpl.div("col-md-1 pull-right", [tmpl.button("btn btn-default navbar-btn pull-right", hBtnSignOut.id, "", [tmpl.fa_icon(hBtnSignOut.sIcon)])]), tmpl.clearfix()])]), tmpl.div("row", SITE.cp.data.container_id, "", [tmpl.div("col-md-3", "", [tmpl.div("affix-top", SITE.cp.data.nav.container_id, "", []), tmpl.clearfix()]), tmpl.div("col-md-9", SITE.cp.data.content.container_id, []), tmpl.clearfix()])]);
      }
    };
  });

}).call(this);

//# sourceMappingURL=cp-tmpl.js.map
