define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp/data-tmpl"

  #+2013.11.26 tuiteraz
  render_nav: () ->
    sCpSlug        = SITE.cp.sSlug
    sDataSlug      = SITE.cp.data.sSlug
    hLogos         = SITE.cp.data.nav.items.logos
    hWorks         = SITE.cp.data.nav.items.works
    hMusic         = SITE.cp.data.nav.items.music
    hArtDepartment = SITE.cp.data.nav.items.art_department
    hTextPieces    = SITE.cp.data.nav.items.text_pieces

    tmpl.ul "nav nav-pills nav-stacked", "",[
      tmpl.li "","","", [
        tmpl.a "/#{sCpSlug}/#{sDataSlug}/#{hLogos.sSlug}","",[hLogos.sTitle]
      ]
      tmpl.li "","","", [
        tmpl.a "/#{sCpSlug}/#{sDataSlug}/#{hWorks.sSlug}","",[hWorks.sTitle]
      ]
      tmpl.li "","","", [
        tmpl.a "/#{sCpSlug}/#{sDataSlug}/#{hMusic.sSlug}","",[hMusic.sTitle]
      ]
      tmpl.li "","","", [
        tmpl.a "/#{sCpSlug}/#{sDataSlug}/#{hArtDepartment.sSlug}","",[hArtDepartment.sTitle]
      ]
      tmpl.li "","","", [
        tmpl.a "/#{sCpSlug}/#{sDataSlug}/#{hTextPieces.sSlug}","",[hTextPieces.sTitle]
      ]
    ]

  #+2013.11.28 tuiteraz
  render_content_header: ->
    hBtnAdd     = SITE.cp.data.content.buttons.add
    hBtnRefresh = SITE.cp.data.content.buttons.refresh

    tmpl.div "cp-content-tab-header","","",[
      tmpl.div "btn-group","",[
        tmpl.button hBtnAdd.sClass,"","data-content-action='#{hBtnAdd.sAction}'",[ tmpl.fa_icon(hBtnAdd.sIcon) ]
        tmpl.button hBtnRefresh.sClass,"","data-content-action='#{hBtnRefresh.sAction}'",[ tmpl.fa_icon(hBtnRefresh.sIcon) ]
      ]
    ]

  #+2013.11.27 tuiteraz
  render_content_logos:(sDataRoute)->
    sHdrHtml = @render_content_header()
    sParams  = "data-route='#{sDataRoute}' style='display:none;' "

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body logos","","",[]
      tmpl.clearfix()
    ]

  #+2013.12.3 tuiteraz
  render_content_item_art_dep_photo:(hItem)->

    sParams = "align='center'"
    if _.isObject(hItem.File)
      sSrc = "/db/file/#{hItem.File._id}"
    else
      sSrc = "/db/file/#{hItem.File}"

    hBtnRemove     = SITE.cp.data.content.buttons.remove

    tmpl.div "cp-content-item",hItem._id,sParams,[
      tmpl.div "cp-content-item-container","", [
        tmpl.div "cp-content-item-image-container","",[
          tmpl.img sSrc,"cp-content-item-image"
        ]
        tmpl.div "cp-content-item-devider"
        tmpl.div "btn-group","",[
          tmpl.button hBtnRemove.sClass,"","data-content-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  render_content_item_logos:(hItem)->

    sParams = "align='center'"
    if _.isObject(hItem.File)
      sSrc = "/db/file/#{hItem.File._id}"
    else
      sSrc = "/db/file/#{hItem.File}"

    hBtnRemove     = SITE.cp.data.content.buttons.remove

    tmpl.div "cp-content-item",hItem._id,sParams,[
      tmpl.div "cp-content-item-container","", [
        tmpl.div "cp-content-item-image-container","",[
          tmpl.img sSrc,"cp-content-item-image"
        ]
        tmpl.div "cp-content-item-devider"
        tmpl.div "btn-group","",[
          tmpl.button hBtnRemove.sClass,"","data-content-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
        ]
      ]
    ]

  #+2013.12.3 tuiteraz
  render_content_item_music:(hItem)->

    hBtnRemove     = SITE.cp.data.content.buttons.remove
    hBtnUpdate     = SITE.cp.data.content.buttons.update

    hMusicTitle = SITE.cp.data.content.music.elements.hMusicTitle
    sParams = hMusicTitle.sParams + """
      value = "#{hItem.sTitle}"
    """

    tmpl.div "cp-content-item",hItem._id,"",[
      tmpl.div "cp-content-item-container","", [
        tmpl.div "pull-left cp-content-item-title","", [
          tmpl.div "form-group", [
            tmpl.input "form-control #{hMusicTitle.sClass}","",sParams,[]
          ]

        ]
        tmpl.div "btn-group pull-right","","style='margin-left:5px;'",[
          tmpl.button hBtnRemove.sClass,"","data-content-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
        ]
        tmpl.div "btn-group pull-right","",[
          tmpl.button hBtnUpdate.sClass,"","data-content-action='#{hBtnUpdate.sAction}' style='display:none;'",[ tmpl.fa_icon(hBtnUpdate.sIcon) ]
        ]

        tmpl.clearfix()
      ]
    ]

  #+2013.12.3 tuiteraz
  render_content_item_text_piece:(hItem)->

    hItem.sSlug    ||= ""
    hItem.sContent ||=""

    hBtnRemove     = SITE.cp.data.content.buttons.remove
    hBtnUpdate     = SITE.cp.data.content.buttons.update

    hSlug = SITE.cp.data.content.text_pieces.elements.hSlug
    sSlugParams = hSlug.sParams + """
      value = "#{hItem.sSlug}"
      placeholder = "#{hSlug.sPlaceholder}"
    """
    hContent = SITE.cp.data.content.text_pieces.elements.hContent
    sContentParams = hContent.sParams + """
      value = "#{hItem.sContent}"
      placeholder = "#{hContent.sPlaceholder}"
    """

    tmpl.div "cp-content-item",hItem._id,"",[
      tmpl.div "cp-content-item-container","", [
        tmpl.div "cp-content-item-form pull-left col-md-10","", [
          tmpl.div "form-group pull-left col-md-3", [
            tmpl.input "form-control #{hSlug.sClass}","",sSlugParams,[]
          ]
          tmpl.div "form-group pull-left col-md-9", [
            tmpl.input "form-control #{hContent.sClass}","",sContentParams,[]
          ]
        ]
        tmpl.div "pull-right","","",[
          tmpl.div "btn-group pull-left","",[
            tmpl.button hBtnUpdate.sClass,"","data-content-action='#{hBtnUpdate.sAction}' style='display:none;'",[ tmpl.fa_icon(hBtnUpdate.sIcon) ]
          ]
          tmpl.div "btn-group pull-left","","style='margin-left:5px;'",[
            tmpl.button hBtnRemove.sClass,"","data-content-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
          ]
        ]
        tmpl.clearfix()
      ]
    ]

  #+2013.12.7 tuiteraz
  render_content_item_work:(hItem)->

    hBtnRemove = SITE.cp.data.content.buttons.remove
    hBtnEdit   = SITE.cp.data.content.buttons.edit

    tmpl.div "cp-content-item",hItem._id,"",[
      tmpl.div "cp-content-item-container","", [
        tmpl.div "pull-left cp-content-item-title","", [
          hItem.sTitle
        ]
        tmpl.div "pull-right","","",[
          tmpl.div "btn-group","",[
            tmpl.button hBtnEdit.sClass,"","data-content-action='#{hBtnEdit.sAction}'",[ tmpl.fa_icon(hBtnEdit.sIcon) ]
          ]
          tmpl.div "btn-group","","style='margin-left:5px;'",[
            tmpl.button hBtnRemove.sClass,"","data-content-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
          ]
        ]

        tmpl.clearfix()
      ]
    ]

  #+2013.12.17 tuiteraz
  render_content_art_department:(sDataRoute)->
    sHdrHtml = @render_content_header()
    sParams  = "data-route='#{sDataRoute}' style='display:none;' "

    tmpl.div "cp-content-tab","",sParams,[
      tmpl.div "cp-content-tab-body art-department","","",[]
      tmpl.clearfix()
    ]

  #+2013.11.27 tuiteraz
  render_content_works:(sDataRoute)->
    sHdrHtml = @render_content_header()
    sParams  = "data-route='#{sDataRoute}' style='display:none;' "

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body works","","",[]
      tmpl.clearfix()
    ]

  #+2013.11.27 tuiteraz
  render_content_music:(sDataRoute)->
    sHdrHtml  = @render_content_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body music","","",[]
      tmpl.clearfix()
    ]

  #+2013.11.27 tuiteraz
  render_content_text_pieces:(sDataRoute)->
    sHdrHtml  = @render_content_header()
    sParams = "data-route='#{sDataRoute}' style='display:none;' "

    tmpl.div "cp-content-tab","",sParams,[
      sHdrHtml
      tmpl.div "cp-content-tab-body text-pieces","","",[]
      tmpl.clearfix()
    ]


