define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/work-item-helpers'
  'bconsole'
], (
  tmpl
  hlprs
  wi_hlprs
  jconsole
) ->

  jconsole.info "cp/data/item-tmpl"

  #+2013.12.9 tuiteraz
  render: (hItem) ->
    jconsole.debug "item-tmpl.render()"

    hItem.sDescription ||= ""

    sResHtml =  @render_tab_header()

    hBtnAddRow   = SITE.cp.data.content.buttons.add_row
    hBtnAddPic   = SITE.cp.data.content.buttons.add_picture
    hBtnRemove   = SITE.cp.data.content.buttons.remove

    hPropTitle     = SITE.cp.data.content.works.item.elements.hTitle
    sPropTitleId   = "#{hItem._id}-#{hPropTitle.sId}"

    hPropDesc      = SITE.cp.data.content.works.item.elements.hDescription
    sPropDescId    = "#{hItem._id}-#{hPropDesc.sId}"

    hPropInArchive = SITE.cp.data.content.works.item.elements.hInArchive
    sPropInArchiveId = "#{hItem._id}-#{hPropInArchive.sId}"
    sInArchiveChecked = if hItem.bInArchive then 'checked' else ''

    hPropThmbImg   = SITE.cp.data.content.works.item.elements.hThmbImg
    sPropThmbImgCntrId = "#{hItem._id}-#{hPropThmbImg.sCntrClass}"
    hPropLogoImg   = SITE.cp.data.content.works.item.elements.hLogoImg
    sPropLogoImgCntrId = "#{hItem._id}-#{hPropLogoImg.sCntrClass}"

    if hItem[hPropThmbImg.sPropertyName]
      sThmbImgSrc = "/db/file/#{hItem[hPropThmbImg.sPropertyName]}"
    else
      sThmbImgSrc = ""

    if hItem[hPropLogoImg.sPropertyName]
      sLogoImgSrc = "/db/file/#{hItem[hPropLogoImg.sPropertyName]}"
    else
      sLogoImgSrc = ""

    sContentPanelHtml = tmpl.div "btn-group","",[
      tmpl.button "btn btn-default","","data-item-action='#{hBtnAddRow.sAction}' data-row-tmpl='single-cell'",[
        tmpl.fa_icon(hBtnAddRow.sIcon)
      ]
      tmpl.button "btn btn-default","","data-item-action='#{hBtnAddRow.sAction}' data-row-tmpl='double-cell'",[
        tmpl.fa_icon(hBtnAddRow.sIcon)
        tmpl.fa_icon(hBtnAddRow.sIcon)
      ]
    ]

    sContentHtml = ""
    _.each hItem.aContent, (hRow) =>
      sContentHtml += @render_content_row(hRow)

    sThmbImgPanelHtml = tmpl.div "thmb-img-panel-container","","", [
      tmpl.div "btn-group","",[
        tmpl.button hBtnAddPic.sClass,"","data-item-action='#{hBtnAddPic.sAction}'",[ tmpl.fa_icon(hBtnAddPic.sIcon) ]
      ]
    ]



    sResHtml += tmpl.div "cp-content-tab-body edit-item","","",[
      tmpl.div "","","style='margin:20px 0;'",[
        tmpl.div "col-md-6","", [
          tmpl.form "form-horizontal","","",[
            tmpl.div "form-group",[
              tmpl.label "col-md-4 control-label no-padding-right", "","for='#{sPropTitleId}'", [ hPropTitle.sTitle ]
              tmpl.div "col-md-8",[
                tmpl.input "form-control",sPropTitleId,"value='#{hItem.sTitle}'",[ ]
              ]
            ]
            tmpl.div "form-group",[
              tmpl.label "col-md-4 control-label no-padding-right", "","for='#{sPropDescId}'", [ hPropDesc.sTitle ]
              tmpl.div "col-md-8",[
                tmpl.textarea "form-control",sPropDescId,"rows='6'",[ hItem.sDescription ]
              ]
            ]

            tmpl.div "form-group",[
              tmpl.label "col-md-4 no-padding-right", "","for='#{sPropInArchiveId}' style='text-align:right;cursor:pointer;'", [
                hPropInArchive.sTitle
              ]
              tmpl.div "col-md-8","","",[
                tmpl.input "",sPropInArchiveId,"type='checkbox' #{sInArchiveChecked}",[ ]
              ]

            ]
          ]

        ]
        tmpl.div "col-md-6","","", [
          tmpl.div "col-md-6","","style='padding-left:0;'",[
            tmpl.div "thumbnail #{hPropThmbImg.sCntrClass}",sPropThmbImgCntrId,"align='center' data-property-name='#{hPropThmbImg.sPropertyName}'",[
              tmpl.div "caption","","",[
                tmpl.h 4,"Slider thumbnail"
              ]
              tmpl.div "img-container","","",[
                sThmbImgPanelHtml
                tmpl.img sThmbImgSrc,"",""
              ]
            ]
          ]
          tmpl.div "col-md-6","","style='padding-right:0;'",[
            tmpl.div "thumbnail #{hPropLogoImg.sCntrClass}",sPropLogoImgCntrId,"align='center' data-property-name='#{hPropLogoImg.sPropertyName}'",[
              tmpl.div "caption","","",[
                tmpl.h 4,"Logo"
              ]
              tmpl.div "img-container","","",[
                sThmbImgPanelHtml
                tmpl.img sLogoImgSrc,"",""
              ]
            ]
          ]
        ]
        tmpl.clearfix()
      ]
      tmpl.div "panel panel-default", '', "",[
        tmpl.div "panel-heading", '',"",[ sContentPanelHtml]
        tmpl.div "panel-body","","align='center'",[sContentHtml]
      ]
    ]

    return sResHtml

  #+2013.12.13 tuiteraz
  render_cell_content:(hCell) ->
    sHtml = ""
    if wi_hlprs.is_cell_content_type_picture(hCell)
      sHtml = tmpl.div "row-cell-content","",[
        tmpl.img "/db/file/#{hCell.sContent}"
      ]

    else if wi_hlprs.is_cell_content_type_video(hCell)
      aMatch = hCell.sContent.match /embed\/([\w\d]{4,30})?/
      if !_.isUndefined(aMatch) & _.isArray(aMatch)
        sHtml = tmpl.div "row-cell-content","",[
          tmpl.img "http://img.youtube.com/vi/#{aMatch[1]}/0.jpg"
        ]

      else
        sHtml = tmpl.div "row-cell-content","",[
          hCell.sContent
        ]

    return sHtml

  #+2013.12.9 tuiteraz
  render_tab:(sRoute)->
    tmpl.div "cp-content-tab","","data-route='#{sRoute}'",[]

  #+2013.11.28 tuiteraz
  render_tab_header: ->
    hBtnUpdate   = SITE.cp.data.content.buttons.update
    hBtnRemove   = SITE.cp.data.content.buttons.remove
    hBtnGoBack   = SITE.cp.data.content.buttons.go_back

    tmpl.div "cp-content-tab-header","","",[
      tmpl.div "btn-group","",[
        tmpl.button hBtnGoBack.sClass,"","data-content-action='#{hBtnGoBack.sAction}'",[ tmpl.fa_icon(hBtnGoBack.sIcon) ]
        tmpl.button hBtnUpdate.sClass,"","data-content-action='#{hBtnUpdate.sAction}'",[ tmpl.fa_icon(hBtnUpdate.sIcon) ]
        tmpl.button hBtnRemove.sClass,"","data-content-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
      ]
      tmpl.clearfix()
    ]

  #+2013.12.11 tuiteraz
  render_content_row:(hRow) ->
    hBtnRemoveRow   = SITE.cp.data.content.buttons.remove_row

    hBtnAddPic   = SITE.cp.data.content.buttons.add_picture
    hBtnAddTxt   = SITE.cp.data.content.buttons.add_text
    hBtnAddVideo = SITE.cp.data.content.buttons.add_video
    hBtnRemove   = SITE.cp.data.content.buttons.remove

    sRowPanelCntrHtml = tmpl.div "row-panel-container","","align='right'",[
      tmpl.div "btn-group","",[
        tmpl.button hBtnRemoveRow.sClass,"","data-item-action='#{hBtnRemoveRow.sAction}'",[ tmpl.fa_icon(hBtnRemoveRow.sIcon) ]
      ]
    ]

    sRowCellPanelCntrHtml = tmpl.div "row-cell-panel-container","","",[
      tmpl.div "btn-group","",[
        tmpl.button hBtnAddPic.sClass,"","data-item-action='#{hBtnAddPic.sAction}'",[ tmpl.fa_icon(hBtnAddPic.sIcon) ]
#        tmpl.button hBtnAddTxt.sClass,"","data-item-action='#{hBtnAddTxt.sAction}'",[ tmpl.fa_icon(hBtnAddTxt.sIcon) ]
        tmpl.button hBtnAddVideo.sClass,"","data-item-action='#{hBtnAddVideo.sAction}'",[ tmpl.fa_icon(hBtnAddVideo.sIcon) ]
#        tmpl.button hBtnRemove.sClass,"","data-item-action='#{hBtnRemove.sAction}'",[ tmpl.fa_icon(hBtnRemove.sIcon) ]
      ]
    ]

    sCellsHtml = ""

    if _.size(hRow.aCells) == 1
      sCellSizeClass = "col-md-12"
    else if _.size(hRow.aCells) == 2
      sCellSizeClass = "col-md-6"

    _.each hRow.aCells, (hCell)=>
      sCellContentHtml = @render_cell_content(hCell)
      sCellsHtml += tmpl.div "row-cell #{sCellSizeClass}",hCell.id,"",[
        sRowCellPanelCntrHtml
        sCellContentHtml
      ]

    tmpl.div "row",hRow.id,"",[
      sRowPanelCntrHtml
      sCellsHtml
    ]

