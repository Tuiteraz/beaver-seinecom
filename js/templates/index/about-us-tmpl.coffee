define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
  'helpers/nav-item-helpers'
], (tmpl,hlprs,jconsole,nav_item_hlprs) ->

  jconsole.info "index/about-us-tmpl"

  #+2013.10.12 tuiteraz
  render: (@ctxContent)->
    iOffset = 0
    sUnderlightStellar = "
          data-stellar-ratio='#{SITE.article_underlight_stellar.v_ratio}'
          data-stellar-vertical-offset='-#{iOffset}'
          "
    sTitleStellar = "
          data-stellar-ratio='1.2'
          data-stellar-vertical-offset='-#{iOffset}'
          "
    hNavItem = nav_item_hlprs.detect_by_slug @ctxContent.ctxNavigation.aNavItems, "about-us"

    sVideoClass = "video-js vjs-default-skin vjs-paused vjs-controls-enabled vjs-user-active vjs-big-play-centered"
    sVideoParams = """
      autoplay muted='true' loop preload='none' width='1000' height='576'
      poster="/images/video-posters/cheval_magali.jpg"
    """

    sEL = "EncorpadaClassic-ExtraLight"
    sB = "EncorpadaClassic-Bold"
    tmpl.html_tag "article","",hNavItem.id,"data-stellar-offset-parent='true'",[
      tmpl.div "article-title-underlight","",sUnderlightStellar,[]
#      tmpl.html_tag "video",sVideoClass,"index-about-video",sVideoParams,[
#         """
#          <source src="/media/cheval_magali_1024x576.webm" type="video/webm"  />
#
#          """
#      ]
      tmpl.html_tag "section","title","",[
        tmpl.h 2, "About us","title-text t1",sTitleStellar
        tmpl.div "underline","","style='width:602px'",[]
      ]
      tmpl.html_tag "section","text","", [
        tmpl.div "","","style='font-family:#{sEL};font-size:28pt;left:275px;top:-18px;'",[
          "We are indies, but pros!"
        ]
        tmpl.div "","","style='font-family:#{sB};font-size:21pt;left:156px;top:27px;'",[
          "whom met in SEINECOM"]
        tmpl.div "","","style='font-family:#{sEL};font-size:18pt;left:108px;top:60px;'",[
          "to use every aspect of our experience"]
        tmpl.div "","","style='font-family:#{sEL};font-size:34pt;left:527px;top:39px;'",[
          "for your projects"]
        tmpl.div "","","style='font-family:#{sB};font-size:26pt;left:231px;top:96px;'",[
          "We are SEINECOM!"]
        tmpl.div "","","style='font-family:#{sEL};font-size:20pt;left:113px;top:137px;'",[
          "Here you'll be heard"]
        tmpl.div "","","style='font-family:#{sEL};font-size:20pt;left:202px;top:165px;'",[
          "and understood."]
      ]
      tmpl.html_tag "section","content","", [

      ]
    ]

