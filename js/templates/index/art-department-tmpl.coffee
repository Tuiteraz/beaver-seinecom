define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
  'helpers/nav-item-helpers'
], (tmpl,hlprs,jconsole,nav_item_hlprs) ->

  jconsole.info "index/art-department-tmpl"

  #+2013.10.12 tuiteraz
  render: (@ctxContent,aArtDepPhotos)->
    iOffset = $("##{SITE.index.container_id}").height()
    sUnderlightStellar = "
              data-stellar-ratio='#{SITE.article_underlight_stellar.v_ratio}'
              data-stellar-vertical-offset='-#{iOffset}'"
    sTitleStellar = "
             data-stellar-ratio='1.2'
             data-stellar-vertical-offset='-#{iOffset}'
            "
    sL = "EncorpadaClassic-Light"
    sEL = "EncorpadaClassic-ExtraLight"
    sB = "EncorpadaClassic-Bold"
    sSB = "EncorpadaClassic-SemiBold"
    sR = "EncorpadaClassic-Regular"
    sRI = "EncorpadaClassic-RegularItalic"

    hNavItem = nav_item_hlprs.detect_by_slug @ctxContent.ctxNavigation.aNavItems, "art-department"

    hArtDep = SITE.index.art_department

    $('body').append @render_modal_photo_slider_for hArtDep.katya, _.where(aArtDepPhotos,{sPhoneSlug:hArtDep.katya.sSlug})
    $('body').append @render_modal_photo_slider_for hArtDep.lilia,_.where(aArtDepPhotos,{sPhoneSlug:hArtDep.lilia.sSlug})
    $('body').append @render_modal_photo_slider_for hArtDep.margo,_.where(aArtDepPhotos,{sPhoneSlug:hArtDep.margo.sSlug})
    $('body').append @render_modal_photo_slider_for hArtDep.tan,_.where(aArtDepPhotos,{sPhoneSlug:hArtDep.tan.sSlug})

    tmpl.html_tag "article","",hNavItem.id,"data-stellar-offset-parent='true'",[
      tmpl.div "article-title-underlight","",sUnderlightStellar,[]
      tmpl.html_tag "section","title","","",[
        tmpl.h 2, "Creative art","title-text",sTitleStellar
        tmpl.div "underline","","style='width:722px'",[]
      ]
      tmpl.html_tag "section","text","", [
        tmpl.div "","","style='font-family:#{sEL};font-size:52pt;left:454px;top:-42px;'",[
          "department"]
        tmpl.div "","","style='font-family:#{sL};font-size:24pt;left:252px;top:42px;'",[
          "Daily life with taste of SEINECOM"]
        tmpl.div "","","style='font-family:#{sR};font-size:31pt;left:650px;top:81px;'",[
          "What we create"]
        tmpl.div "","","style='font-family:#{sL};font-size:21pt;left:498px;top:126px;'",[
          "when we don't create ideas for you"]
        tmpl.div "","","style='font-family:#{sB};font-size:36pt;left:622px;top:498px;'",[
          "space and life"]
      ]
      tmpl.html_tag "section","content","", [
        tmpl.div "art-department-phone",hArtDep.katya.sPhoneId, [
          tmpl.img SITE.sImgBlankPath,'lazy',"data-src='/images/phone-katya.png'"
          tmpl.div "title", [ hArtDep.katya.sTitleHtml ]
        ]
        tmpl.div "art-department-phone",hArtDep.lilia.sPhoneId, [
          tmpl.img SITE.sImgBlankPath,'lazy',"data-src='/images/phone-lilia.png'"
          tmpl.div "title", [ hArtDep.lilia.sTitleHtml ]
        ]
        tmpl.div "art-department-phone",hArtDep.margo.sPhoneId, [
          tmpl.img SITE.sImgBlankPath,'lazy',"data-src='/images/phone-margo.png'"
          tmpl.div "title", [ hArtDep.margo.sTitleHtml ]
        ]
        tmpl.div "art-department-phone",hArtDep.tan.sPhoneId, [
          tmpl.img SITE.sImgBlankPath,'lazy',"data-src='/images/phone-tan.png'"
          tmpl.div "title", [ hArtDep.tan.sTitleHtml ]
        ]
      ]
    ]

  #+2013.10.20 tuiteraz
  render_modal_photo_slider_for: (hPerson,aImages=[]) ->
    sLiHtml = ""
    for hImage in aImages
      sLiHtml += tmpl.li "","", [
        tmpl.a "#","", tmpl.img(SITE.sImgBlankPath,'lazy',"data-src='/db/file/#{hImage.File}'")
      ]
    sUlHtml = tmpl.ul "rs-slider","#{hPerson.sPhoneId}-rs-slider", [ sLiHtml ]

    tmpl.div "modal-photo-container", "","data-phone-id='#{hPerson.sPhoneId}'",[
      tmpl.div "title", "", [
        tmpl.div "top-nav-underlight"
        hPerson.sTitleHtml
        tmpl.div "close", "", [ tmpl.fa_icon('times') ]
      ]

      sUlHtml
    ]


