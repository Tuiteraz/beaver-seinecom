define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
  'helpers/nav-item-helpers'
], (tmpl,hlprs,jconsole,nav_item_hlprs) ->

  jconsole.info "index/clients-tmpl"

  #+2013.10.12 tuiteraz
  render: (@ctxContent)->
    iOffset = $("##{SITE.index.container_id}").height()
    sUnderlightStellar = "
      data-stellar-ratio='#{SITE.article_underlight_stellar.v_ratio}'
      data-stellar-vertical-offset='-#{iOffset}'"
    sTitleStellar = "
     data-stellar-ratio='1.2'
     data-stellar-vertical-offset='-#{iOffset}'
    "
    sL = "EncorpadaClassic-Light"
    sEL = "EncorpadaClassic-ExtraLight"
    sB = "EncorpadaClassic-Bold"
    sSB = "EncorpadaClassic-SemiBold"
    sR = "EncorpadaClassic-Regular"

    hNavItem = nav_item_hlprs.detect_by_slug @ctxContent.ctxNavigation.aNavItems, "clients"

    sClImgHtml = ""
    _.each hNavItem.aChildrens, (sFileId) ->
      sClImgHtml += tmpl.img(SITE.sImgBlankPath,'lazy',"data-src='/db/file/#{sFileId}'")

    tmpl.html_tag "article","",hNavItem.id,"data-stellar-offset-parent='true'",[
      tmpl.div "article-title-underlight","",sUnderlightStellar,[]
      tmpl.html_tag "section","title","","",[
        tmpl.h 2, "Clients","title-text",sTitleStellar
        tmpl.div "underline","","style='width:602px;top:-1px;'",[]
      ]
      tmpl.html_tag "section","text","", [
        tmpl.div "","","style='font-family:#{sL};font-size:33pt;left:182px;top:-22px;'",[
          "To be part of"]
        tmpl.div "","","style='font-family:#{sL};font-size:28pt;left:237px;top:35px;'",[
          "this company"]
        tmpl.div "","","style='font-family:#{sB};font-size:50pt;left:493px;top:6px;'",[
          "is great"]
      ]
      tmpl.html_tag "section","content","","align='center'", [
        tmpl.div "clients-list","","align='center'",[sClImgHtml]
        tmpl.div "clearfix"
      ]
    ]
