define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
  'helpers/nav-item-helpers'
  'helpers/text-pieces-helpers'
], (
  tmpl
  hlprs
  jconsole
  nav_item_hlprs
  tp_hlprs
) ->

  jconsole.info "index/contacts-tmpl"

  #+2013.10.12 tuiteraz
  render: (@ctxContent, aTextPieces)->
    iOffset = $("##{SITE.index.container_id}").height()
    sUnderlightStellar = "
                  data-stellar-ratio='#{SITE.article_underlight_stellar.v_ratio}'
                  data-stellar-vertical-offset='-#{iOffset}'"
    sTitleStellar = "
                 data-stellar-ratio='1.2'
                 data-stellar-vertical-offset='-#{iOffset}'
                "
    sL = "EncorpadaClassic-Light"
    sEL = "EncorpadaClassic-ExtraLight"
    sB = "EncorpadaClassic-Bold"
    sSB = "EncorpadaClassic-SemiBold"
    sR = "EncorpadaClassic-Regular"
    sRI = "EncorpadaClassic-RegularItalic"
    sMyriad = "MyriadPro-Regular"

    hNavItem = nav_item_hlprs.detect_by_slug @ctxContent.ctxNavigation.aNavItems, "contacts"
    sContactEmail = tp_hlprs.content_of("contact-4",aTextPieces)
    tmpl.html_tag "article","",hNavItem.id,"data-stellar-offset-parent='true'",[
      tmpl.div "article-title-underlight","",sUnderlightStellar,[]
      tmpl.html_tag "section","title","","",[
        tmpl.h 2, "Contacts","title-text",sTitleStellar
        tmpl.div "underline","","style='width:722px'",[]
      ]
      tmpl.html_tag "section","text","", [
        tmpl.div "","","style='font-family:#{sR};font-size:36pt;left:169px;top:-14px;'",[
          "Maybe now"]
        tmpl.div "","","style='font-family:#{sL};font-size:21pt;left:278px;top:38px;'",[
          "you don't have any projects for us."]
        tmpl.div "","","style='font-family:#{sL};font-size:30pt;left:248px;top:74px;'",[
          "We invite you"]
        tmpl.div "","","style='font-family:#{sL};font-size:25pt;left:454px;top:112px;'",[
          "to work, to share, to create,"]
        tmpl.div "","","style='font-family:#{sL};font-size:36pt;left:206px;top:147px;'",[
          "to drink tea and talk"]
        tmpl.div "","","style='font-family:#{sMyriad};font-size:40pt;left:596px; top:208px;color:#888;'", [
          tp_hlprs.content_of("contact-1",aTextPieces) ]
        tmpl.div "","","style='font-family:#{sMyriad};font-size:30pt;left:695px; top:268px;color:#888;'",[
          tp_hlprs.content_of("contact-2")]
        tmpl.div "","","style='font-family:#{sMyriad};font-size:18pt;left:808px; top:312px;color:#6a6a6a;'", [
          tp_hlprs.content_of("contact-3") ]
        tmpl.div "","","style='font-family:#{sMyriad};font-size:18pt;left:741px; top:340px;color:#fff;'",[
          tmpl.a "mailto:#{sContactEmail}","style='color:#fff;'",[ sContactEmail ]
        ]
        tmpl.div "map-marker","","style='font-size:105pt;left:322px;top:243px;'",[
          tmpl.fa_icon('map-marker')]
        tmpl.div "","","style='font-family:#{sMyriad};font-size:18pt; left:410px;top:356px;color:#666;'",[
          tp_hlprs.content_of("contact-5")]
        tmpl.div "","","style='font-family:#{sL};font-size:36pt; left:206px;top:431px;'",[
          "So just come!"]
      ]
      tmpl.html_tag "section","content","", [ ]
    ]