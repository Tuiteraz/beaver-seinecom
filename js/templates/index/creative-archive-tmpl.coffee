define [
  'libs/beaver/templates'
  'helpers/helpers'
  'bconsole'
  'helpers/nav-item-helpers'
], (tmpl,hlprs,jconsole,nav_item_hlprs) ->

  jconsole.info "index/creative-archive-tmpl"

  #+2013.10.12 tuiteraz
  render: (@ctxContent)->
    iOffset = $("##{SITE.index.container_id}").height()
    sUnderlightStellar = "
          data-stellar-ratio='#{SITE.article_underlight_stellar.v_ratio}'
          data-stellar-vertical-offset='-#{iOffset}'"
    sTitleStellar = "
         data-stellar-ratio='1.2'
         data-stellar-vertical-offset='-#{iOffset}'
        "

    hWorks = nav_item_hlprs.detect_by_slug @ctxContent.ctxNavigation.aNavItems, "creative-archive"

    sWorksUlHtml = ""
    sWorksLiHtml = ""
    for hWork in hWorks.aChildrens
      sWorkHref = nav_item_hlprs.get_href_for hWork
      sWorksLiHtml += tmpl.li "", "", [
        tmpl.a "/#{hWorks.sSlug}#{sWorkHref}","",
          [ tmpl.img(SITE.sImgBlankPath,'lazy',"data-src='/db/file/#{hWork.ThmbImg}'") ] ]

    sWorksUlHtml = tmpl.ul "jcarousel-skin-index-creative-archive",SITE.index.creative_archive.carousel_id,"",[
      sWorksLiHtml
    ]

    sL = "EncorpadaClassic-Light"
    sEL = "EncorpadaClassic-ExtraLight"
    sB = "EncorpadaClassic-Bold"
    sSB = "EncorpadaClassic-SemiBold"
    sR = "EncorpadaClassic-Regular"
    sRI = "EncorpadaClassic-RegularItalic"

    hNavItem = nav_item_hlprs.detect_by_slug @ctxContent.ctxNavigation.aNavItems, "creative-archive"

    tmpl.html_tag "article","",hNavItem.id,"data-stellar-offset-parent='true'",[
      tmpl.div "article-title-underlight","",sUnderlightStellar,[]
      tmpl.html_tag "section","title","","",[
        tmpl.h 2, "Creative","title-text",sTitleStellar
        tmpl.div "underline","","style='width:602px'",[]
      ]
      tmpl.html_tag "section","text","", [
        tmpl.div "","","style='font-family:#{sR};font-size:54pt;left:613px;top:-70px;'",[
          "archive"]
        tmpl.div "","","style='font-family:#{sR};font-size:36pt;left:87px;top:-6px;'",[
          "We can't stop create."]
        tmpl.div "","","style='font-family:#{sL};font-size:24pt;left:370px;top:52px;'",[
          "We have good ideas for clients."]
        tmpl.div "","","style='font-family:#{sB};font-size:34pt;left:194px;top:89px;'",[
          "for tenders"]
        tmpl.div "","","style='font-family:#{sRI};font-size:26pt;left:90px;top:135px;'",[
          "or just for fun."]
      ]
      tmpl.html_tag "section","content","", [
        sWorksUlHtml
      ]
    ]

