define [
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/work-item-helpers'
  'bconsole'
], (
  tmpl
  hlprs
  wi_hlprs
  jconsole
) ->

  jconsole.info "templates/work-item-tmpl"

  #+2013.12.18 tuiteraz
  render:(hItem)->
    jconsole.debug "templates/work-item-tmpl.render(#{hItem._id})"

    hItem.sDescription ||= ""


    sLogoImgSrc = if hItem.LogoImg then "/db/file/#{hItem.LogoImg}" else ""

    sContentHtml = tmpl.html_tag "tr","row","","",[
      tmpl.html_tag "td", "row-cell col-md-6","","",[
        tmpl.img sLogoImgSrc
      ]
      tmpl.html_tag "td", "row-cell work-item-description col-md-6","","",[
        hItem.sDescription
      ]
    ]

    _.each hItem.aContent, (hRow) =>
      if _.size(hRow.aCells)==1
        sContentHtml += tmpl.html_tag "tr","row","","",[
          tmpl.html_tag "td", "row-cell col-md-12","","colspan='2'",[ @render_cell_content(hRow.aCells[0])]
        ]
      else
        sContentHtml += tmpl.html_tag "tr","row","","",[
          tmpl.html_tag "td", "row-cell col-md-6","","",[ @render_cell_content(hRow.aCells[0])]
          tmpl.html_tag "td", "row-cell col-md-6","","",[ @render_cell_content(hRow.aCells[1])]
        ]

    sParams = """
      valign="middle"
      width = "100%"
    """
    tmpl.div "col-md-12 work",hItem._id, [
      tmpl.div "","","style='height:#{SITE.iNavbarHeight}px'", []
      tmpl.html_tag "table","","",sParams,[sContentHtml]
    ]

  #+2013.12.18 tuiteraz
  render_old: (hItem) ->
    jconsole.debug "templates/work-item-tmpl.render(#{hItem._id})"

    hItem.sDescription ||= ""

    sLogoImgSrc = if hItem.LogoImg then "/db/file/#{hItem.LogoImg}" else ""

    sContentHtml = tmpl.div "row","","",[
      tmpl.div "row-cell col-md-6","","",[
        tmpl.img sLogoImgSrc
      ]
      tmpl.div "row-cell col-md-6 work-item-description","","",[
        hItem.sDescription
      ]
      tmpl.clearfix()
    ]


    _.each hItem.aContent, (hRow) =>
      sContentHtml += @render_content_row(hRow)

    tmpl.div "col-md-12 work",hItem._id, [
      tmpl.div "","","style='height:#{SITE.iNavbarHeight}px'", []
      sContentHtml
    ]

  #+2013.12.18 tuiteraz
  render_cell_content:(hCell) ->
    sHtml = ""
    if wi_hlprs.is_cell_content_type_picture(hCell)
      sHtml = tmpl.div "row-cell-content","",[
        tmpl.img "/db/file/#{hCell.sContent}"
      ]

    else if wi_hlprs.is_cell_content_type_video(hCell)
      sHtml = tmpl.div "row-cell-content","",[
        hCell.sContent
      ]

    return sHtml

  #+2013.12.18 tuiteraz
  render_content_row:(hRow) ->
    sCellsHtml = ""

    if _.size(hRow.aCells) == 1
      sCellSizeClass = "col-md-12"
    else if _.size(hRow.aCells) == 2
      sCellSizeClass = "col-md-6"

    _.each hRow.aCells, (hCell)=>
      sCellContentHtml = @render_cell_content(hCell)
      sCellsHtml += tmpl.div "row-cell #{sCellSizeClass}","","",[
        sCellContentHtml
      ]

    tmpl.div "row","","",[
      sCellsHtml
    ]

