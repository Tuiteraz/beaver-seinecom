_           = require "underscore"
_.str       = require "underscore.string"
_.mixin _.str.exports()

Bcrypt      = require "bcrypt"

SALT_WORK_FACTOR = 10

# init, validate,save,remove only events available

pre =
  save: (fnNext)->
    mdUser = this
    return fnNext() if !mdUser.isModified 'sPassword'

    Bcrypt.genSalt SALT_WORK_FACTOR, (Err,iSalt) ->
      return fnNext(Err) if Err

      Bcrypt.hash mdUser.sPassword, iSalt, (Err,sHash) ->
        return fnNext(Err) if Err

        mdUser.sPassword = sHash
        fnNext()

post =
  save: (mdThis)->
    # pending

exports.pre  = pre
exports.post = post

