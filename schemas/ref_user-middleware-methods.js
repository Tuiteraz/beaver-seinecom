// Generated by CoffeeScript 1.12.7
(function() {
  var Bcrypt, SALT_WORK_FACTOR, _, post, pre;

  _ = require("underscore");

  _.str = require("underscore.string");

  _.mixin(_.str.exports());

  Bcrypt = require("bcrypt");

  SALT_WORK_FACTOR = 10;

  pre = {
    save: function(fnNext) {
      var mdUser;
      mdUser = this;
      if (!mdUser.isModified('sPassword')) {
        return fnNext();
      }
      return Bcrypt.genSalt(SALT_WORK_FACTOR, function(Err, iSalt) {
        if (Err) {
          return fnNext(Err);
        }
        return Bcrypt.hash(mdUser.sPassword, iSalt, function(Err, sHash) {
          if (Err) {
            return fnNext(Err);
          }
          mdUser.sPassword = sHash;
          return fnNext();
        });
      });
    }
  };

  post = {
    save: function(mdThis) {}
  };

  exports.pre = pre;

  exports.post = post;

}).call(this);

//# sourceMappingURL=ref_user-middleware-methods.js.map
