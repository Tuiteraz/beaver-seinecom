{
    "sSlug"       : {"type":"String", "index":{"unique":true}},
    "sContent"    : "String",
    "dtCreated"   : "Date",
    "dtModified"  : "Date"
}