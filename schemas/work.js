{
    "sTitle"       : "String",
    "sDescription" : "String",
    "bInArchive"   : "Boolean",
    "ThmbImg"      : {"type":"ObjectId","ref":"file"},
    "LogoImg"      : {"type":"ObjectId","ref":"file"},
    "iOrder"       : "Number",
    "aContent"     : ["Mixed"],
    "dtCreated"    : "Date",
    "dtModified"   : "Date"
}